# -*- coding:Utf8 -*-

"""
    How to run it ?
        - First go to the project root folder of the package ("pyMayBee")
        - Run python unittest command line ("python3 -m unittest")

    cd /home/pampi/Documents/Projects/pyMayBee
    python3 -m unittest tests/test_sources.py

"""

import unittest

from pyMayBee.Primitive.archive import EpsilonArchive
from pyMayBee.Primitive.fitness import Fitness, FitnessConstrained
from pyMayBee.Primitive.criterion import QualitativeCriterion, DiscreteCriterion, ContinuousCriterion
from pyMayBee.Entity import FoodSource


class MyFitness(Fitness):
    __slots__ = ()
    weights = (1, 1, 1)


class MyConsFitness(FitnessConstrained):
    __slots__ = ()
    weights = (1, 1, 1)


class FoodSourceUnitTests(unittest.TestCase):

    """
        Tests unit for the **FoodSource** class implementation.
    """

# USEFUL FUNCTIONS
    def setUp(self):
        """
            Executing prior to each tasks.
        """
        # Fitness instance
        self.MyFitness = MyFitness
        self.fitness_weights = MyFitness.weights
        # Fitness with constraints instance
        self.MyConsFitness = MyConsFitness
        self.fitness_cons_weights = MyConsFitness.weights
        # An archive instance
        self.base_archive = EpsilonArchive((1, 1, 1))

    def tearDown(self):
        """
            Executing after each task.
            Clean environment.
        """
        pass


# TOOLS (function which can be used to avoid repeated stuff)
    def create_fitness(self, values=(3, 3, 3)):
        return self.MyFitness(values), values

    def create_fitness_with_constraints(self, values=(3, 3, 3)):
        return self.MyConsFitness(values), values

    def create_fitness_class(self, weights=(-1, 2, 3), slots=()):
        class AnotherFitness(Fitness):
            __slots__ = slots
        AnotherFitness.set_weights(weights)
        return AnotherFitness

    def continuous(self, name="continuous_instance", value=0.5, low_bound=0, up_bound=1):
        """Return a **Continuous** Criterion instance."""
        return ContinuousCriterion(name=name, value=value, low_bound=low_bound, up_bound=up_bound)

    def discrete(self, name="discrete_instance", value=6,
                 steps=(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)):
        """Return a **Discrete** Criterion instance."""
        return DiscreteCriterion(name=name, value=value, steps=steps)

    def qualitative(self, name="qualitative_instance", value="Red",
                    steps=("Red", "Blue", "Black", "Green", "Purple")):
        """Return a **Qualitative** Criterion instance."""
        return QualitativeCriterion(name=name, value=value, steps=steps)

    def create_position(self, sequence=True):
        """Create a sequence or a dict of criterion based on sequence parameter."""
        position = []
        position.append(self.discrete())
        position.append(self.qualitative())
        position.append(self.continuous())
        if sequence:
            return position
        else:
            return {pos.name: pos for pos in position}

    def create_source(self, FitnessClass=MyFitness):
        position = self.create_position(False)
        return FoodSource(position, FitnessClass)

    def create_archive(self, epsilons=(1, 1, 1), **kwargs):
        return EpsilonArchive(epsilons=epsilons, **kwargs)

# TESTS (Must start with <test_>)

# Init
    def test_source_cannot_be_created_without_a_container(self):
        with self.assertRaises(TypeError):
            FoodSource(self.discrete(), MyFitness)

    def test_source_cannot_be_created_without_a_FitnessClass(self):
        # If fitness_class not a class at all
        error_msg = 'fitness must be a sub class or and instance of Fitness'
        with self.assertRaises(TypeError) as cm:
            FoodSource(self.discrete(), "miaou")
        self.assertEqual(cm.exception.args[0], error_msg)
        # If not the right type
        error_msg = 'fitness must be a sub class or and instance of Fitness'
        with self.assertRaises(TypeError) as cm:
            FoodSource(self.discrete(), EpsilonArchive)
        self.assertEqual(cm.exception.args[0], error_msg)

    def test_source_can_be_created_using_sequence_or_dict(self):
        # Using a sequence
        position = self.create_position(sequence=True)
        source = FoodSource(position, MyFitness)
        self.assertNotEqual(position, source.position)  # position store as a dict in source
        # Using a dict
        position = self.create_position(sequence=False)
        source = FoodSource(position, MyFitness)
        self.assertEqual(position, source.position)
        # Using helper method
        source = self.create_source()
        self.assertEqual(position, source.position)

    def test_source_can_be_created_with_a_fitness_instance(self):
        fitness = MyFitness(values=(1, 2, 3))
        position = self.create_position(sequence=True)
        source = FoodSource(position, fitness)
        self.assertTrue(source.evaluated is True)

    def test_source_can_be_created_with_a_fitness_class(self):
        fitness = MyFitness
        position = self.create_position(sequence=True)
        source = FoodSource(position, fitness)
        self.assertTrue(source.evaluated is False)


# Update
    def test_trial_init_at_zero_and_can_be_incremented_or_reseted(self):
        source = self.create_source()
        # Init at 0
        trial = source.trial
        self.assertTrue(trial == 0)
        # Can be incremented
        source.increment_trial()
        self.assertTrue(trial + 1 == source.trial)
        # Can be reseted
        source.reset_trial()
        self.assertTrue(trial == source.trial)

    def test_source_is_lost_if_max_trial_exceeded(self):
        source = self.create_source()
        self.assertTrue(source.lost(20) is False)
        source._trial = 30
        self.assertTrue(source.lost(20) is True)

    def test_new_position_reset_trial(self):
        source = self.create_source()
        source._trial = 30
        self.assertTrue(30 == source.trial)
        source.random_position()
        self.assertTrue(0 == source.trial)

    def test_source_can_be_updated(self):
        source = self.create_source()
        source.increment_trial()
        source.increment_trial()
        source.increment_trial()
        self.assertTrue(3 == source.trial)
        fitness = self.create_fitness(values=(3, 2, 1))[0]
        source.update(fitness=fitness, position=source.position)
        self.assertTrue(0 == source.trial)
        self.assertTrue(source.fitness == fitness)

# Archive API
    def test_source_can_be_added_to_archive_as_immutable_object(self):
        archive = self.create_archive()
        source = self.create_source()
        source.add_to(archive)

    def test_source_in_archive_can_be_compare_to_original(self):
        archive = self.create_archive()
        source = self.create_source()
        source.add_to(archive)
        self.assertIn(source, archive)

# Copy
    def test_copy_source_return_a_true_copy(self):
        source = self.create_source()
        # No change on trial
        another_src = source.copy()
        self.assertTrue(source.fitness == another_src.fitness)
        self.assertTrue(source.position == another_src.position)
        self.assertTrue(source == another_src)
        # Change trial
        source.increment_trial()
        self.assertTrue(source.trial == 1)
        self.assertTrue(another_src.trial == 0)
        self.assertTrue(source == another_src)  # Trial is not used to test equality
        another_src = source.copy()
        self.assertTrue(source.trial == another_src.trial)
        self.assertTrue(source == another_src)
        # Copy a fitness create a true copy
        # A criterion is immutable
        # New source change are then not apply to copy
        source.fitness.values = (2, 2, 2)
        self.assertTrue(source.fitness != another_src.fitness)
        self.assertTrue(source != another_src)

    def test_deepcopy_source_return_a_true_copy(self):
        source = self.create_source()
        another_src = source.__deepcopy__(None)
        self.assertTrue(source == another_src)
        self.assertTrue(source == another_src.clone())

# Representation
    def test_repr(self):
        source = self.create_source()
        new_source = eval(repr(source))
        self.assertTrue(source == new_source)

    def test_str(self):
        source = self.create_source()
        self.assertEqual(str(source), 'FoodSource(MyFitness((), ()), trial=0)')


# Run all tests
if __name__ == '__main__':
    unittest.main()
