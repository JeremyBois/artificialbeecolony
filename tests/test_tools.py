# -*- coding:Utf8 -*-

"""
    How to run it ?
        - First go to the project root folder of the package ("pyMayBee")
        - Run python unittest command line ("python3 -m unittest")

    cd /home/pampi/Documents/Projects/pyMayBee
    python3 -m unittest tests/test_tools.py

"""

import unittest
import random

from pyMayBee.Misc.tools import (inherit_docstring, from_json, closest, binary_search)


class ToolsUnitTests(unittest.TestCase):

    """
        Tests unit for the **tools** utlities implementations.
    """

# USEFUL FUNCTIONS
    def setUp(self):
        """
            Executing prior to each tasks.
        """

    def tearDown(self):
        """
            Executing after each task.
            Clean environment.
        """
        pass


# TOOLS (function which can be used to avoid repeated stuff)

# TESTS (Must start with <test_>)
    def test_can_inherite_docstring_using_inherit_docstring(self):
        class WithDocstring():

            """I’m a docstring and I’m inheritable !"""
            pass

        @inherit_docstring
        class InheritedDocString(WithDocstring):
            pass
        instance = WithDocstring()
        inherited_instance = InheritedDocString()
        self.assertEqual(instance.__doc__, inherited_instance.__doc__)

    def test_can_load_json_from_file(self):
        file_path = 'tests/Criteria/tools_test.json'
        # Result expected
        ref_dico = {}
        ref_dico['criterion1'] = dict(low_bound=2, up_bound=10, type='continuous')
        ref_dico['criterion2'] = dict(steps=[1, 3, 4, 5, 6, 7], type='discrete')
        ref_dico['criterion3'] = dict(steps=['miam', 'bof', 'Maimouna'],
                                      type='qualitative')
        # Function result with debug to call pp function
        dico = from_json(file_path, encoding='utf-8', debug=True)['criteria']

        self.assertEqual(dico, ref_dico)

    def test_binary_search(self):
        array = [1, 1, 3, 5, 5, 6, 7, 11, 11]
        # Return always existing index even if random lower or higher
        for _ in range(10000):
            random_picked = binary_search(array, random.randint(-100, 100))
            self.assertIn(random_picked, list(range(9)))
            random_value = closest(array, random.randint(-100, 100))
            self.assertIn(random_value, array)


# Run all tests
if __name__ == '__main__':
    unittest.main()
