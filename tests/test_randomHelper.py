# -*- coding:Utf8 -*-

"""
    How to run it ?
        - First go to the project root folder of the package ("pyMayBee")
        - Run python unittest command line ("python3 -m unittest")

    cd /home/pampi/Documents/Projects/pyMayBee
    python3 -m unittest tests/test_randomHelper.py

"""

import unittest

import pyMayBee.Misc.random_helper as rh


class RandomHelperUnitTests(unittest.TestCase):

    """
        Tests unit for the **random_helper** utilities implementations.
    """

# USEFUL FUNCTIONS
    def setUp(self):
        """
            Executing prior to each tasks.
        """
        # Always same random seed
        rh.set_random_seed(12345)

    def tearDown(self):
        """
            Executing after each task.
            Clean environment.
        """
        pass


# TOOLS (function which can be used to avoid repeated stuff)

# TESTS (Must start with <test_>)

# Random motion
    def test_uniform_motion(self):
        result = 11.167321784365225
        random_value = 10 + rh.uniform_motion(10, 20, 0.7)
        self.assertEqual(random_value, result)

    def test_directed_uniform_motion(self):
        result = 12.916339107817388
        # Ref bigger
        based_value, ref_value = 10, 20
        random_value = 10 + rh.directed_uniform_motion(based_value, ref_value, 0.7)
        self.assertEqual(random_value, result)
        # Random_walk always from value to ref_value
        for _ in range(10000):
            new_value = based_value + rh.directed_uniform_motion(based_value, ref_value, 0.7)
            self.assertGreater(new_value, based_value)
        # Ref lower
        based_value, ref_value = 20, 10
        # Random_walk always from value to ref_value
        for _ in range(10000):
            random_value = based_value + rh.directed_uniform_motion(based_value, ref_value, 0.7)
            self.assertLess(random_value, based_value)
        # Négative
        based_value, ref_value = 20, -10
        # Random_walk always from value to ref_value
        for _ in range(10000):
            random_value = based_value + rh.directed_uniform_motion(based_value, ref_value, 0.7)
            self.assertLess(random_value, based_value)

    def test_levy_flight(self):
        result = 10.03486209532915
        random_value = 10 + rh.levy_flight(10, 20, 1.5, 0.01)
        self.assertEqual(random_value, result)

    def test_directed_levy_flight(self):
        result = 10.03486209532915
        # Ref bigger
        based_value, ref_value = 10, 20
        random_value = based_value + rh.directed_levy_flight(based_value, ref_value, 1.5, 0.01)
        self.assertEqual(random_value, result)
        # Random_walk always from value to ref_value
        for _ in range(10000):
            new_value = based_value + rh.directed_levy_flight(based_value, ref_value, 1.5, 0.01)
            self.assertGreater(new_value, based_value)
        # Ref lower
        based_value, ref_value = 20, 10
        # Random_walk always from value to ref_value
        for _ in range(10000):
            random_value = based_value + rh.directed_levy_flight(based_value, ref_value, 1.5, 0.01)
            self.assertLess(random_value, based_value)
        # Ref négative
        based_value, ref_value = 20, -10
        # Random_walk always from value to ref_value
        for _ in range(10000):
            random_value = based_value + rh.directed_levy_flight(based_value, ref_value, 1.5, 0.01)
            self.assertLess(random_value, based_value)

    def test_browian_motion(self):
        result = -317.52398408822296
        # Ref bigger
        based_value, ref_value = 10, 20
        random_value = based_value + rh.brownian_motion(based_value, ref_value, 33, 2)
        self.assertEqual(random_value, result)

# Discrete table
    def test_roll_prob_table_can_only_pick_value_with_weight_positive(self):
        # (Prob, value) table where 0 as weight cannot be picked
        prob_table = [(20, 1), (0, 2), (10, 3), (0, 4), (0, 5), (14, 6)]
        possible_result = [1, 3, 6]
        for _ in range(10000):
            random_picked = rh.roll(prob_table)
            self.assertIn(random_picked, possible_result)

    def test_roulette_selection(self):
        dominance = [2, 1, 0, 1, 0, 0]
        # Without normalization
        cum_prob = rh.init_roulette(dominance)
        for i in range(10000):
            random_picked = rh.launch_roulette(cum_prob)
            self.assertNotIn(random_picked, [2, 4, 5])
            self.assertIn(random_picked, [0, 1, 3])
        # With normalization on source number
        cum_prob = rh.init_roulette([el / 6 for el in dominance])
        for i in range(10000):
            random_picked = rh.launch_roulette(cum_prob)
            self.assertNotIn(random_picked, [2, 4, 5])
            self.assertIn(random_picked, [0, 1, 3])
        # With normalization on source number and dominance
        cum_prob = rh.init_roulette([el / 6 for el in dominance])
        sumProb = sum(cum_prob)
        cum_prob = [el / sumProb for el in cum_prob]
        for i in range(10000):
            random_picked = rh.launch_roulette(cum_prob)
            self.assertNotIn(random_picked, [2, 4, 5])
            self.assertIn(random_picked, [0, 1, 3])


# Run all tests
if __name__ == '__main__':
    unittest.main()
