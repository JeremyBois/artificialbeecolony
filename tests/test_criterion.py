# -*- coding:Utf8 -*-

"""
    How to run it ?
        - First go to the project root folder of the package ("pyMayBee")
        - Run python unittest command line ("python3 -m unittest")

    cd /home/pampi/Documents/Projects/pyMayBee
    python3 -m unittest tests/test_criterion.py

    À vérifier:
        - __repr__ permet de construire la même instance
"""

import unittest

import pandas as pd
from pandas.util.testing import assert_frame_equal

from pyMayBee.Primitive.criterion import *


class CriteriaUnitTests(unittest.TestCase):

    """
        Tests unit for the **Criterion** subclass implementation.
    """

# USEFUL FUNCTIONS

    def setUp(self):
        """
            Executing prior to each tasks.
        """
        pass

    def tearDown(self):
        """
            Executing after each task.
            Clean environment.
        """
        pass


# TOOLS (function which can be used to avoid repeated stuff)

    def continuous(self, name="continuous_instance", value=0.5, low_bound=0, up_bound=1):
        """Return a **Continuous** Criterion instance."""
        return ContinuousCriterion(name=name, value=value, low_bound=low_bound, up_bound=up_bound)

    def discrete(self, name="discrete_instance", value=6,
                 steps=(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)):
        """Return a **Discrete** Criterion instance."""
        return DiscreteCriterion(name=name, value=value, steps=steps)

    def qualitative(self, name="qualitative_instance", value="Red",
                    steps=("Red", "Blue", "Black", "Green", "Purple")):
        """Return a **Qualitative** Criterion instance."""
        return QualitativeCriterion(name=name, value=value, steps=steps)


# TESTS (Must start with <test_>)

# Static methods

    def test_create_dict_from_criterion_list(self):
        result = {'criterion 2': 2, 'criterion 3': 3, 'criterion 1': 1}
        criteria = [self.discrete('criterion {}'.format(i), value=i) for i in range(1, 4)]
        dico = DiscreteCriterion.to_dict(criteria)
        self.assertEqual(result, dico)

    def test_create_dict_from_criterion_dict(self):
        result = {'criterion 2': 2, 'criterion 3': 3, 'criterion 1': 1}
        criteria = {'criterion {}'.format(i - 1): self.discrete('criterion {}'.format(i), value=i) for i in range(1, 4)}
        dico = DiscreteCriterion.to_dict(criteria)
        self.assertEqual(result, dico)

    def test_create_dict_from_criterion(self):
        result = {'criterion 1': 1}
        criterion = self.discrete('criterion 1', value=1)
        dico = DiscreteCriterion.to_dict(criterion)
        self.assertEqual(result, dico)

    def test_create_df_from_list_of_criterion_list(self):
        result = {'criterion 0': [1, 1], 'criterion 1': [1, 2], 'criterion 2': [1, 3]}
        list_of_criteria = []
        # First position
        list_of_criteria.append({'criterion {}'.format(i): self.discrete('criterion {}'.format(i), value=1) for i in range(3)})
        # Second position
        list_of_criteria.append({'criterion {}'.format(i - 1): self.discrete('criterion {}'.format(i), value=i) for i in range(1, 4)})
        assert_frame_equal(pd.DataFrame(result), DiscreteCriterion.to_df(list_of_criteria))


# Init
    def test_criterion_must_have_name_define(self):
        with self.assertRaises(TypeError):
            ContinuousCriterion()
        with self.assertRaises(TypeError):
            DiscreteCriterion()
        with self.assertRaises(TypeError):
            QualitativeCriterion()
        criterion = self.continuous()
        self.assertEqual(criterion.name, "continuous_instance")
        criterion = self.discrete()
        self.assertEqual(criterion.name, "discrete_instance")
        criterion = self.qualitative()
        self.assertEqual(criterion.name, "qualitative_instance")

    def test_discrete_criterion_must_have_steps_define(self):
        error_msg = "`steps` cannot be empty"
        steps = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        # Wrong way
        with self.assertRaises(ValueError) as cm:
            self.discrete(steps=())
        self.assertEqual(cm.exception.args[0], error_msg)
        # Correct way
        criterion = self.discrete()
        self.assertEqual(criterion.definition["steps"], steps)

    def test_discrete_criterion_boundaries_are_get_from_steps(self):
        steps = (1, 2, 3, 4, 5, 6, 7, 8, 9, 133)
        criterion = self.discrete(steps=steps)
        self.assertEqual(criterion.definition["up_bound"], steps[-1])
        self.assertEqual(criterion.definition["low_bound"], steps[0])

    def test_qualitative_criterion_must_have_steps_defined(self):
        error_msg = "`steps` cannot be empty"
        steps = tuple(sorted(["Red", "Blue", "Black", "Green", "Purple"]))
        # Wrong way
        with self.assertRaises(ValueError) as cm:
            self.qualitative(steps=())
        self.assertEqual(cm.exception.args[0], error_msg)
        # Correct way
        criterion = self.qualitative(steps=steps)
        self.assertEqual(criterion.definition["steps"], steps)

    def test_qualitative_criterion_have_no_bounds(self):
        steps = tuple(sorted(["Red", "Blue", "Black", "Green", "Purple"]))
        criterion = self.qualitative(steps=steps)
        with self.assertRaises(KeyError):
            criterion.definition["up_bound"]
        with self.assertRaises(KeyError):
            criterion.definition["low_bound"]

    def test_continuous_criterion_have_default_boundaries(self):
        up, down = 1, 0
        criterion = self.continuous()
        self.assertEqual(criterion.definition["up_bound"], up)
        self.assertEqual(criterion.definition["low_bound"], down)

    def test_continuous_criterion_lower_bound_must_be_lower_than_upper(self):
        error_msg = "Lower bound (`low_bound`) must be lower than Upper bound (`up_bound`)"
        with self.assertRaises(ValueError) as cm:
            self.continuous(up_bound=-11, low_bound=9)
        self.assertEqual(cm.exception.args[0], error_msg)

    def test_discrete_criterion_steps_sorted_and_duplicate_values_removed(self):
        steps = (1, 2, 3, 3, 3, 2, 1, 0, 7, 3, 4, 5, 6, 8, 9, 133)
        correct_steps = tuple(sorted(set(steps)))
        criterion = self.discrete(steps=steps)
        self.assertEqual(criterion.definition["steps"], correct_steps)
        self.assertNotEqual(criterion.definition["steps"], steps)

    def test_criterion_have_default_value(self):
        # Discrete
        criterion = self.discrete(value=None)
        self.assertIn(criterion.value, criterion.steps)
        # Continuous
        criterion = self.continuous(value=None)
        self.assertTrue(criterion.value <= criterion.up_bound)
        self.assertTrue(criterion.value >= criterion.low_bound)
        # Qualitative
        criterion = self.qualitative(value=None)
        self.assertIn(criterion.value, criterion.steps)

    def test_call_method_create_new_criterion(self):
        # Discrete
        criterion = self.discrete(value=None)
        criterion = criterion(2)  # Specific new value
        self.assertEqual(criterion.value, 2)
        self.assertIn(criterion.value, criterion.steps)
        criterion = criterion()  # Random new value
        self.assertIn(criterion.value, criterion.steps)
        # Continuous
        criterion = self.continuous(value=None)
        criterion = criterion(0.2)  # Specific new value
        self.assertEqual(criterion.value, 0.2)
        self.assertTrue(criterion.value <= criterion.up_bound)
        self.assertTrue(criterion.value >= criterion.low_bound)
        criterion = criterion()  # Random new value
        self.assertTrue(criterion.value <= criterion.up_bound)
        self.assertTrue(criterion.value >= criterion.low_bound)
        # Qualitative
        criterion = self.qualitative(value=None)
        criterion = criterion('Red')  # Specific new value
        self.assertEqual(criterion.value, 'Red')
        self.assertIn(criterion.value, criterion.steps)
        criterion = criterion()  # Random new value
        self.assertIn(criterion.value, criterion.steps)

# Update
    def test_setting_new_value_return_another_instance(self):
        steps = (1, 2, 3, 3, 3, 2, 1, 0, 7, 3, 4, 5, 6, 8, 9, 133)
        criterion_1 = self.discrete(steps=steps)
        criterion_2 = criterion_1(133)
        criterion_3 = criterion_1.update(3)
        self.assertEqual(criterion_1.value, 6)
        self.assertEqual(criterion_2.value, 133)
        self.assertEqual(criterion_3.value, 3)

    def test_discrete_criterion_value_cannot_be_outisde_bounds(self):
        steps = (-1, 2, 3, 3, 3, 2, 1, 0, 7, 3, 4, 5, 6, 8, 9, 133)
        # Value too big
        criterion_1 = self.discrete(value=13333, steps=steps)
        self.assertEqual(criterion_1.value, 133)
        # New value too small
        criterion_2 = criterion_1.update(-22222)
        self.assertEqual(criterion_2.value, -1)

    def test_continuous_criterion_new_value_cannot_be_outisde_bounds(self):
        # New value too big
        criterion_1 = self.continuous(value=13333)
        self.assertEqual(criterion_1.value, 1)
        # New value too small
        criterion_2 = criterion_1.update(-22222)
        self.assertEqual(criterion_2.value, 0)

    def test_new_qualitative_value_must_be_in_steps(self):
        steps = ("Red", "Blue", "Black", "Green", "Purple")
        error_msg = "New value must be in ('Black', 'Blue', 'Green', 'Purple', 'Red')"
        criterion = self.qualitative(steps=steps)
        # If new value outside steps
        with self.assertRaises(ValueError) as cm:
            criterion("White")
        self.assertEqual(cm.exception.args[0], error_msg)
        # If new value inside steps
        correct_criterion = criterion("Red")
        self.assertEqual(correct_criterion.value, "Red")

    def test_discrete_new_value_prefer_upper_one_if_both_closest(self):
        steps = (-1, 2, 3, 3, 3, 2, 1, 0, 7, 3, 4, 5, 6, 9, 133)
        criterion_1 = self.discrete(steps=steps)
        # New value too small
        criterion_2 = criterion_1.update(8)
        self.assertEqual(criterion_2.value, 9)

# Representation
    def test_qualitative_criterion_can_be_instantiate_from_repr(self):
        steps = tuple(sorted(["Red", "Blue", "Black", "Green", "Purple"]))
        repr_text = ("QualitativeCriterion(name='qualitative_instance', " +
                     "value='Red', steps=('Black', 'Blue', 'Green', 'Purple', 'Red'))")

        criterion = self.qualitative(steps=steps)
        self.assertEqual(repr(criterion), repr_text)
        self.assertEqual(eval(repr(criterion)), criterion)

    def test_discrete_criterion_can_be_instantiate_from_repr(self):
        steps = steps = (1, 2, 3, 4, 5, 6, 7, 8, 9, 133)
        repr_text = ("DiscreteCriterion(name='discrete_instance', value=6, steps=(1, 2, 3, 4, 5, 6, 7, 8, 9, 133))")
        criterion = self.discrete(steps=steps)
        self.assertEqual(repr(criterion), repr_text)
        self.assertEqual(eval(repr(criterion)), criterion)

    def test_continuous_criterion_can_be_instantiate_from_repr(self):
        repr_text = ("ContinuousCriterion(name='continuous_instance', " +
                     "value=0.5, low_bound=0, up_bound=1)")

        criterion = self.continuous()
        self.assertEqual(repr(criterion), repr_text)
        self.assertEqual(eval(repr(criterion)), criterion)

    def test_print(self):
        qual_steps = tuple(sorted(["Red", "Blue", "Black", "Green", "Purple"]))
        quali_text = ("QualitativeCriterion(name=qualitative_instance, value=Red, " +
                      "steps=('Black', 'Blue', 'Green', 'Purple', 'Red'))")
        discrete_steps = (1, 2, 3, 4, 5, 6, 7, 8, 9, 133)
        discrete_text = ("DiscreteCriterion(name=discrete_instance, value=6, "
                         "low_bound=1, up_bound=133, steps=(1, 2, 3, 4, 5, 6, 7, 8, 9, 133))")
        cont_text = ("ContinuousCriterion(name=continuous_instance, value=0.5, "
                     "low_bound=0, up_bound=1)")
        criterion = self.qualitative(steps=qual_steps)
        self.assertEqual(str(criterion), quali_text)

        criterion = self.discrete(steps=discrete_steps)
        self.assertEqual(str(criterion), discrete_text)

        criterion = self.continuous()
        self.assertEqual(str(criterion), cont_text)

# State
    def test_Criterion_is_immutable_and_support_hashing(self):
        hash(self.continuous())
        hash(self.discrete([1, 2, 3]))
        hash(self.qualitative(["Red", "Blue", "Black"]))

    def test_criterion_name_does_not_has_to_be_the_same_to_be_equal(self):
        # name are just a syntactic sugar attribute
        criterion_1 = self.continuous(name="mouse")
        criterion_2 = self.continuous(name="cat")
        self.assertEqual(criterion_1, criterion_2)
        self.assertEqual(hash(criterion_1), hash(criterion_2))

    def test_definition_attribute_can_be_access_from_class(self):
        error_msg = "'ContinuousCriterion' object has no attribute 'steps'"
        criterion = self.continuous(name="continuous")
        self.assertEqual(criterion.up_bound, 1)
        # Attribute error raise when attribute does not exist inside definition
        with self.assertRaises(AttributeError) as cm:
            criterion.steps
        self.assertEqual(cm.exception.args[0], error_msg)
        criterion = self.discrete(name="discrete", steps=(1, 2, 3))
        self.assertEqual(criterion.steps, (1, 2, 3))
        self.assertEqual(criterion.up_bound, 3)
        self.assertEqual(criterion.low_bound, 1)
        # Attribute error raise when attribute does not exist at all
        error_msg = "'DiscreteCriterion' object has no attribute 'nothere'"
        with self.assertRaises(AttributeError) as cm:
            criterion.nothere
        self.assertEqual(cm.exception.args[0], error_msg)

    def test_if_is_quantitative_discrete_and_continuous_criterion(self):
        con_criterion = self.continuous()
        disc_criterion = self.discrete()
        qual_criterion = self.qualitative()
        self.assertTrue(con_criterion.is_quantitative())
        self.assertTrue(disc_criterion.is_quantitative())
        self.assertFalse(qual_criterion.is_quantitative())

# Operators
    def test_equality_and_inequality(self):
        criterion1 = self.continuous(value=0.5)
        criterion2 = self.continuous(value=1)
        criterion3 = self.discrete(value=1)
        criterion4 = self.discrete(value=1)
        criterion5 = self.discrete(value=1, steps=[1, 2, 3])
        # Same definition and same value
        self.assertEqual(criterion3, criterion4)
        # Same definition but different value
        self.assertNotEqual(criterion1, criterion2)
        # Same value but different definition
        self.assertNotEqual(criterion2, criterion5)
        self.assertNotEqual(criterion4, criterion5)
        # Different value and different definition
        self.assertNotEqual(criterion1, criterion3)
        self.assertFalse(criterion1 == 22)
        self.assertTrue(criterion3, criterion4)

# Copy
    def test_deepcopy_a_criterion_does_not_use_reference(self):
        # Original
        criterion1 = self.continuous()
        criterion2 = self.qualitative(["cat", "dog"])
        criterion3 = self.discrete()
        # Copies
        criterion1_c = criterion1.clone()
        criterion2_c = criterion2.clone()
        criterion3_c = criterion3.clone()
        # They must be equals
        self.assertEqual(criterion1, criterion1_c)
        self.assertEqual(criterion2, criterion2_c)
        self.assertEqual(criterion3, criterion3_c)
        # Test definition id for continuous
        self.assertIsNot(criterion1.definition, criterion1_c.definition)
        # Test definition id for qualitative
        self.assertIsNot(criterion2.definition, criterion2_c.definition)
        self.assertIsNot(criterion2.definition["steps"], criterion2_c.definition["steps"])
        # Test definition id for discrete
        self.assertIsNot(criterion3.definition, criterion3_c.definition)
        self.assertIsNot(criterion3.definition["steps"], criterion3_c.definition["steps"])


# Run all tests
if __name__ == '__main__':
    unittest.main()
