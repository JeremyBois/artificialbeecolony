# -*- coding:Utf8 -*-

"""
    How to run it ?
        - First go to the project root folder of the package ("pyMayBee")
        - Run python unittest command line ("python3 -m unittest")

    cd /home/pampi/Documents/Projects/pyMayBee
    python3 -m unittest tests/test_archive.py
"""

import unittest

import random
from collections import namedtuple

from pyMayBee.Primitive.archive import ArchiveInterface, EpsilonArchive
from pyMayBee.Primitive.fitness import Fitness


class MyFitness(Fitness):
    __slots__ = ()
    weights = (1, 1, 1)


class MyFitness2(Fitness):
    __slots__ = ()
    weights = (1, -1, 1)


class MyFitness3(Fitness):
    __slots__ = ()
    weights = (1, 1)


# A simple source representation with only fitness attribute.
# Other can be added but archive does not care about
Source = namedtuple('Source', ['fitness'])


class BaseArchiveUnitTests(unittest.TestCase):

    """
        Tests unit for the **Archive** class implementation.
    """

# USEFUL FUNCTIONS

    def setUp(self):
        """
            Executing prior to each tasks.
        """
        # Base archive
        self.base_archive = ArchiveInterface()

    def tearDown(self):
        """
            Executing after each task.
            Clean environment.
        """
        pass

# TESTS (Must start with <test_>)

# Init
    def test_can_add_to_archive(self):
        for i in range(10):
            self.base_archive.add(i)
        for i in range(10):
            self.assertIn(i, self.base_archive)

    def test_repr_allow_to_create_an_empty_instance(self):
        archive = eval(repr(self.base_archive))
        self.assertTrue(archive == self.base_archive)

    def test_str_representation(self):
        alist = []
        for i in range(10):
            self.base_archive.add(i)
            alist.append(i)
        self.assertTrue(alist == self.base_archive.archive)
        self.assertTrue(str(alist) == str(self.base_archive))


class EpsilonArchiveUnitTests(unittest.TestCase):

    """
        Tests unit for the **EpsilonArchive** class implementation.
    """

# USEFUL FUNCTIONS

    def setUp(self):
        """
            Executing prior to each tasks.
        """
        # Fitness constructor
        self.MyFitness = MyFitness
        self.fitness_weights = MyFitness.weights
        # Source constructor
        self.FakeSource = Source
        # Base archive
        self.base_archive = EpsilonArchive((1, 1, 1))

    def tearDown(self):
        """
            Executing after each task.
            Clean environment.
        """
        pass


# TOOLS (function which can be used to avoid repeated stuff)

    def create_fitness(self, values=(3, 3, 3)):
        return self.MyFitness(values), values

    def create_fitness_class(self, weights=(-1, 2, 3), slots=()):
        class AnotherFitness(Fitness):
            __slots__ = slots
        AnotherFitness.set_weights(weights)
        return AnotherFitness

    def create_source(self, values, FitnessClass=MyFitness):
        return Source(FitnessClass(values))

    def create_archive(self, epsilons=(1, 1, 1), **kwargs):
        return EpsilonArchive(epsilons=epsilons, **kwargs)

    def source_to_box(self, source, epsilons=(1, 1, 1)):
        """Keep track of the box (as a tuple) for the *source*.
        Ceil used to get the upper corner of the box as a reference.
        """
        from math import ceil
        return tuple(ceil(w_value / epsilon)
                     for (w_value, epsilon) in zip(source.fitness.w_values, epsilons))


# TESTS (Must start with <test_>)

# Init
    def test_cannot_create_archive_without_epsilons(self):
        error_text = "__init__() missing 1 required positional argument: 'epsilons'"
        with self.assertRaises(TypeError) as cm:
            EpsilonArchive()
        self.assertEqual(cm.exception.args[0], error_text)

    def test_can_instantiate_archive_with_any_sequence(self):
        epsilons = ((1, 2, 3), [1, 2, 3], set((1, 2, 3)))
        self.assertEqual(EpsilonArchive(epsilons[0]).epsilons, (1, 2, 3))
        self.assertEqual(EpsilonArchive(epsilons[1]).epsilons, (1, 2, 3))
        self.assertEqual(EpsilonArchive(epsilons[2]).epsilons, (1, 2, 3))

# Update
    def test_epsilons_are_read_only_attribute(self):
        error_text = "can't set attribute"
        archive = self.create_archive()
        with self.assertRaises(AttributeError) as cm:
            archive.epsilons = (1, 2, 4)
        self.assertEqual(cm.exception.args[0], error_text)

    def test_new_source_can_be_added(self):
        # Adding a new source populate archive attribute
        archive = self.create_archive()
        new_source = self.create_source((1, 1, 1))
        archive.add(new_source)
        self.assertTrue(new_source, archive.archive[0])
        self.assertIn(new_source, archive)
        self.assertTrue(new_source in archive)

    def test_new_source_with_negative_objectives_can_be_added(self):
        archive = self.create_archive()
        new_source = self.create_source((-1, 1, 1))
        archive.add(new_source)
        archive.add(new_source)
        self.assertTrue(new_source, archive.archive[0])
        self.assertIn(new_source, archive)
        self.assertTrue(archive.__contains__(new_source))

    def test_existing_source_in_archive_are_unique(self):
        archive = self.create_archive()
        new_source = self.create_source((-1, 1, 1))
        archive.add(new_source)
        archive.add(new_source)
        self.assertTrue(len(archive) == 1)
        # Check if that’s not len implementation which is broken
        with self.assertRaises(IndexError):
            archive.archive[1]

    def test_new_source_added_to_archive_are_reference(self):
        archive = self.create_archive()
        new_source = self.create_source((1, 1, 1))
        archive.add(new_source)
        # Sources must be equal
        self.assertTrue(new_source, archive.archive[0])
        self.assertIn(new_source, archive)
        self.assertTrue(archive.__contains__(new_source))
        # Sources must not have the same id
        self.assertIs(archive.archive[0], new_source)
        self.assertEqual(id(archive.archive[0]), id(new_source))

    def test_if_new_source_boxdominante_then_dominated_source_are_removed(self):
        archive = self.create_archive()
        first_source = self.create_source((2, 2, 4))
        second_source = self.create_source((3, 2, 3))
        archive.add(first_source)
        archive.add(second_source)
        self.assertTrue(len(archive) == 2)
        self.assertIn(first_source, archive)
        self.assertIn(second_source, archive)
        new_source = self.create_source((5, 5, 5))
        archive.add(new_source)
        # New source replace all dominated archive sources
        self.assertIn(new_source, archive)
        self.assertNotIn(first_source, archive)
        self.assertTrue(len(archive) == 1)

    def test_new_source_boxdominated_not_added(self):
        archive = self.create_archive()
        first_source = self.create_source((5, 5, 5))
        archive.add(first_source)
        self.assertIn(first_source, archive)
        new_source = self.create_source((2, 2, 4))
        archive.add(new_source)
        # New source must not be inside
        self.assertNotIn(new_source, archive)
        self.assertIn(first_source, archive)
        self.assertTrue(len(archive) == 1)

    def test_new_source_with_box_distance_larger_is_not_added(self):
        """Test box solution selection with  distance criterion."""
        epsilons = (2, 2, 2)
        archive = self.create_archive(epsilons)
        first_source = self.create_source((2, 2, 4))
        archive.add(first_source)
        # Source should be added
        self.assertIn(first_source, archive)
        # Try to add another with same box value but further to box corner
        new_source = self.create_source((2, 2, 3))
        archive.add(new_source)
        self.assertNotIn(new_source, archive)
        self.assertIn(first_source, archive)
        # Make sure box values are the same
        self.assertEqual(self.source_to_box(new_source, epsilons),
                         self.source_to_box(first_source, epsilons))

    def test_new_source_with_box_distance_smaller_is_added(self):
        """Test box solution selection with distance criterion."""
        epsilons = (2, 2, 2)
        archive = self.create_archive(epsilons)
        first_source = self.create_source((2, 2, 3))
        archive.add(first_source)
        # Source should be added
        self.assertIn(first_source, archive)
        # Try to add another with same box value but closer to box corner
        new_source = self.create_source((2, 2, 4))
        archive.add(new_source)
        self.assertIn(new_source, archive)
        self.assertNotIn(first_source, archive)
        # Make sure box values are the same
        self.assertEqual(self.source_to_box(new_source, epsilons),
                         self.source_to_box(first_source, epsilons))

    def test_new_normalized_source_does_not_normalize_again(self):
        # Adding a new source populate archive attribute
        epsilons = (1, 1, 1)
        archive = self.create_archive(epsilons)
        first_source = self.create_source((1, 1, 0.5))
        archive.add(first_source)
        # Source should be added
        self.assertIn(first_source, archive)
        # Try to add another with same box value but closer to box corner
        new_source = self.create_source((1, 1, 0.6))
        archive.add(new_source)
        self.assertIn(new_source, archive)
        self.assertNotIn(first_source, archive)
        # Make sure box values are the same
        self.assertEqual(self.source_to_box(new_source, epsilons),
                         self.source_to_box(first_source, epsilons))

    def test_new_source_with_same_box_must_dominate_archive_to_be_added(self):
        """Test box solution selection with strict dominance criterion."""
        epsilons = (2, 2, 2)
        archive = self.create_archive(epsilons, distance_based=False)
        first_source = self.create_source((2.2, 2.1, 4))
        archive.add(first_source)
        # Source should be added
        self.assertIn(first_source, archive)
        # Try to add another with same box value but further to box corner
        non_dominated_source = self.create_source((2.1, 2.2, 3))
        dominante_source = self.create_source((2.3, 2.1, 4))
        # Non dominated source not added
        archive.add(non_dominated_source)
        self.assertNotIn(non_dominated_source, archive)
        self.assertIn(first_source, archive)
        # Dominante source added
        archive.add(dominante_source)
        self.assertTrue(dominante_source.fitness > first_source.fitness)
        self.assertIn(dominante_source, archive)
        self.assertNotIn(first_source, archive)
        # Make sure box values are the same
        self.assertEqual(self.source_to_box(first_source, epsilons),
                         self.source_to_box(non_dominated_source, epsilons))
        self.assertEqual(self.source_to_box(first_source, epsilons),
                         self.source_to_box(dominante_source, epsilons))

    def test_new_source_with_non_dominated_box_added(self):
        # A new source non boxe dominated and not inside another one is added
        epsilons = (2, 2, 2)
        archive = self.create_archive(epsilons)
        first_source = self.create_source((2, 2, 5))
        archive.add(first_source)
        # Source should be added
        self.assertIn(first_source, archive)
        # Source added if box value non dominated
        second_source = self.create_source((4, 2, 3))
        archive.add(second_source)
        self.assertIn(first_source, archive)
        self.assertIn(second_source, archive)
        # Make sur box values are different
        self.assertNotEqual(self.source_to_box(first_source, epsilons),
                            self.source_to_box(second_source, epsilons))
        self.assertGreater(self.source_to_box(second_source, epsilons),
                           self.source_to_box(first_source, epsilons))

    def test_archive_can_merge_inplace_another_archive(self):
        epsilons = (2, 2, 2)
        # Populate first archive with two source
        archive1 = self.create_archive(epsilons)
        first_source = self.create_source((2, 2, 5))
        archive1.add(first_source)
        second_source = self.create_source((4, 2, 3))
        archive1.add(second_source)
        self.assertIn(first_source, archive1)
        self.assertIn(second_source, archive1)
        # Populate archive with two source (but one already inside the first one)
        archive2 = self.create_archive(epsilons)
        another_source = self.create_source((2, 4, 3))
        archive2.add(another_source)
        archive2.add(first_source)
        self.assertIn(another_source, archive2)
        self.assertIn(first_source, archive2)
        # Merge should return an archive with 3 sources
        archive1.merge(archive2)
        self.assertEqual(len(archive1), 3)
        self.assertIn(first_source, archive1)
        self.assertIn(second_source, archive1)
        self.assertIn(another_source, archive1)

    def test_minimization_working_for_box_and_distance_domination(self):
        epsilons = (2, 2, 2)
        archive = self.create_archive(epsilons)
        # Create source with second objective to minimize
        # MyFitness2 defines with weights as (1, -1, 1)
        A = self.create_source((4, 6, 7), MyFitness2)     # A
        B = self.create_source((4, 7, 7), MyFitness2)     # B
        C = self.create_source((4, 2, 7), MyFitness2)     # C
        D = self.create_source((4, -2, 7), MyFitness2)    # D
        E = self.create_source((3.6, -2, 8), MyFitness2)  # E
        # Add source A
        archive += A
        self.assertIn(A, archive)
        # Add source B: box(A) == box(B) but A > B
        self.assertEqual(self.source_to_box(A, epsilons),
                         self.source_to_box(B, epsilons))
        archive += B
        self.assertIn(A, archive)
        self.assertNotIn(B, archive)
        # Add source C: box(C) > box(A)
        self.assertGreater(self.source_to_box(C, epsilons),
                           self.source_to_box(A, epsilons))
        archive += C
        self.assertIn(C, archive)
        self.assertNotIn(A, archive)
        # Add source D: box(D) > box(C) car minimization
        self.assertGreater(self.source_to_box(D, epsilons),
                           self.source_to_box(C, epsilons))
        archive += D
        self.assertIn(D, archive)
        self.assertNotIn(C, archive)
        # Add source E: box(E) == box(D), D et E nondominated, dist_E < dist_D
        self.assertEqual(self.source_to_box(D, epsilons),
                         self.source_to_box(E, epsilons))
        archive += E
        self.assertIn(E, archive)
        self.assertNotIn(D, archive)

    def test_dominated_distance_can_allow_solution_where_domination_not(self):
        """Test difference between distance based approach and domination approach.
        First one allows new solution easily.
        """
        epsilons = (2, 2, 2)
        # Create source with second objective to minimize
        D = self.create_source((4, -2, 7), MyFitness2)    # D
        E = self.create_source((3.6, -2, 8), MyFitness2)  # E
        # box(E) == box(D), D et E nondominated, dist_E < dist_D
        self.assertEqual(self.source_to_box(D, epsilons),
                         self.source_to_box(E, epsilons))
        # With distance based approach
        archive_dist = self.create_archive(epsilons, distance_based=True)
        archive_dist += D
        self.assertIn(D, archive_dist)
        archive_dist += E
        self.assertIn(E, archive_dist)  # New source added
        self.assertNotIn(D, archive_dist)
        # With domination approach
        archive_dom = self.create_archive(epsilons, distance_based=False)
        archive_dom += D
        self.assertIn(D, archive_dom)
        archive_dom += E
        self.assertIn(D, archive_dom)   # New source not added
        self.assertNotIn(E, archive_dom)

    def test_normalized_distance_make_sure_no_objective_more_important_than_another(self):
        """Test if using normalized_distance to True. Each objectives inside a same box
        must be equally important. An objective with more variation must be as important
        as another one with smaller variation according to epsilons values.
        """
        # More variation for the second objective but both as the same importance
        epsilons = (1, 10)

        # Two source with the same box value and same epsilon relative importance
        # but with or without normalization they are no dominance
        # First in stay in
        A = self.create_source((3.1, 38), MyFitness3)    # A
        B = self.create_source((3.8, 31), MyFitness3)    # B
        self.assertEqual(self.source_to_box(A, epsilons),
                         self.source_to_box(B, epsilons))
        # Create two archive with and without normalization of objectives
        archive_norm = self.create_archive(epsilons, box_normalized=True)  # Normalization
        archive = self.create_archive(epsilons, box_normalized=False)      # No normalization
        # Check solutions inside archive WITH normalization
        archive_norm += A
        archive_norm += B
        self.assertNotIn(B, archive_norm)
        self.assertIn(A, archive_norm)
        # Check solutions inside archive WITHOUT normalization
        archive += A
        archive += B
        self.assertNotIn(B, archive)
        self.assertIn(A, archive)

        # `C` closer to box high right corner relatively to epsilon variation
        C = self.create_source((3.9, 33), MyFitness3)    # A
        D = self.create_source((3.3, 38), MyFitness3)    # B
        self.assertEqual(self.source_to_box(C, epsilons),
                         self.source_to_box(D, epsilons))
        # Create two archive with and without normalization of objectives
        archive_norm = self.create_archive(epsilons, box_normalized=True)  # Normalization
        archive = self.create_archive(epsilons, box_normalized=False)      # No normalization
        # WITHOUT normalization second objective is more important due to higher value
        archive += C
        archive += D
        self.assertIn(D, archive)
        self.assertNotIn(C, archive)
        # With normalization each objectives are equally important
        archive_norm += C
        archive_norm += D
        self.assertNotIn(D, archive_norm)
        self.assertIn(C, archive_norm)

# State
    def test_archive_not_defined_if_empty(self):
        archive = self.create_archive()
        first_source = self.create_source((2, 2, 5))
        self.assertFalse(bool(archive))
        self.assertFalse(archive.is_define)
        archive.add(first_source)
        self.assertIn(first_source, archive)
        self.assertTrue(bool(archive))
        self.assertTrue(archive.is_define)

    def test_to_delete_attribute_always_empty_outside_add_function(self):
        archive = self.create_archive()
        self.assertFalse(archive._to_delete)
        first_source = self.create_source((2, 2, 5))
        self.assertFalse(bool(archive))
        self.assertFalse(archive.is_define)
        archive.add(first_source)
        self.assertFalse(archive._to_delete)

# Operators
    def test_archive_does_not_support_hashing(self):
        error_text = "unhashable type: 'EpsilonArchive'"
        archive = self.create_archive()
        with self.assertRaises(TypeError) as cm:
            hash(archive)
        self.assertEqual(cm.exception.args[0], error_text)

    def test_archive_with_same_source_and_epsilons_are_identical(self):
        # Empty archive with same epsilons are equals
        archive1 = self.create_archive()
        archive2 = self.create_archive()
        self.assertEqual(archive1, archive2)
        # No empty archive with same source and epsilons are equals
        first_source = self.create_source((2, 2, 5))
        archive1.add(first_source)
        archive2.add(first_source)
        self.assertEqual(archive1, archive2)

    def test_archive_with_same_source_and_epsilons_but_different_flag_are_equals(self):
        # Empty archive with same epsilons are equals
        archive1 = self.create_archive()
        archive2 = self.create_archive(distance_based=False, box_normalized=False)
        self.assertEqual(archive1, archive2)
        # No empty archive with same source and epsilons are equals
        # even if flags are different
        first_source = self.create_source((2, 2, 5))
        archive1.add(first_source)
        archive2.add(first_source)
        self.assertEqual(archive1, archive2)

    def test_archive_with_same_source_but_different_epsilons_are_not_identical(self):
        # Empty archive with same epsilons are equals
        archive1 = self.create_archive()
        archive2 = self.create_archive((2, 2, 2))
        self.assertNotEqual(archive1, archive2)
        # No empty archive with same source and epsilons are equals
        first_source = self.create_source((2, 2, 5))
        archive1.add(first_source)
        archive2.add(first_source)
        self.assertNotEqual(archive1, archive2)

    def test_archive_with_same_epsilons_but_different_sources_are_not_identical(self):
        # Empty archive with same epsilons are equals
        archive1 = self.create_archive()
        archive2 = self.create_archive()
        first_source = self.create_source((2, 2, 5))
        second_source = self.create_source((2, 2, 4))
        archive1.add(first_source)
        archive2.add(second_source)
        self.assertNotEqual(archive1, archive2)

    def test_adding_two_archive_return_another_archive(self):
        archive1 = self.create_archive()
        archive2 = self.create_archive()
        first_source = self.create_source((2, 2, 5))
        second_source = self.create_source((3, 2, 4))
        archive1.add(first_source)
        archive2.add(second_source)
        # Adding two archive does not change existing archives
        archive3 = archive1 + archive2
        self.assertIn(first_source, archive1)
        self.assertIn(second_source, archive2)
        self.assertIn(first_source, archive3)
        self.assertIn(second_source, archive3)
        self.assertTrue(len(archive3) == 2)

    def test_adding_iterable_with_archive_return_another_archive(self):
        archive1 = self.create_archive()
        first_source = self.create_source((2, 2, 5))
        second_source = self.create_source((3, 2, 4))
        archive1.add(first_source)
        alist = [second_source]
        # Adding two archive does not change existing archives
        archive3 = archive1 + alist
        self.assertIn(first_source, archive1)
        self.assertIn(first_source, archive3)
        self.assertIn(second_source, archive3)
        self.assertTrue(len(archive3) == 2)
        self.assertIsInstance(archive3, EpsilonArchive)
        archive4 = alist + archive1
        self.assertIn(first_source, archive1)
        self.assertIn(first_source, archive4)
        self.assertIn(second_source, archive4)
        self.assertTrue(len(archive4) == 2)
        self.assertIsInstance(archive4, EpsilonArchive)

    def test_can_add_new_source_using_plus_operator_left_untouched_original(self):
        archive1 = self.create_archive()
        first_source = self.create_source((2, 2, 5))
        second_source = self.create_source((3, 2, 4))
        archive1.add(first_source)
        # Can add another source with archive on the left side
        archive3 = archive1 + second_source
        self.assertIn(first_source, archive1)
        self.assertIn(first_source, archive3)
        self.assertIn(second_source, archive3)
        # Make sure second_source not in archive1
        self.assertNotIn(second_source, archive1)
        # Check source added to archive3
        self.assertTrue(len(archive3) == 2)
        # Check new object is an EpsilonArchive
        self.assertIsInstance(archive3, EpsilonArchive)
        # Can add another source with archive on the right side
        archive4 = second_source + archive1
        self.assertIn(first_source, archive1)
        self.assertIn(first_source, archive4)
        self.assertIn(second_source, archive4)
        # Make sure second_source not in archive1
        self.assertNotIn(second_source, archive1)
        # Check source added to archive4
        self.assertTrue(len(archive4) == 2)
        # Check new object is an EpsilonArchive
        self.assertIsInstance(archive4, EpsilonArchive)

    def test_cannot_add_non_iterable_other_than_source_with_EpsilonArchive(self):
        error_text = "<class 'int'> is not supported."
        archive1 = self.create_archive()
        first_source = self.create_source((2, 2, 5))
        archive1.add(first_source)
        non_iter = 33
        with self.assertRaises(TypeError) as cm:
            archive1 + non_iter
        self.assertEqual(cm.exception.args[0], error_text)

    def test_compare_archive_with_other_object_return_false(self):
        archive = self.create_archive()
        first_source = self.create_source((2, 2, 5))
        archive.add(first_source)
        self.assertFalse(archive == int())
        self.assertFalse(archive == tuple())
        self.assertFalse(archive == (first_source, ))
        self.assertFalse(archive == [first_source, ])

# Representation
    def test_repr_allow_to_create_the_same_instance(self):
        text = "EpsilonArchive((1, 1, 1), [Source(fitness=MyFitness((2.0, 2.0, 5.0)))])"
        archive = self.create_archive()
        first_source = self.create_source((2, 2, 5))
        archive.add(first_source)
        self.assertEqual(repr(archive), text)
        self.assertEqual(eval(repr(archive)), archive)

    def test_str(self):
        text = "[Source(fitness=MyFitness((2.0, 2.0, 5.0)))]"
        archive = self.create_archive()
        first_source = self.create_source((2, 2, 5))
        archive.add(first_source)
        self.assertEqual(str(archive), text)

    def test_EpsilonArchive_len_return_archive_attribute_len(self):
        archive = self.create_archive()
        first_source = self.create_source((2, 2, 5))
        archive.add(first_source)
        self.assertEqual(len(archive), len(archive.archive))
        self.assertEqual(len(archive), 1)

# Other
    def test_pick_random_source_from_archive_return_source_from_archive(self):
        archive = self.create_archive(distance_based=False, box_normalized=False)
        first_source = self.create_source((2, 2, 5))
        archive.add(first_source)
        self.assertIn(archive.pick_random(), archive)

    def test_remove_cluster_from_let_archive_intact_but_reduce_archive_size(self):
        # Make random generation each time the same
        random.seed(123214)  # 17 solutions
        # Create an archive with a lot of solution
        archive = self.create_archive(distance_based=True, box_normalized=True)
        for _ in range(1000):
            x, y, z = random.randint(1, 100), random.randint(1, 100), random.randint(1, 100)
            source = self.create_source(values=(x, y, z))
            archive += source
        self.assertEqual(len(archive), 17)
        clusterless_archive = EpsilonArchive.remove_clusters_from(archive, epsilons=(5, 5, 5))
        # Less source inside the archive ...
        self.assertTrue(len(archive) > len(clusterless_archive))
        self.assertEqual(len(clusterless_archive), 7)
        # but same epsilons values
        self.assertEqual(archive.epsilons, clusterless_archive.epsilons)
        # and archive remains untouched
        self.assertEqual(len(archive), 17)

# Copy
    def test_swallow_copy_has_no_reference_from_parent(self):
        archive = self.create_archive(distance_based=False, box_normalized=False)
        first_source = self.create_source((2, 2, 5))
        archive.add(first_source)
        archive_c = archive.copy()
        self.assertEqual(archive, archive_c)
        self.assertIsNot(archive, archive_c)
        second_source = self.create_source((4, 2, 3))
        archive.add(second_source)
        self.assertNotEqual(archive, archive_c)
        self.assertIsNot(archive, archive_c)
        # Make sure flags are the same
        self.assertEqual(archive._box_normalized, archive_c._box_normalized)
        self.assertEqual(archive._distance_based, archive_c._distance_based)

    def test_deep_copy_has_no_reference_from_parent(self):
        archive = self.create_archive(distance_based=False, box_normalized=False)
        first_source = self.create_source((2, 2, 5))
        archive.add(first_source)
        archive_c = archive.clone()
        self.assertEqual(archive, archive_c)
        self.assertIsNot(archive, archive_c)
        second_source = self.create_source((4, 2, 3))
        archive.add(second_source)
        self.assertNotEqual(archive, archive_c)
        self.assertIsNot(archive, archive_c)
        # Make sure flags are the same
        self.assertEqual(archive._box_normalized, archive_c._box_normalized)
        self.assertEqual(archive._distance_based, archive_c._distance_based)


# Run all tests
if __name__ == '__main__':
    unittest.main()
