# -*- coding:Utf8 -*-

"""
    How to run it ?
        - First go to the project root folder of the package ("pyMayBee")
        - Run python unittest command line ("python3 -m unittest")

    cd /home/pampi/Documents/Projects/pyMayBee
    python3 -m unittest tests/test_bees.py

"""

import unittest
import random

from pyMayBee.Primitive.archive import EpsilonArchive
from pyMayBee.Primitive.fitness import Fitness, FitnessConstrained
from pyMayBee.Primitive.criterion import QualitativeCriterion, DiscreteCriterion, ContinuousCriterion
from pyMayBee.Entity import (FoodSource,
                             OnlookerBee, EmployedBee, OBLBee,
                             ScoutBee, ScoutLevyBee, EmployedLevyBee, OnlookerLevyBee)
from pyMayBee.Entity.bees import BaseBee

from pyMayBee.Misc.random_helper import init_roulette
from pyMayBee.exceptions import AbcAttributeError


class MockHive():

    """Mock of an hive. Only archive attribute is relevant."""

    def __init__(self):
        # Keep track of best solutions
        self.archive = EpsilonArchive((1, 1, 1))

        # Fake sources in hive
        self.sources = []

        # Fake weights probabilities
        self.weighted_prob = None

        # Fake nb_evaluation
        self.nb_evaluation = 0


class MyFitness(Fitness):
    __slots__ = ()
    weights = (1, 1, 1)


class MyConsFitness(FitnessConstrained):
    __slots__ = ()
    weights = (1, 1, 1)


class BaseBeeUnitTests(unittest.TestCase):

    """
        Tests unit for the **BaseBee** class implementation.
    """

# USEFUL FUNCTIONS
    def setUp(self):
        """
            Executing prior to each tasks.
        """
        # Fitness instance
        self.MyFitness = MyFitness
        self.fitness_weights = MyFitness.weights
        # Fitness with constraints instance
        self.MyConsFitness = MyConsFitness
        self.fitness_cons_weights = MyConsFitness.weights
        # An archive instance
        self.base_archive = EpsilonArchive((1, 1, 1))

        # Seed random generator to always act the same
        random.seed(1234)

    def tearDown(self):
        """
            Executing after each task.
            Clean environment.
        """
        # Reset Modification rate
        EmployedBee.MODIF_RATE = 0.5

# TOOLS (function which can be used to avoid repeated stuff)
    def create_fitness(self, values=(3, 3, 3)):
        return self.MyFitness(values), values

    def create_fitness_with_constraints(self, values=(3, 3, 3)):
        return self.MyConsFitness(values), values

    def create_fitness_class(self, weights=(-1, 2, 3), slots=()):
        class AnotherFitness(Fitness):
            __slots__ = slots
        AnotherFitness.set_weights(weights)
        return AnotherFitness

    def continuous(self, name="continuous_instance", value=None, low_bound=0, up_bound=1):
        """Return a **Continuous** Criterion instance."""
        return ContinuousCriterion(name=name, value=value, low_bound=low_bound, up_bound=up_bound)

    def discrete(self, name="discrete_instance", value=None,
                 steps=tuple(range(30))):
        """Return a **Discrete** Criterion instance."""
        return DiscreteCriterion(name=name, value=value, steps=steps)

    def qualitative(self, name="qualitative_instance", value=None,
                    steps=("Red", "Blue", "Black", "Green", "Purple", "Other", "Yellow", "Brown")):
        """Return a **Qualitative** Criterion instance."""
        return QualitativeCriterion(name=name, value=value, steps=steps)

    def create_position(self, sequence=True):
        """Create a sequence or a dict of criterion based on sequence parameter."""
        position = []
        position.append(self.discrete('criterion 0'))
        position.append(self.discrete('criterion 1'))
        position.append(self.qualitative('criterion 2'))
        position.append(self.qualitative('criterion 3'))
        position.append(self.continuous('criterion 4'))
        if sequence:
            return position
        else:
            return {pos.name: pos for pos in position}

    def create_source(self, FitnessClass=MyFitness):
        position = self.create_position(False)
        return FoodSource(position, FitnessClass)

    def create_hive(self, fitness_values=None):
        fitness_values = ((1, 2, 1), (1, 1, 2), (2, 1, 1)) if fitness_values is None else fitness_values
        hive = MockHive()
        # Add source to archive to be able to select a random one
        for fitness in fitness_values:
            source = self.create_source(MyFitness(fitness))
            source.add_to(hive.archive)
        return hive, len(hive.archive)


# TESTS (Must start with <test_>)


# Init
    def test_init_Bee(self):
        hive, nb_source = self.create_hive()
        source = self.create_source()
        BaseBee(hive, source)


# Attribute
    def test_attribute_access(self):
        hive, nb_source = self.create_hive()
        source = self.create_source()
        bee = BaseBee(hive, source)
        # Source properties
        self.assertEqual(source.fitness, bee.source_fitness)
        self.assertEqual(source.position, bee.source_position)
        # Bee properties
        self.assertEqual(bee.fitness, bee._fitness)
        self.assertEqual(bee.position, bee._position)

    def test_setting_fitness_value_of_source_mark_source_as_evaluated(self):
        hive, nb_source = self.create_hive()
        source = self.create_source()
        bee = BaseBee(hive, source)
        self.assertTrue(source.evaluated is False)
        self.assertEqual(source.fitness, bee.source_fitness)
        # Set value of source fitness
        bee.source_fitness = (11, 1, 11)
        self.assertEqual(source.fitness, bee.source_fitness)
        self.assertTrue(source.evaluated is True)

# Methods
    def test_bee_can_be_added_to_archive(self):
        hive, nb_source = self.create_hive()
        source = self.create_source(MyFitness((1, 2, 3)))
        bee = BaseBee(hive, source)
        # Fake fitness evaluation attribution
        bee.fitness.values = (1, 2, 3)
        # Cannot add bee if fitness not yet initialize
        bee.add_to_archive()

    def test_update_replace_source_and_reset_trial_if_bee_better(self):
        # Init a hive a source and a bee
        hive, nb_source = self.create_hive()
        source = self.create_source(MyFitness((1, 2, 3)))
        source.increment_trial()
        source.increment_trial()
        # Trial is incremented twice
        self.assertEqual(source.trial, 2)
        # Bee change position but source remains the same
        bee = EmployedBee(hive, source)
        # Fake fitness evaluation attribution to bee position of bee
        bee.fitness.values = (3, 3, 3)
        self.assertFalse(bee.fitness == bee.source_fitness)
        # Bee fitness better
        bee.update_source()
        # Bee and source fitness now the same ...
        self.assertTrue(bee.fitness == bee.source_fitness)
        self.assertTrue(bee.position == bee.source_position)
        # ... and trial is reseted because source updated
        self.assertTrue(bee.source.trial == 0)

    def test_update_not_replace_source_and_increment_trial_if_source_better(self):
        # Init a hive a source and a bee
        hive, nb_source = self.create_hive()
        source = self.create_source(MyFitness((1, 2, 3)))
        source.increment_trial()
        source.increment_trial()
        # Bee change position but source remains the same
        bee = EmployedBee(hive, source)
        # Trial is incremented twice
        self.assertEqual(bee.source.trial, 2)
        # Fake fitness evaluation attribution to bee position of bee
        bee.fitness.values = (1, 1, 4)
        self.assertFalse(bee.fitness == bee.source_fitness)
        # Bee fitness better
        bee.update_source()
        # Bee and source fitness remains different ...
        self.assertFalse(bee.fitness == bee.source_fitness)
        self.assertFalse(bee.position == bee.source_position)
        # ... and trial is incremented because source not updated
        self.assertTrue(bee.source.trial == 3)

    def test_can_update_bee_fitness(self):
        hive, nb_source = self.create_hive()
        source = self.create_source(MyFitness((1, 2, 3)))
        bee = BaseBee(hive, source)
        bee.fitness = MyFitness(values=(1, 1, 1))
        self.assertTrue(bee.fitness == MyFitness(values=(1, 1, 1)))
        del bee.fitness
        self.assertTrue(bee.fitness == MyFitness())
        with self.assertRaises(AbcAttributeError):
            bee.fitness = (1, 2, 4)
        bee.fitness.values = (11, 11, 11)
        self.assertTrue(bee.fitness.values == (11, 11, 11))

    def test_OBLBee_added_to_archive_add_source_also(self):
        hive, nb_source = self.create_hive()
        source = self.create_source(MyFitness((1, 2, 3)))
        bee = OBLBee(hive, source)
        # Update bee fitness
        bee.fitness.values = (1, 2, 4)
        # Add and check both are in archive
        bee.add_to_archive()

    def test_at_least_one_criterion_is_updated_with_0_as_MODIF_RATE(self):
        class EmployedBeeOneChange(EmployedBee):
            """Only one change is allowed."""
            MODIF_RATE = 0
            pass
        hive = MockHive()
        # Create a source with limited criterion variation
        position = [self.discrete(steps=(1, 2, 3, 4))]
        source = FoodSource(position, MyFitness)
        source.add_to(hive.archive)
        position = [self.discrete(steps=(1, 2, 3, 4))]
        source = FoodSource(position, MyFitness)
        source.add_to(hive.archive)
        # Create a lot of bees
        for x in range(5):
            bee = EmployedBeeOneChange(hive, source)
            self.assertNotEqual(bee.position, source.position)

# Run
    def test_OBLBee(self):
        hive, nb_source = self.create_hive()
        self.assertTrue(hive.nb_evaluation == 0)
        source = self.create_source()
        source.fitness.values = (3, 0.5, 1)
        bee = OBLBee(hive, source)
        self.assertIs(source, bee.source)
        self.assertTrue(source.position != bee.position)
        # Hive number of evaluation must have been incremented
        self.assertTrue(hive.nb_evaluation == 1)

    def test_ScoutBee(self):
        hive, nb_source = self.create_hive()
        self.assertTrue(hive.nb_evaluation == 0)
        source = self.create_source()
        source.fitness.values = (3, 0.5, 1)
        source.increment_trial()
        source.increment_trial()
        self.assertTrue(source.trial == 2)
        bee = ScoutBee(hive, source)
        self.assertTrue(source.position != bee.position)
        # Bee source only reference of source
        self.assertTrue(source.trial == bee.source.trial)
        # Bee is reseted with bee creation
        self.assertTrue(bee.source.trial == 0)
        # Hive number of evaluation must have been incremented
        self.assertTrue(hive.nb_evaluation == 2)

    def test_ScoutLevyBee(self):
        hive, nb_source = self.create_hive()
        self.assertTrue(hive.nb_evaluation == 0)
        source = self.create_source()
        source.fitness.values = (3, 0.5, 1)
        source.increment_trial()
        source.increment_trial()
        self.assertTrue(source.trial == 2)
        bee = ScoutLevyBee(hive, source)
        self.assertTrue(source.position != bee.position)
        # Bee source only reference of source
        self.assertTrue(source.trial == bee.source.trial)
        # Bee is reseted with bee creation
        self.assertTrue(bee.source.trial == 0)
        # Hive number of evaluation must have been incremented
        self.assertTrue(hive.nb_evaluation == 2)

    # def test_ScoutBee_position_always_different_from_old(self):
    #     lines 156, 184, 399
    #     hive, nb_source = self.create_hive()
    #     random.seed(123)
    #     position = []
    #     position.append(self.discrete('ddd', steps=[1, 2, 3]))
    #     position.append(self.discrete('eee', steps=[1, 2, 3]))
    #     source = FoodSource(position, MyFitness)
    #     random.seed(1)
    #     source.position['ddd'] = source.position['ddd'](new_value=1)
    #     source.position['eee'] = source.position['eee'](new_value=3)
    #     random.seed(1)
    #     bee = ScoutBee(hive, source)
    #     print(bee.source_position)

    def test_EmployedBee(self):
        hive, nb_source = self.create_hive()
        self.assertTrue(hive.nb_evaluation == 0)
        source = self.create_source()
        source.fitness.values = (3, 0.5, 1)
        bee = EmployedBee(hive, source, scale_uniform=0.133)
        self.assertIs(source, bee.source)
        # Hive number of evaluation must have been incremented
        self.assertTrue(hive.nb_evaluation == 1)
        # Check arguments are assigned
        self.assertEqual(bee.scale_uniform, 0.133)

    def test_EmployedLevyBee(self):
        hive, nb_source = self.create_hive()
        self.assertTrue(hive.nb_evaluation == 0)
        source = self.create_source()
        source.fitness.values = (3, 0.5, 1)
        bee = EmployedLevyBee(hive, source, scale_levy=0.03, beta=1.2)
        self.assertIs(source, bee.source)
        # Hive number of evaluation must have been incremented
        self.assertTrue(hive.nb_evaluation == 1)
        # Check arguments are assigned
        self.assertEqual(bee.scale_levy, 0.03)
        self.assertEqual(bee.beta, 1.2)

    def test_OnlookerLevyBee(self):
        hive, nb_source = self.create_hive()
        self.assertTrue(hive.nb_evaluation == 0)
        # Fake sources in Hive
        fitness_values = ((1, 2, 1), (1, 1, 2), (2, 1, 1))
        for fitness in fitness_values:
            source = self.create_source(MyFitness(fitness))
            hive.sources.append(source)
        # Fake weights probabilities in hive
        hive.weighted_prob = init_roulette([1 for _ in range(nb_source)])
        bee = OnlookerLevyBee(hive, scale_levy=0.02, beta=1.4)
        # Hive number of evaluation must have been incremented
        self.assertTrue(hive.nb_evaluation == 1)
        # Check arguments are assigned
        self.assertEqual(bee.scale_levy, 0.02)
        self.assertEqual(bee.beta, 1.4)

    def test_OnlookerBee(self):
        hive, nb_source = self.create_hive()
        self.assertTrue(hive.nb_evaluation == 0)
        # Fake sources in Hive
        fitness_values = ((1, 2, 1), (1, 1, 2), (2, 1, 1))
        for fitness in fitness_values:
            source = self.create_source(MyFitness(fitness))
            hive.sources.append(source)
        # Fake weights probabilities in hive
        hive.weighted_prob = init_roulette([1 for _ in range(nb_source)])
        bee = OnlookerBee(hive, scale_uniform=0.12)
        # Hive number of evaluation must have been incremented
        self.assertTrue(hive.nb_evaluation == 1)
        # Check arguments are assigned
        self.assertEqual(bee.scale_uniform, 0.12)

# Corner case
    def test_prevent_random_walk_if_criterion_is_0_and_archive_with_unique_source(self):
        # Same position for source and archive source
        # with one value to 0 to reach corner case
        position = []
        position.append(self.discrete('criterion 0', value=1))
        position.append(self.discrete('criterion 1', value=1))
        position.append(self.qualitative('criterion 2', value='Red'))
        position.append(self.qualitative('criterion 3', value='Red'))
        position.append(self.continuous('criterion 4', value=0))
        # Source creation
        source = FoodSource(position, MyFitness)
        source.fitness.values = (1, 1, 1)
        # Add same to archive
        hive = MockHive()
        source.add_to(hive.archive)
        nb_source = len(hive.archive)
        # Test archive size
        self.assertEqual(nb_source, 1)
        # Test archive source is the same as source
        self.assertEqual(source, [source for source in hive.archive][0])
        self.assertTrue(hive.nb_evaluation == 0)
        # Employed Bee
        # Make sur change occurs for each criterion
        EmployedBee.MODIF_RATE = 2
        bee = EmployedBee(hive, source)
        self.assertIs(source, bee.source)
        self.assertTrue(source.position != bee.position)
        # Hive number of evaluation must have been incremented
        self.assertTrue(hive.nb_evaluation == 1)

    def test_prevent_levy_flight_if_criterion_is_0_and_archive_with_unique_source(self):
        # Same position for source and archive source
        # with one value to 0 to reach corner case
        position = []
        position.append(self.discrete('criterion 0', value=1))
        position.append(self.discrete('criterion 1', value=1))
        position.append(self.qualitative('criterion 2', value='Red'))
        position.append(self.qualitative('criterion 3', value='Red'))
        position.append(self.continuous('criterion 4', value=0))
        # Source creation
        source = FoodSource(position, MyFitness)
        source.fitness.values = (1, 1, 1)
        # Add same to archive
        hive = MockHive()
        source.add_to(hive.archive)
        nb_source = len(hive.archive)
        # Test archive size
        self.assertEqual(nb_source, 1)
        # Test archive source is the same as source
        self.assertEqual(source, [source for source in hive.archive][0])
        self.assertTrue(hive.nb_evaluation == 0)
        # Employed Lévy Bee
        # Make sur change occurs for each criterion
        EmployedLevyBee.MODIF_RATE = 2
        bee = EmployedLevyBee(hive, source)
        self.assertIs(source, bee.source)
        self.assertTrue(source.position != bee.position)
        # Hive number of evaluation must have been incremented
        self.assertTrue(hive.nb_evaluation == 1)


# Run all tests
if __name__ == '__main__':
    unittest.main()
