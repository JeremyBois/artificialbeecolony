# -*- coding:Utf8 -*-

"""
    How to run it ?
        - First go to the project root folder of the package ("pyMayBee")
        - Run python unittest command line ("python3 -m unittest")

    cd /home/pampi/Documents/Projects/pyMayBee
    python3 -m unittest tests/test_fitness.py


    À vérifier:
        - Pickle marche en dehors de la suite de test. Le problème vient de la définition de
          classes dans une class (Unittest class...)
        - Penalization seems to gentle when rate close to 0
"""

import unittest

# import pickle
from pyMayBee.Primitive.fitness import Fitness, FitnessConstrained, fitness_factory
from pyMayBee.exceptions import ClassAttributeError
from operator import mul


class FitnessUnitTests(unittest.TestCase):

    """
        Test unit for the **Fitness** class implementation.
    """

# USEFUL FUNCTIONS

    def setUp(self):
        """
            Executing prior to each tasks.
        """
        class MyFitness(Fitness):
            __slots__ = ()
            weights = (-1, 2, 3)

        self.MyFitness = MyFitness
        self.fitness_weights = MyFitness.weights

    def tearDown(self):
        """
            Executing after each task.
            Clean environment.
        """
        pass


# TOOLS (function which can be used to avoid repeated stuff)

    def create_instance(self, values=(3, 3, 3)):
        return self.MyFitness(values), values

    def create_class(self, weights=(-1, 2, 3), slots=()):
        class AnotherFitness(Fitness):
            __slots__ = slots
        AnotherFitness.set_weights(weights)
        return AnotherFitness


# TESTS (Must start with <test_>)

# Init
    def test_can_create_fitness_using_factory(self):
        FitnessClass1 = fitness_factory((-1, -3, 2), Fitness, 'FitnessClass1')
        instance1 = FitnessClass1((1, 1, 1))

        class FitnessClass2(Fitness):
            __slots__ = ()
            weights = (-1, -3, 2)
        instance2 = FitnessClass2((1, 1, 1))
        self.assertEqual(instance1, instance2)

    def test_create_instance_with_values(self):
        objectives, values = self.create_instance()
        weights_values = tuple((el for el in map(mul, values, self.fitness_weights)))
        self.assertEqual(objectives.w_values, weights_values)
        # A instance is define if values are set
        self.assertEqual(objectives.is_define, True)
        self.assertEqual(bool(objectives), True)
        # Cannot create an instance where len(values) != len(weights)
        with self.assertRaises(ValueError):
            objectives, _ = self.create_instance((2, 3))
        # Cannot instantiate Abstract class
        with self.assertRaises(TypeError):
            objectives, _ = Fitness((2, 3))
        self.assertTrue(objectives.is_feasible())

    def test_create_instance_without_values(self):
        objectives, _ = self.create_instance(values=())
        # Empty values works but instance marked as undefine
        self.assertFalse(objectives)
        self.assertEqual(objectives.values, ())
        values = (1, 2, 3)
        weights_values = tuple((el for el in map(mul, values, self.fitness_weights)))
        # With values instance is now defined
        objectives.values = values
        self.assertEqual(objectives.w_values, weights_values)
        self.assertEqual(objectives.values, values)
        self.assertTrue(objectives)
        self.assertTrue(objectives.is_feasible())

# Update instance attribute
    def test_update_values(self):
        objectives, _ = self.create_instance()
        # Test if we can update values attribute
        values = (2, 4, 5)
        objectives.values = values
        weights_values = tuple((el for el in map(mul, values, self.fitness_weights)))
        self.assertEqual(objectives.w_values, weights_values)
        self.assertEqual(objectives.is_define, True)
        # Cannot update values if length does not match
        with self.assertRaises(ValueError):
            objectives.values = (1, 3)  # Correct type but wrong size
        # Cannot update values with a non iterable
        with self.assertRaises(TypeError):
            objectives.values = 44      # Wrong type

    def test_del_values(self):
        objectives, _ = self.create_instance()
        del objectives.values
        self.assertEqual(objectives.values, ())
        self.assertNotEqual(objectives.values, None)

    def test_iteration(self):
        objectives1, values = self.create_instance((1, -11, 22))
        iteration = tuple(obj for obj in objectives1)
        self.assertEqual(iteration, objectives1.values)
        objectives2, values = self.create_instance((1, -11, 22))
        self.assertEqual(objectives1, objectives2)

    def test_Fitness_attribute_locked(self):
        objectives, _ = self.create_instance()
        with self.assertRaises(AttributeError):
            objectives.new_attribute = 44

# Update class attribute
    def test_get_set_shared_weight(self):
        objectives1, _ = self.create_instance()
        objectives2, _ = self.create_instance((2, 4, 2))
        # Check if weights are shared
        self.assertIs(objectives1.get_weights(), objectives2.get_weights())

    def test_set_or_delete_weighted_values(self):
        objectives1, _ = self.create_instance()
        with self.assertRaises(AttributeError):
            objectives1.w_values = (1, 2, 4)
        with self.assertRaises(AttributeError):
            del objectives1.w_values

    def test_cannot_set_weight_with_zero_using_set_weights_class_method(self):
        with self.assertRaises(ValueError):
            self.create_class((1, 3, 0))

    def test_new_weights_must_be_an_iterable(self):
        error_msg = "Attribute weights of AnotherFitness must be an iterable."
        with self.assertRaises(TypeError) as cm:
            self.create_class(33)
        self.assertEqual(cm.exception.args[0], error_msg)

    def test_warning_raise_if_setting_weights_from_instance(self):
        error_msg = "You must not changed MyFitness.weights after an instance is created."
        objectives1, _ = self.create_instance()
        # Raise an error if we change weights with an instance already define
        with self.assertRaises(ClassAttributeError) as cm:
            objectives1.set_weights((1, 3, 1))
        self.assertEqual(cm.exception.args[0], error_msg)

# Representation
    def test_repr(self):
        objectives1, values = self.create_instance((1, -11, 22))
        self.assertEqual(repr(objectives1), "MyFitness((1.0, -11.0, 22.0))")

    def test_str(self):
        objectives1, values = self.create_instance((1, -11, 22))
        w_values = objectives1.w_values
        values = tuple((el for el in map(float, values)))
        text = "MyFitness({}, {})".format(values, w_values)
        self.assertEqual(str(objectives1), text)

    def test_Fitness_is_mutable_and_does_support_hashing(self):
        objectives1, _ = self.create_instance((1, -11, 22))
        self.assertEqual(hash(objectives1), hash(objectives1.w_values))
        set_test = set()
        set_test.add(objectives1)
        set_test.add(objectives1)
        set_test.add(objectives1)
        self.assertTrue(len(set_test) == 1)
        objectives1.values = (1, 222, 11)
        self.assertEqual(hash(objectives1), hash(objectives1.w_values))

# Operators
    def test_length(self):
        objectives1, values = self.create_instance((1, -11, 22))
        self.assertEqual(len(objectives1), len(values))

    def test_compare_fitness_with_another_type_evaluated_wvalues_as_tuple(self):
        Fitness_new = self.create_class(weights=(1, 1, 1))
        objectives1 = Fitness_new((1, 2, 3))
        # Fitness allow duct typing
        self.assertTrue(objectives1 == (1, 2, 3))
        self.assertTrue(objectives1 != (1, 1, 1))
        self.assertTrue(objectives1 > (1, 2, 2))
        self.assertTrue(objectives1 >= (1, 2, 1))
        self.assertTrue(objectives1 < (2, 2, 3))
        self.assertTrue(objectives1 <= (1, 2, 3))

    def test_dominate_with_positive_weights(self):
        # Dominance take effect on the weights values not values !
        self.MyFitness.weights = (1, 1, 1)
        objectives1, _ = self.create_instance((1, -11, 22))
        objectives2, _ = self.create_instance((1, -10, 22))
        objectives3, _ = self.create_instance((2, -10, 23))
        objectives4, _ = self.create_instance((2, -20, 23))
        objectives5, _ = self.create_instance((1, -11, 22))
        # If one value greater and the other are the same
        self.assertTrue(objectives2 > objectives1)
        self.assertTrue(objectives2.dominate(objectives1))
        # If all element of the first one are greater than the second
        self.assertTrue(objectives3 > objectives1)
        # If one element is smaller than the other there is no domination
        self.assertFalse(objectives4 > objectives1)
        # If all element are similar there is no dominance
        self.assertFalse(objectives5 > objectives1)

    def test_dominate_with_positive_and_negative_weights(self):
        # Dominance take effect on the weights values not values !
        self.MyFitness.weights = (1, -1, 1)
        objectives1, _ = self.create_instance((1, -10, 22))
        objectives2, _ = self.create_instance((1, 10, 22))
        objectives3, _ = self.create_instance((2, -20, 23))
        objectives4, _ = self.create_instance((2, 10, 22))
        # If one value greater and the other are the same
        self.assertTrue(objectives1 > objectives2)
        # If all element of the first one are greater than the second
        self.assertFalse(objectives1 > objectives3)
        # If one element is smaller than the other there is no domination
        self.assertFalse(objectives1 > objectives4)

    def test_is_dominated(self):
        # Dominance take effect on the weights values not values !
        self.MyFitness.weights = (1, -1, 1)
        objectives1, _ = self.create_instance((1, -10, 22))
        objectives2, _ = self.create_instance((1, 10, 22))
        objectives3, _ = self.create_instance((2, -20, 23))
        objectives4, _ = self.create_instance((2, 10, 22))
        # If one value greater and the other are the same
        self.assertTrue(objectives2 < objectives1)
        self.assertTrue(objectives2.is_dominated(objectives1))
        # If all element of the first one are smaller than the second
        self.assertFalse(objectives3 < objectives1)
        # If one element is smaller than the other there is no domination
        self.assertFalse(objectives4 < objectives1)

    def test_is_not_dominated(self):
        # Dominance take effect on the weights values not values !
        self.MyFitness.weights = (1, 1, 1)
        objectives1, _ = self.create_instance((1, -11, 23))
        objectives2, _ = self.create_instance((1, -10, 22))
        objectives3, _ = self.create_instance((2, -10, 22))
        objectives4, _ = self.create_instance((1, -11, 23))
        objectives5, _ = self.create_instance((2, -11, 23))
        # If both have a better fitness for an objective
        self.assertTrue(objectives1.is_not_dominated(objectives2))
        self.assertTrue(objectives2.is_not_dominated(objectives1))
        # If the first have more better fitness but the second has at least one strictly better
        self.assertTrue(objectives3.is_not_dominated(objectives1))
        self.assertTrue(objectives1.is_not_dominated(objectives3))
        # If both are the same
        self.assertTrue(objectives4.is_not_dominated(objectives1))
        self.assertTrue(objectives1.is_not_dominated(objectives4))
        # If the first dominate the second
        self.assertTrue(objectives5.is_not_dominated(objectives1))
        self.assertFalse(objectives1.is_not_dominated(objectives5))

    def test_equality_inequality(self):
        # Dominance take effect on the weights values not values !
        objectives1, _ = self.create_instance((1, -10, 22))
        objectives2, _ = self.create_instance((1, -10, 22))
        objectives3, _ = self.create_instance((1, 10, 22))
        self.assertTrue(objectives1 == objectives2)
        self.assertTrue(objectives1 != objectives3)

    def test_dominate_or_equal(self):
        # Dominance take effect on the weights values not values !
        objectives1, _ = self.create_instance((1, -10, 22))
        objectives2, _ = self.create_instance((1, -11, 22))
        objectives3, _ = self.create_instance((1, -10, 22))
        self.assertTrue(objectives1 >= objectives2)
        self.assertTrue(objectives1 >= objectives3)

    def test_is_dominated_or_equal(self):
        # Dominance take effect on the weights values not values !
        objectives1, _ = self.create_instance((1, -10, 22))
        objectives2, _ = self.create_instance((1, -8, 22))
        objectives3, _ = self.create_instance((1, -10, 22))
        self.assertTrue(objectives1 <= objectives2)
        self.assertTrue(objectives1 <= objectives3)

# Inheritance
    def test_class_childs_does_not_share_weights(self):
        # At start both child and parent must have the same class attribute
        Fitness_new = self.create_class(self.MyFitness.weights)
        self.assertEqual(Fitness_new.get_weights(), self.MyFitness.get_weights())

        # Setting weights for child does not change parent weights
        Fitness_new.weights = (22, 33)
        objectives_new = Fitness_new((2, 3))
        self.assertNotEqual(Fitness_new.get_weights(), self.MyFitness.get_weights())

        # New values can be added if the shape match new weights size
        values_new = (2, 3)
        self.assertEqual(objectives_new.values, values_new)

        # Weights values are update correctly
        weights_values_new = tuple((el for el in map(mul, values_new, Fitness_new.get_weights())))
        self.assertEqual(objectives_new.w_values, weights_values_new)

# Copy
    def test_swallow_copy_has_no_reference_from_parent(self):
        values = (1, -11, 22)
        objectives1, values = self.create_instance(values)
        objectives2 = objectives1.copy()
        self.assertEqual(objectives1, objectives2)
        self.assertEqual(objectives1.values, values)
        self.assertIsNot(objectives1, objectives2)
        # Check if it is a reference (IsNot test must fail but just in case)
        objectives2.values = (1, 2, 4)
        self.assertNotEqual(objectives1, objectives2)

    def test_deep_copy_has_no_reference_from_parent(self):
        values = (1, -11, 22)
        objectives1, values = self.create_instance(values)
        objectives2 = objectives1.clone()
        self.assertEqual(objectives1, objectives2)
        self.assertEqual(objectives1.values, values)
        self.assertIsNot(objectives1, objectives2)
        # Check if it is a reference (IsNot test must fail but just in case)
        objectives2.values = (1, 2, 4)
        self.assertNotEqual(objectives1, objectives2)


# Pickle
    # def test_pickled_instance_are_the_same_after_unpickled(self):
    #     objectives1, _ = self.create_instance((1, -11, 22))
    #     foo_pick = pickle.dumps(objectives1)
    #     objectives2 = pickle.loads(foo_pick)
    #     self.assertEqual(objectives1, objectives2)


class FitnessConstrainedUnitTests(unittest.TestCase):

    """
        Tests unit for the **FitnessConstrained** class implementation.
    """

# USEFUL FUNCTIONS

    def setUp(self):
        """
            Executing prior to each tasks.
        """
        class MyConsFitness(FitnessConstrained):
            __slots__ = ()
            weights = (1, 1, 1)

        class MyConsFitnessMinimize(FitnessConstrained):
            __slots__ = ()
            weights = (-1, -1, -1)

        self.MyConsFitness = MyConsFitness
        self.MyConsFitnessMinimize = MyConsFitnessMinimize
        self.fitness_weights = MyConsFitness.weights

    def tearDown(self):
        """
            Executing after each task.
            Clean environment.
        """
        # Maximization
        self.MyConsFitness.fitness_max = ()
        self.MyConsFitness.fitness_min = ()
        self.MyConsFitness.constraints_max = (0, )
        self.MyConsFitness.set_feasibility_rate(1)
        # Minimization
        self.MyConsFitnessMinimize.fitness_max = ()
        self.MyConsFitnessMinimize.fitness_min = ()
        self.MyConsFitnessMinimize.constraints_max = (0, )
        self.MyConsFitnessMinimize.set_feasibility_rate(1)


# TOOLS (function which can be used to avoid repeated stuff)

    def create_instance(self, values=(3, 3, 3), constraints=(0, ), mini=False):
        if mini is True:
            return self.MyConsFitnessMinimize(values, constraints), values, constraints
        else:
            return self.MyConsFitness(values, constraints), values, constraints

    def create_class(self, weights=(-1, 2, 3), slots=()):
        class AnotherFitness(FitnessConstrained):
            __slots__ = slots
        AnotherFitness.set_weights(weights)
        return AnotherFitness

# TESTS (Must start with <test_>)


# Init
    def test_create_instance_with_values(self):
        objectives, values, constraints = self.create_instance()
        weights_values = tuple((el for el in map(mul, values, self.fitness_weights)))
        self.assertEqual(objectives.w_values, weights_values)
        # A instance is define if values are set
        self.assertEqual(objectives.is_define, True)
        self.assertEqual(bool(objectives), True)
        self.assertEqual(objectives.constraints, (0, ))
        # Cannot create an instance where len(values) != len(weights)
        with self.assertRaises(ValueError):
            objectives, _, _ = self.create_instance((2, 3))

    def test_create_instance_without_values(self):
        objectives, _, _ = self.create_instance(values=())
        # Empty values works but instance marked as undefine
        self.assertFalse(objectives)
        self.assertEqual(objectives.values, ())
        values = (1, 2, 3)
        weights_values = tuple((el for el in map(mul, values, self.fitness_weights)))
        # With values instance is now defined
        objectives.values = values
        self.assertEqual(objectives.w_values, weights_values)
        self.assertEqual(objectives.values, values)
        self.assertTrue(objectives)

# Attribute
    def test_fitness_with_constraints_is_not_feasible(self):
        objectives, values, constraints = self.create_instance(constraints=(3, 5))
        # Objectives with constraints is not feasible
        self.assertEqual(objectives.constraints, (3, 5))
        self.assertFalse(objectives.is_feasible())
        # Fitness without constraints is feasible
        objectives.constraints = (0, 0)
        self.assertTrue(objectives.is_feasible())

    def test_norm_values_different_from_values(self):
        objectives, values, constraints = self.create_instance(constraints=(3, 5))
        self.MyConsFitness.set_bounds_for_fitness((1, 1, 1), (3, 3, 3), (3, 5))
        self.MyConsFitness.set_feasibility_rate(1)
        self.assertNotEqual(objectives.norm_values, objectives.values)
        result = 1
        self.assertEqual(objectives.norm_values, (result, result, result))

    # def test_handle_case_where_max_equals_min_fitness(self):
    #     objectives, values1, constraints1 = self.create_instance(values=(1, 2, 3),
    #                                                              constraints=(0, 0))
    #     self.MyConsFitness.set_bounds_for_fitness((1, 2, 3), (1, 2, 3), (0, 0))
    #     self.MyConsFitness.set_feasibility_rate(1)
    #     self.assertTrue(objectives.is_feasible())
    #     self.assertTrue(objectives.feasibility_rate == 1)
    #     self.assertEqual(objectives.norm_values,
    #                      (1.4142135623730951, 1.4142135623730951, 1.4142135623730951))

    def test_same_fitness_but_with_constraint_is_worst(self):
        objectives1, values1, constraints1 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(1, 0.5))
        objectives2, values2, constraints2 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(0, 0))
        # Assign class attribute which correspond
        self.MyConsFitness.set_bounds_for_fitness((1, 2, 3), (1, 2, 3), (100, 50))
        self.MyConsFitness.set_feasibility_rate(0.5)
        self.assertEqual(objectives1.w_values, objectives2.w_values)
        self.assertNotEqual(objectives1.norm_values, objectives2.norm_values)
        self.assertTrue(objectives2.norm_dominate(objectives1))

    def test_better_fitness_and_constraint_can_be_selected(self):
        objectives1, values1, constraints1 = self.create_instance(values=(4, 4, 4),
                                                                  constraints=(1, 0))
        objectives2, values2, constraints2 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(0, 0))
        # Assign class attribute which correspond
        self.MyConsFitness.set_bounds_for_fitness((1, 2, 3), (4, 4, 4), (10, 5))
        self.MyConsFitness.set_feasibility_rate(0.5)
        # Fitness with constraint is selected
        self.assertNotEqual(objectives1.w_values, objectives2.w_values)
        self.assertTrue(objectives1 > objectives2)
        self.assertFalse(objectives1.is_feasible())
        self.assertTrue(objectives1.norm_dominate(objectives2))

    def test_better_solution_but_with_constraint_can_be_declined(self):
        objectives1, values1, constraints1 = self.create_instance(values=(3, 3, 4),
                                                                  constraints=(10, 5))
        objectives2, values2, constraints2 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(0, 0))
        # Assign class attribute which correspond
        self.MyConsFitness.set_bounds_for_fitness((1, 2, 3), (3, 3, 4), (10, 5))
        self.MyConsFitness.set_feasibility_rate(0.5)
        # Fitness with constraint is discard
        self.assertNotEqual(objectives1.w_values, objectives2.w_values)
        # Objective 1 dominate objective 2 if constraint not accounted
        self.assertFalse(objectives1.is_feasible())
        self.assertTrue(objectives1 > objectives2)
        self.assertFalse(objectives1.norm_dominate(objectives2))
        self.assertTrue(objectives2.norm_dominate(objectives1))

    def test_constraint_can_handle_minimization(self):
        objectives1, values1, constraints1 = self.create_instance(values=(3, 3, 4),
                                                                  constraints=(10, 5),
                                                                  mini=True)
        objectives2, values2, constraints2 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(0, 0),
                                                                  mini=True)
        # Assign class attribute which correspond
        self.MyConsFitnessMinimize.set_bounds_for_fitness((1, 2, 3), (3, 3, 4), (10, 5))
        self.MyConsFitnessMinimize.set_feasibility_rate(0.5)
        # Fitness with constraint is discard
        self.assertNotEqual(objectives1.w_values, objectives2.w_values)
        # Objective 1 is dominated for both in minimization
        self.assertFalse(objectives1.is_feasible())
        self.assertTrue(objectives2 > objectives1)
        self.assertTrue(objectives2.norm_dominate(objectives1))
        self.assertFalse(objectives1.norm_dominate(objectives2))
        self.assertFalse(objectives2.is_norm_dominated(objectives1))
        self.assertTrue(objectives1.is_norm_dominated(objectives2))

    def test_same_fitness_but_with_constraint_is_worst_even_for_minimization(self):
        objectives1, values1, constraints1 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(1, 0.5),
                                                                  mini=True)
        objectives2, values2, constraints2 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(0, 0),
                                                                  mini=True)
        # Assign class attribute which correspond
        self.MyConsFitnessMinimize.set_bounds_for_fitness((1, 2, 3), (1, 2, 3), (10, 5))
        self.MyConsFitnessMinimize.set_feasibility_rate(0.5)
        self.assertEqual(objectives1.w_values, objectives2.w_values)
        self.assertNotEqual(objectives1.norm_values, objectives2.norm_values)
        self.assertTrue(objectives2.norm_dominate(objectives1))

    def test_works_if_no_constraint_for_minimization(self):
        objectives1, values1, constraints1 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(0, 0),
                                                                  mini=True)
        objectives2, values2, constraints2 = self.create_instance(values=(0.5, 2, 3),
                                                                  constraints=(0, 0),
                                                                  mini=True)
        # Assign class attribute which correspond
        self.MyConsFitnessMinimize.set_bounds_for_fitness((0.5, 2, 3), (1, 2, 3), (10, 5))
        self.MyConsFitnessMinimize.set_feasibility_rate(1)
        self.assertTrue(objectives2 > objectives1)
        self.assertTrue(objectives2.norm_dominate(objectives1))

    def test_works_if_no_constraint_for_maximization(self):
        objectives1, values1, constraints1 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(0, 0),
                                                                  mini=False)
        objectives2, values2, constraints2 = self.create_instance(values=(0.5, 2, 3),
                                                                  constraints=(0, 0),
                                                                  mini=False)
        # Assign class attribute which correspond
        self.MyConsFitness.set_bounds_for_fitness((0.5, 2, 3), (1, 2, 3), (10, 5))
        self.MyConsFitness.set_feasibility_rate(1)
        self.assertTrue(objectives1 > objectives2)
        self.assertTrue(objectives1.norm_dominate(objectives2))

    def test_both_solution_can_have_constraints(self):
        objectives1, values1, constraints1 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(1, 2),
                                                                  mini=True)
        objectives2, values2, constraints2 = self.create_instance(values=(0.5, 2, 3),
                                                                  constraints=(1, 0),
                                                                  mini=True)
        # Assign class attribute which correspond
        self.MyConsFitnessMinimize.set_bounds_for_fitness((0.5, 2, 3), (1, 2, 3), (10, 5))
        self.MyConsFitnessMinimize.set_feasibility_rate(0)
        self.assertTrue(objectives2 > objectives1)
        self.assertTrue(objectives2.norm_dominate(objectives1))

    def test_if_all_solution_constraint_only_constraint_matter(self):
        objectives1, values1, constraints1 = self.create_instance(values=(4, 4, 4),
                                                                  constraints=(10, 0))
        objectives2, values2, constraints2 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(0, 0))
        # # Assign class attribute which correspond
        self.MyConsFitness.set_bounds_for_fitness((1, 2, 3), (4, 4, 4), (10, 5))
        self.MyConsFitness.set_feasibility_rate(0.5)
        # Fitness with constraint is selected
        self.assertNotEqual(objectives1.w_values, objectives2.w_values)
        self.assertTrue(objectives1 > objectives2)
        self.assertFalse(objectives1.is_feasible())
        self.assertTrue(objectives1.norm_dominate(objectives2))
        # Fitness with constraint is not selected
        self.MyConsFitness.set_feasibility_rate(0)
        self.assertNotEqual(objectives1.w_values, objectives2.w_values)
        self.assertTrue(objectives1 > objectives2)
        self.assertFalse(objectives1.is_feasible())
        self.assertFalse(objectives1.norm_dominate(objectives2))

    def test_if_feasibility_rate_lower_than_05_percent_infeasible_cannot_be_taken(self):
        objectives1, values1, constraints1 = self.create_instance(values=(35, 35, 35),
                                                                  constraints=(10, 2))
        objectives2, values2, constraints2 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(0, 0))
        # # Assign class attribute which correspond
        self.MyConsFitness.set_bounds_for_fitness((1, 2, 3), (40, 40, 40), (10, 5))
        self.MyConsFitness.set_feasibility_rate(0.05)
        # 1 is far more better than 2 but too many solution do not respect constraints
        self.assertTrue(objectives1 > objectives2)
        # 1 is then rejected
        self.assertTrue(objectives2.norm_dominate(objectives1))


# Class attribute
    def test_can_update_bounds_for_fitness_normalization(self):
        objectives1, values1, constraints1 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(3, 5))
        objectives2, values2, constraints2 = self.create_instance(values=(3, 4, 1),
                                                                  constraints=(0, 0))
        self.assertEqual(objectives1.constraints_max, (0,))
        self.assertEqual(objectives2.constraints_max, objectives1.constraints_max)
        # Set bounds affect all instance
        objectives1.set_bounds_for_fitness((1, 2, 1), (3, 4, 3), (3, 5))
        self.assertEqual(objectives1.constraints_max, (3, 5))
        self.assertEqual(objectives2.constraints_max, objectives1.constraints_max)
        self.assertEqual(objectives1.fitness_max, (3, 4, 3))
        self.assertEqual(objectives1.fitness_max, objectives2.fitness_max)
        self.assertEqual(objectives1.fitness_min, (1, 2, 1))
        self.assertEqual(objectives1.fitness_min, objectives2.fitness_min)
        # Up bound must be larger than lower
        with self.assertRaises(AssertionError):
            objectives1.set_bounds_for_fitness((1, 2, 1), (3, 1, 3), (2,))
        # Up and down bounds must be of the same length as Fitness weights
        with self.assertRaises(AssertionError):
            objectives1.set_bounds_for_fitness((1, 1), (3, 1, 3), (2,))
        with self.assertRaises(AssertionError):
            objectives1.set_bounds_for_fitness((1, 2, 1), (3, 3), (2,))

    def test_feasibility_rate_is_shared_among_all_instance(self):
        objectives1, values1, constraints1 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(3, 5))
        objectives2, values2, constraints2 = self.create_instance(values=(3, 4, 1),
                                                                  constraints=(0, 0))
        # Update feasibility rate
        self.MyConsFitness.set_feasibility_rate(0.5)
        self.assertEqual(objectives1.feasibility_rate, 0.5)
        self.assertEqual(objectives1.feasibility_rate, objectives2.feasibility_rate)
        # Cannot update feasability rate with value larger than 1 or lower than 0
        self.MyConsFitness.set_feasibility_rate(-1)
        self.assertEqual(objectives1.feasibility_rate, 0.5)
        self.MyConsFitness.set_feasibility_rate(1.1)
        self.assertEqual(objectives1.feasibility_rate, 0.5)

# Equality
    def test_two_constrained_fitness_can_be_equals(self):
        objectives1, values1, constraints1 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(1, 0),
                                                                  mini=False)
        objectives2, values2, constraints2 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(1, 0),
                                                                  mini=False)
        self.assertEqual(objectives1, objectives2)

    def test_two_fitness_with_constraint_are_different_if_constraint_differents(self):
        # Different if constraint different they are different
        objectives1, values1, constraints1 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(1, 0),
                                                                  mini=False)
        objectives2, values2, constraints2 = self.create_instance(values=(1, 2, 3),
                                                                  constraints=(11, 0),
                                                                  mini=False)
        self.assertNotEqual(objectives1, objectives2)

    def test_compare_with_an_unconstrained_fitness_only_check_fitness_not_constraint(self):
        class NormalFitness(Fitness):
            __slots__ = ()
            weights = (1, 1, 1)
        objectives1 = NormalFitness(values=(1, 1, 1))
        objectives2, values2, constraints2 = self.create_instance(values=(1, 1, 1),
                                                                  constraints=(1, 10),
                                                                  mini=False)
        self.assertEqual(objectives2, objectives1)

    def test_fitness_against_tuple_does_not_check_for_constraints(self):
        objectives1 = (1, 1, 1)
        objectives2, values2, constraints2 = self.create_instance(values=(1, 1, 1),
                                                                  constraints=(1, 10),
                                                                  mini=False)
        self.assertEqual(objectives2, objectives1)

# Representation
    def test_repr(self):
        objectives1, values, constraints = self.create_instance(values=(11, -41, 1),
                                                                constraints=(1, 10),
                                                                mini=False)
        self.assertEqual(repr(objectives1), "MyConsFitness((11.0, -41.0, 1.0), (1, 10))")

    def test_str(self):
        objectives1, values, constraints = self.create_instance(values=(1, 2, 3),
                                                                constraints=(1, 10),
                                                                mini=False)
        w_values = objectives1.w_values
        values = tuple((el for el in map(float, values)))
        self.MyConsFitness.set_bounds_for_fitness((1, 2, 3), (1, 2, 3), (1, 10))
        self.MyConsFitness.set_feasibility_rate(0)
        n_values = objectives1.norm_values
        text = "MyConsFitness({}, {}, {})".format(values, w_values, n_values)
        self.assertEqual(str(objectives1), text)

    def test_Fitness_is_mutable_and_does_not_support_hashing(self):
        objectives1, _, _ = self.create_instance(values=(1, 1, 1),
                                                 constraints=(1, 10),
                                                 mini=False)
        with self.assertRaises(TypeError):
            hash(objectives1)

# Copy
    def test_swallow_copy_has_no_reference_from_parent(self):
        values = (1, 2, 3)
        constraints = (1, 10)
        objectives1, values, constraints = self.create_instance(values=values,
                                                                constraints=constraints,
                                                                mini=False)
        objectives2 = objectives1.copy()
        self.assertEqual(objectives1, objectives2)
        self.assertEqual(objectives1.values, values)
        self.assertEqual(objectives1.constraints, constraints)
        self.assertIsNot(objectives1, objectives2)
        # Check if it is a reference (IsNot test must fail but just in case)
        objectives2.values = (1, 2, 4)
        self.assertNotEqual(objectives1, objectives2)

    def test_deep_copy_has_no_reference_from_parent(self):
        values = (1, 2, 3)
        constraints = (1, 10)
        objectives1, values, constraints = self.create_instance(values=values,
                                                                constraints=constraints,
                                                                mini=False)
        objectives2 = objectives1.clone()
        self.assertEqual(objectives1, objectives2)
        self.assertEqual(objectives1.values, values)
        self.assertEqual(objectives1.constraints, constraints)
        self.assertIsNot(objectives1, objectives2)
        # Check if it is a reference (IsNot test must fail but just in case)
        objectives2.values = (1, 2, 4)
        self.assertNotEqual(objectives1, objectives2)


# Run all tests
if __name__ == '__main__':
    unittest.main()
