# -*- coding:Utf8 -*-

"""
    How to run it ?
        - First go to the project root folder of the package ("pyMayBee")
        - Run python unittest command line ("python3 -m unittest")

    cd /home/pampi/Documents/Projects/pyMayBee
    python3 -m unittest tests/test_abc.py

    ****************************************************************************
    * Benchmarks can be run to test ABCHive implementation in ".benchmarks.py" *
    ****************************************************************************

"""

import unittest

from path import Path

import pyMayBee.Misc.random_helper as rh

from pyMayBee.Algorithm import ABCHive
from pyMayBee.Primitive.archive import EpsilonArchive
from pyMayBee.Primitive.fitness import Fitness
from pyMayBee.Misc import inherit_docstring, from_json


@inherit_docstring
class MockHive(ABCHive):

    def evaluate(self, simulations):
        """Return fitness for simulations randomly."""
        nb_simulations = len(simulations)
        fitness = ((rh.RandomGenerator.randint(0, 10), rh.RandomGenerator.randint(0, 10)) for i in range(nb_simulations))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class MyFitness(Fitness):
    __slots__ = ()
    weights = (1, 1)


class HiveUnitTests(unittest.TestCase):

    """
        Tests unit for the **Hive** class implementation.
    """

# USEFUL FUNCTIONS
    def setUp(self):
        """
            Executing prior to each tasks.
        """
        # Fitness instance
        self.MyFitness = MyFitness
        self.fitness_weights = MyFitness.weights

        # An archive instance
        self.archive = EpsilonArchive((1, 1))

        # Criteria file path
        self.json_folder = Path('tests/Criteria').abspath()

        # Seed random generator to always act the same
        rh.RandomGenerator.seed(123)

    def tearDown(self):
        """
            Executing after each task.
            Clean environment.
        """
        pass


# TOOLS (function which can be used to avoid repeated stuff)
    def create_hive(self, criteria_file, size=10, max_trial=30, fitness_type=MyFitness):
        json_path = self.json_folder / criteria_file
        hive = MockHive(self.archive, from_json(json_path), size, max_trial,
                        fitness_type=fitness_type)
        return hive


# TESTS (Must start with <test_>)

# Init
    def test_create_hive_(self):
        # Manual
        json_path = self.json_folder / 'criteria.json'
        hive_1 = MockHive(self.archive, from_json(json_path), 10, 30, MyFitness)
        rh.RandomGenerator.seed(123)  # Same seed for both hives creation
        # With helper
        hive_2 = self.create_hive('criteria.json', 10, 30, MyFitness)
        # Same number of sources
        self.assertEqual(hive_1.nb_sources, 5)
        self.assertEqual(hive_1.nb_sources, hive_2.nb_sources)
        # Same trial number
        self.assertEqual(hive_1.max_trial, 30)
        self.assertEqual(hive_1.max_trial, hive_2.max_trial)
        # Same sources and check sources is of length nb_sources
        self.assertEqual(len(hive_1.sources), 5)
        self.assertEqual(hive_1.sources, hive_2.sources)
        # Start probs are equals for all sources (weights sum)
        self.assertEqual(hive_1.weighted_prob, [1, 2, 3, 4, 5])
        self.assertEqual(len(hive_1.weighted_prob), 5)
        self.assertEqual(hive_1.weighted_prob, hive_2.weighted_prob)
        # Check archive is only a reference to an external archive class
        self.assertIs(hive_1.archive, hive_2.archive)

# see benchmark.py


# Run all tests
if __name__ == '__main__':
    unittest.main()
