# -*- coding:Utf8 -*-


"""
    A list of multi-objective function with and without constraints to test
    the algorithm performance which can be found at for the most part

    Each problem has its own configuration file which can be found in
    `tests/Criteria` folder.
"""


import pyMayBee.Misc.random_helper as rh
import pyMayBee.Entity.bees as bee_entity

from pyMayBee.Misc.tools import from_json
from pyMayBee.Primitive.archive import EpsilonArchive
from pyMayBee.Primitive.fitness import fitness_factory, Fitness, FitnessConstrained
from pyMayBee.Problem import *

import numpy as np
import pandas as pd

from path import Path
from itertools import product
from math import sin, cos, exp, sqrt, pi

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.mlab import griddata


def dominate_pareto(val1, val2, solutions):
    """Return True if (val1, val2) dominate all solution in *solutions* else False.
    Suppose minimization for both objectives.
    """
    for (f1, f2) in solutions:
        if f1 < val1 and f2 < val2:
            return False
        elif f1 == val1 and f2 < val2:
            return False
        elif f1 < val1 and f2 == val2:
            return False
    return True


def larger_than_five(x1, x2):
    """Used for Hanne functions."""
    return x1 + x2 >= 5


def hanne4_cond(x1, x2):
    """Used for Hanne functions."""
    return x2 - 5 + 0.5 * x1 * sin(4 * x1) >= 0


def run(problem_list, modifiers, seed=None):
    """Run problems in a loop"""
    if seed is not None:
        rh.set_random_seed(seed)
    for name in problem_list:
        # If Optimization not working continue
        try:
            hive = start_optimization(name, bees, modifiers=modifiers)
            if len(hive._fitness_type.weights) == 3:
                plot_3D(hive, name)
            else:
                plot_2D(hive, name)
        except IndexError:
            hive.logging.error('Archive empty, stop process.')


def start_optimization(name, bees, modifiers={},
                       json_folder=None):
    """Start an optimization problem list inside **PROBLEMS_DEF** container."""
    # Normalized name
    name = name.strip()
    # Default folder assignation
    if json_folder is None:
        json_folder = Path('tests/Criteria').expand().abspath()
    json_path = json_folder / PROBLEMS_DEF[name][0]
    config = from_json(json_path)
    # Overwrite default if needed
    if modifiers:
        config.update(modifiers)
    archive = EpsilonArchive(epsilons=config['epsilons'])
    # Create hive
    fitness_base = FitnessConstrained if config['constrained'] is True else Fitness
    fitness_type = fitness_factory(config['weights'], fitness_base, name + 'Fitness')
    HiveType = PROBLEMS_DEF[name][1]
    hive = HiveType(archive, config, population_size=config['hive_size'],
                    max_trial=config['max_trial'], fitness_type=fitness_type)
    # Run it and return hive reference
    hive.run(max_iteration=config['max_iteration'], max_evaluation=config['max_evaluation'], bees=bees)
    return hive


def plot_2D(hive, name, fontdict=None):
    """Plot hive result for 2D objectives."""
    # Plot Pareto if exists
    f1 = [soource.fitness.values[0] for soource in hive.archive]
    f2 = [soource.fitness.values[1] for soource in hive.archive]
    fig = plt.figure()
    ax = fig.add_subplot(111)
    if name in TRUE_PARETO.keys():
        for front in TRUE_PARETO[name]:
            p1, p2 = front()
            ax.scatter(p1, p2, color='#93a1a1', alpha=0.6, marker='o', s=30)
    ax.scatter(f1, f2, marker='P', s=45, alpha=0.9, color='#268bd2')
    plt.suptitle(name.upper())
    if fontdict is None:
        fontdict = dict(size=15, family='Source Code Pro')
    ax.set_xlabel('f1', fontdict=fontdict, labelpad=10)
    ax.set_ylabel('f2', fontdict=fontdict, labelpad=10)


def plot_3D(hive, name, fontdict=None):
    """Plot hive result for 3D objectives."""
    # Plot Pareto if exists
    f1 = [soource.fitness.values[0] for soource in hive.archive]
    f2 = [soource.fitness.values[1] for soource in hive.archive]
    f3 = [soource.fitness.values[2] for soource in hive.archive]
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    if name in TRUE_PARETO.keys():
        for front in TRUE_PARETO[name]:
            p1, p2, p3 = front()
            # Discontinue (DTLZ4 is buggy)
            if name in ('DTLZ4', 'DTLZ7'):
                # Create scatter for lines as PF
                ax.scatter3D(p1, p2, p3, color='#93a1a1', alpha=0.5, marker='o', s=20)
            # Too many point just draw curve
            elif name in ('DTLZ5', 'DTLZ6', 'viennet'):
                ax.plot3D(p1, p2, p3, color='#93a1a1', alpha=0.5)
            else:
                # Create meshes for surfaces as PF
                X, Y, Z = prepare_meshes(p1, p2, p3)
                ax.plot_wireframe(X, Y, Z, rcount=25, ccount=25, alpha=0.8, color='#93a1a1')
    ax.scatter3D(f1, f2, f3, marker='X', s=45, alpha=0.9, color='#268bd2')
    plt.suptitle(name.upper())
    if fontdict is None:
        fontdict = dict(size=15, family='Source Code Pro')
    ax.set_xlabel('f1', fontdict=fontdict, labelpad=10)
    ax.set_ylabel('f2', fontdict=fontdict, labelpad=10)
    ax.set_zlabel('f3', fontdict=fontdict, labelpad=10)

    # Create meshes (uncomment to add)
    # X, Y, Z = prepare_meshes(f1, f2, f3)
    # ax.plot_trisurf(f1, f2, f3, color='#93a1a1')
    # ax.plot_wireframe(X, Y, Z, rcount=25, ccount=25)


def prepare_meshes(x, y, z):
    xi = np.linspace(min(x), max(x))
    yi = np.linspace(min(y), max(y))
    Z = griddata(x, y, z, xi, yi, 'linear')
    X, Y = np.meshgrid(xi, yi)
    return X, Y, Z

################################################################################
# PARETO FRONT
################################################################################


def ZDT1_pareto():
    """Pareto for G == 1."""
    pareto_f1 = [x1 / 100 for x1 in range(0, 101)]
    pareto_f2 = [1 - sqrt(x1) for x1 in pareto_f1]
    return pareto_f1, pareto_f2


def ZDT1_linear_pareto():
    """Pareto for G == 1."""
    pareto_f1 = [x1 / 100 for x1 in range(0, 101)]
    pareto_f2 = [1 - x1 for x1 in pareto_f1]
    return pareto_f1, pareto_f2


def ZDT2_pareto():
    """Pareto for G == 1."""
    pareto_f1 = [x1 / 100 for x1 in range(0, 101)]
    pareto_f2 = [1 - x1 ** 2 for x1 in pareto_f1]
    return pareto_f1, pareto_f2


def ZDT3_pareto():
    """Pareto for G == 1."""
    pareto_f1 = [x1 / 100 for x1 in range(0, 101)]
    pareto_f2 = [1 - sqrt(x1) - x1 * sin(10 * pi * x1) for x1 in pareto_f1]
    return pareto_f1, pareto_f2


def ZDT4_pareto():
    """Pareto for G == 1."""
    pareto_f1 = [x1 / 100 for x1 in range(0, 101)]
    pareto_f2 = [1 - sqrt(x1) for x1 in pareto_f1]
    return pareto_f1, pareto_f2


def ZDT6_pareto():
    """Pareto for G == 1."""
    pareto_f1 = [1 - exp(-4 * x1 / 100) * sin(6 * pi * x1 / 100) ** 6 for x1 in range(0, 101)]
    pareto_f2 = [1 - f1 ** 2 for f1 in pareto_f1]
    return pareto_f1, pareto_f2


def constrEx_pareto():
    """Pareto front for Constr-Ex problem."""
    folder = Path('tests/Criteria/PF')
    frame = pd.read_csv(folder / 'constrEx.pf', sep=' ', names=[0, 1])
    return frame[0].values, frame[1].values


def fonsecafleming_pareto():
    """Pareto front for Fonseca Fleming problem."""
    folder = Path('tests/Criteria/PF')
    frame = pd.read_csv(folder / 'fonsecaFleming.pf', sep=' ', names=[0, 1])
    return frame[0].values, frame[1].values


def kursawe_pareto():
    """Pareto front for Kursawe problem."""
    folder = Path('tests/Criteria/PF')
    frame = pd.read_csv(folder / 'kursawe.pf', sep=' ', names=[0, 1])
    return frame[0].values, frame[1].values


def schaffer1_pareto():
    """Pareto front for Schaffer problem."""
    folder = Path('tests/Criteria/PF')
    frame = pd.read_csv(folder / 'schaffer.pf', sep=' ', names=[0, 1])
    return frame[0].values, frame[1].values


def osyczkaKundu_pareto():
    """Pareto front for OsyczkaKundu problem."""
    folder = Path('tests/Criteria/PF')
    frame = pd.read_csv(folder / 'osyczkaKundu.pf', sep=' ', names=[0, 1])
    return frame[0].values, frame[1].values


def viennet_pareto():
    """Pareto front for Viennet 3D problem."""
    folder = Path('tests/Criteria/PF')
    frame = pd.read_csv(folder / 'viennet_3D.pf', sep=' ', names=[0, 1, 2])
    return frame[0].values, frame[1].values, frame[2].values


def DTLZ1_pareto():
    """Pareto front for DTLZ1 3D problem."""
    folder = Path('tests/Criteria/PF/DTLZ')
    frame = pd.read_csv(folder / 'DTLZ1_3D.pf', sep=' ', names=[0, 1, 2])
    return frame[0].values, frame[1].values, frame[2].values


def DTLZ2_pareto():
    """Pareto front for DTLZ2 3D problem."""
    folder = Path('tests/Criteria/PF/DTLZ')
    frame = pd.read_csv(folder / 'DTLZ2_3D.pf', sep=' ', names=[0, 1, 2])
    return frame[0].values, frame[1].values, frame[2].values


def DTLZ3_pareto():
    """Pareto front for DTLZ3 3D problem."""
    folder = Path('tests/Criteria/PF/DTLZ')
    frame = pd.read_csv(folder / 'DTLZ3_3D.pf', sep=' ', names=[0, 1, 2])
    return frame[0].values, frame[1].values, frame[2].values


def DTLZ4_pareto():
    """Pareto front for DTLZ4 3D problem."""
    folder = Path('tests/Criteria/PF/DTLZ')
    frame = pd.read_csv(folder / 'DTLZ4_3D.pf', sep=' ', names=[0, 1, 2])
    return frame[0].values, frame[1].values, frame[2].values


def DTLZ5_pareto():
    """Pareto front for DTLZ5 3D problem."""
    folder = Path('tests/Criteria/PF/DTLZ')
    frame = pd.read_csv(folder / 'DTLZ5_3D.pf', sep=' ', names=[0, 1, 2])
    return frame[0].values, frame[1].values, frame[2].values


def DTLZ6_pareto():
    """Pareto front for DTLZ6 3D problem."""
    folder = Path('tests/Criteria/PF/DTLZ')
    frame = pd.read_csv(folder / 'DTLZ6_3D.pf', sep=' ', names=[0, 1, 2])
    return frame[0].values, frame[1].values, frame[2].values


def DTLZ7_pareto():
    """Pareto front for DTLZ7 3D problem."""
    folder = Path('tests/Criteria/PF/DTLZ')
    frame = pd.read_csv(folder / 'DTLZ7_3D.pf', sep=' ', names=[0, 1, 2])
    return frame[0].values, frame[1].values, frame[2].values


def hanne1_pareto():
    """Approximation of True pareto front."""
    temp = [el / 10 for el in range(0, 51)]
    # Get f1 / f2 values
    values = [(x1, x2)
              for (x1, x2) in product(temp, temp) if larger_than_five(x1, x2)]
    # Remove dominated ones
    couples = [(p_f1, p_f2) for (p_f1, p_f2) in values if dominate_pareto(p_f1, p_f2, values)]
    pareto_f1 = [el[0] for el in couples]
    pareto_f2 = [el[1] for el in couples]
    return pareto_f1, pareto_f2


def hanne2_pareto():
    """Approximation of True pareto front."""
    temp = [el / 10 for el in range(0, 51)]
    # Get f1 / f2 values
    values = [(x1 * x1, x2 * x2)
              for (x1, x2) in product(temp, temp) if larger_than_five(x1, x2)]
    # Remove dominated ones
    couples = [(p_f1, p_f2) for (p_f1, p_f2) in values if dominate_pareto(p_f1, p_f2, values)]
    pareto_f1 = [el[0] for el in couples]
    pareto_f2 = [el[1] for el in couples]
    return pareto_f1, pareto_f2


def hanne3_pareto():
    """Approximation of True pareto front."""
    temp = [el / 10 for el in range(0, 51)]
    # Get f1 / f2 values
    values = [(sqrt(x1), sqrt(x2))
              for (x1, x2) in product(temp, temp) if larger_than_five(x1, x2)]
    # Remove dominated ones
    couples = [(p_f1, p_f2) for (p_f1, p_f2) in values if dominate_pareto(p_f1, p_f2, values)]
    pareto_f1 = [el[0] for el in couples]
    pareto_f2 = [el[1] for el in couples]
    return pareto_f1, pareto_f2


def hanne4_pareto():
    """Approximation of True pareto front."""
    temp = [el / 10 for el in range(0, 121)]
    # Get f1 / f2 values
    values = [(x1, x2)
              for (x1, x2) in product(temp, temp) if hanne4_cond(x1, x2)]
    # Remove dominated ones
    couples = [(p_f1, p_f2) for (p_f1, p_f2) in values if dominate_pareto(p_f1, p_f2, values)]
    pareto_f1 = [el[0] for el in couples]
    pareto_f2 = [el[1] for el in couples]
    return pareto_f1, pareto_f2


def hanne5_pareto():
    """Approximation of True pareto front."""
    temp = [el / 20 for el in range(0, 241)]
    # Get f1 / f2 values
    values = [(int(x1) + 0.5 + (x1 - int(x1)) * sin(2 * pi * (x2 - int(x2))),
               int(x2) + 0.5 + (x1 - int(x1)) * cos(2 * pi * (x2 - int(x2))))
              for (x1, x2) in product(temp, temp) if larger_than_five(x1, x2)]
    # Remove dominated ones
    couples = [(p_f1, p_f2) for (p_f1, p_f2) in values if dominate_pareto(p_f1, p_f2, values)]
    pareto_f1 = [el[0] for el in couples]
    pareto_f2 = [el[1] for el in couples]
    return pareto_f1, pareto_f2


def biModalConvex_pareto():
    """Optimal Pareto front for x2 ~ 0.2."""
    pareto_f1 = [x1 / 100 for x1 in range(10, 101)]
    G_pareto = 2 - exp(-((0.2 - 0.2) / 0.004) ** 2) - 0.8 * exp(-((0.2 - 0.6) / 0.4) ** 2)
    pareto_f2 = [G_pareto / f1 for f1 in pareto_f1]
    return pareto_f1, pareto_f2


def rastrigin_pareto():
    """Optimal Pareto front for G = 0 with (x[i] == 0 for i in range(1, 20))."""
    pareto_f1 = [x1 / 100 for x1 in range(10, 101)]
    pareto_f2 = [1 / f1 for f1 in pareto_f1]
    return pareto_f1, pareto_f2


def matyas_pareto():
    """Optimal Pareto front for G = 0 with x == y == 0."""
    pareto_f1 = [x1 / 100 for x1 in range(10, 101)]
    pareto_f2 = [(1 / f1) for f1 in pareto_f1]
    return pareto_f1, pareto_f2


def convexGlobalNonConvex_false_pareto():
    """Local non convex True pareto front (alpha == 4)."""
    pareto_f1 = [(x1 / 100) * 2 for x1 in range(0, 101)]
    G = [2 for _ in range(0, 101)]
    alpha = 4
    H = [1 - (f1 / g) ** alpha if f1 <= g else 0 for (g, f1) in zip(G, pareto_f1)]
    pareto_f2 = [h * g for (h, g) in zip(H, G)]
    return pareto_f1, pareto_f2


def convexGlobalNonConvex_true_pareto():
    """Global convex True pareto front (alpha == 0.25)."""
    pareto_f1 = [(x1 / 100) for x1 in range(0, 101)]
    G = [1 for _ in range(0, 101)]
    alpha = 0.25
    H = [1 - (f1 / g) ** alpha if f1 <= g else 0 for (g, f1) in zip(G, pareto_f1)]
    pareto_f2 = [h * g for (h, g) in zip(H, G)]
    return pareto_f1, pareto_f2


################################################################################
# DEFINITION
################################################################################

# Problems description
PROBLEMS_DEF = {'kursawe': ('kursawe.json', KursaweHive),
                'schaffer1': ('schaffer1.json', Schaffer1Hive),
                'schaffer2': ('schaffer2.json', Schaffer2Hive),
                'viennet': ('viennet.json', ViennetHive),
                'fonsecafleming': ('fonsecaFleming.json', FonsecaFlemingHive),
                'poloni': ('poloni.json', PoloniHive),
                'ZDT1': ('zitzlerDebThiele_1.json', ZDT1Hive),
                'ZDT1_linear': ('zitzlerDebThiele_1.json', ZDT1LinearHive),
                'ZDT1_3': ('zitzlerDebThiele_1_3.json', ZDT1_3Hive),
                'ZDT2': ('zitzlerDebThiele_2.json', ZDT2Hive),
                'ZDT3': ('zitzlerDebThiele_3.json', ZDT3Hive),
                'ZDT3_3': ('zitzlerDebThiele_3_3.json', ZDT3_3Hive),
                'ZDT4': ('zitzlerDebThiele_4.json', ZDT4Hive),
                'ZDT6': ('zitzlerDebThiele_6.json', ZDT6Hive),
                'ZDT6_3': ('zitzlerDebThiele_6_3.json', ZDT6_3Hive),
                'constrEx': ('constrEx.json', ConstrExHive),
                'binhKorn': ('binhKorn.json', BinhKornHive),
                'chakongHaimes': ('chakongHaimes.json', ChakongHaimesHive),
                'osyczkaKundu': ('osyczkaKundu.json', OsyczkaKunduHive),
                'binh': ('binh.json', BinhHive),
                'TNK': ('TNK.json', TNKHive),
                'hanne1': ('hanne1.json', Hanne1Hive),
                'hanne2': ('hanne2.json', Hanne2Hive),
                'hanne3': ('hanne3.json', Hanne3Hive),
                'hanne4': ('hanne4.json', Hanne4Hive),
                'hanne5': ('hanne5.json', Hanne5Hive),
                'biModalConvex': ('biModalConvex.json', BiModalConvexHive),
                'rastrigin': ('rastrigin.json', RastriginHive),
                'matyas': ('matyas.json', MatyasHive),
                'convexGlobalNonConvexLocal': ('convexGlobalNonConvexLocal.json', ConvexGlobalNonConvexLocalHive),
                'DTLZ1': ('DTLZ1.json', DTLZ1Hive),
                'DTLZ2': ('DTLZ2.json', DTLZ2Hive),
                'DTLZ3': ('DTLZ3.json', DTLZ3Hive),
                # Function must be check because PF found better than True PF
                'DTLZ4': ('DTLZ4.json', DTLZ4Hive),
                'DTLZ5': ('DTLZ5.json', DTLZ5Hive),
                'DTLZ6': ('DTLZ6.json', DTLZ6Hive),
                'DTLZ7': ('DTLZ7.json', DTLZ7Hive),
                }

# True or approximation of pareto front
TRUE_PARETO = {'ZDT1': [ZDT1_pareto, ],
               'ZDT1_linear': [ZDT1_linear_pareto, ],
               'ZDT2': [ZDT2_pareto, ],
               'ZDT3': [ZDT3_pareto, ],
               'ZDT4': [ZDT4_pareto, ],
               'ZDT6': [ZDT6_pareto, ],
               'DTLZ1': [DTLZ1_pareto, ],
               'DTLZ2': [DTLZ2_pareto, ],
               'DTLZ3': [DTLZ3_pareto, ],
               'DTLZ4': [DTLZ4_pareto, ],
               'DTLZ5': [DTLZ5_pareto, ],
               'DTLZ6': [DTLZ6_pareto, ],
               'DTLZ7': [DTLZ7_pareto, ],
               'constrEx': [constrEx_pareto, ],
               'osyczkaKundu': [osyczkaKundu_pareto, ],
               'schaffer1': [schaffer1_pareto, ],
               'fonsecafleming': [fonsecafleming_pareto, ],
               'viennet': [viennet_pareto, ],
               'kursawe': [kursawe_pareto, ],
               'hanne1': [hanne1_pareto, ],
               'hanne2': [hanne2_pareto, ],
               'hanne3': [hanne3_pareto, ],
               'hanne4': [hanne4_pareto, ],
               # 'hanne5': [hanne5_pareto, ],
               'biModalConvex': [biModalConvex_pareto, ],
               'rastrigin': [rastrigin_pareto, ],
               'matyas': [matyas_pareto, ],
               'convexGlobalNonConvexLocal': [convexGlobalNonConvex_false_pareto,
                                              convexGlobalNonConvex_true_pareto],
               }


def log_basicConfig():
    """Convenient way to initialize a default logger. Must only be called once."""
    import logging
    # Define base config which log to a file
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(levelname)-8s: %(name)-35s %(funcName)-25s >> %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        filename='log.log',
                        filemode='w')
    # Add Stream handler to ABCHive algorithm
    cons_handler = logging.StreamHandler()
    cons_handler.setLevel(logging.INFO)
    formatter = logging.Formatter('%(levelname)-8s: %(name)-35s %(funcName)-25s >> %(message)s')
    cons_handler.setFormatter(formatter)
    logging.getLogger('').addHandler(cons_handler)


if __name__ == '__main__':
    log_basicConfig()
    # Run problems
    problem_list = ['DTLZ4', 'ZDT4', 'convexGlobalNonConvexLocal']
    # Run multiple time the same problem
    problem_list = (['ZDT4' for el in range(25)] +
                    ['kursawe' for el in range(25)] +
                    ['DTLZ7' for el in range(25)] +
                    ['rastrigin' for el in range(25)] +
                    ['DTLZ1' for el in range(25)])
    problem_list = (['ZDT6' for el in range(10)])

    # Used to temporary change a problem definition
    modifiers = {}
    # modifiers = {'max_evaluation': 500}

    # Seed (not working as expected)
    seed = None

    # EmployedLevySpyBee  ---> Améliore ZDT2 et ZDT4 et osyczkaKundu et TNK
    # EmployedLevySpyBee  ---> Dégrade Kursawe
    # ScoutLevyBee        ---> Dégrade Kursawe
    # ScoutBee            ---> Améliore multi modal (ratringin) et Kursawe
    # OnlookerLevyBee     ---> Dégrade convergence

    # Combine Lévy flight to uniform motion to increase exploitation and exploration
    # Add OBL when a new source is defined
    # Uniform : Employed, Onlookers, Scouts and Inits
    # Lévy    : Employed
    # OBL     : Scouts and Inits
    bees = {'init': bee_entity.OBLBee,
            'employed': (bee_entity.EmployedLevySpyBee,
                         bee_entity.EmployedSpyBee,
                         0.5),
            'onlookers': bee_entity.OnlookerBee,
            'scout': bee_entity.ScoutBee,
            }

    # Update bees modification rate
    bee_entity.EmployedLevySpyBee.MODIF_RATE = 0.3
    bee_entity.EmployedSpyBee.MODIF_RATE = 0.3
    bee_entity.OnlookerBee.MODIF_RATE = 0.2

    # Start optimizations
    run(problem_list, modifiers, seed)

    print(bee_entity.EmployedLevySpyBee.MODIF_RATE)
    print(bee_entity.EmployedSpyBee.MODIF_RATE)
    print(bee_entity.OnlookerBee.MODIF_RATE)

    # Display all figures at the end
    plt.show(block=True)
