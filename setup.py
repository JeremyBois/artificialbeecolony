#! /usr/bin/env python
# -*- coding:Utf8 -*-

import os

from setuptools import setup, find_packages

# Test support for features: Hypervolume indicator
try:
    from pygmo import hypervolume
except ImportError as e:
    print('No support for hypervolume computation, to enable it please install the *pygmo* library.')

# Test support for features: Plotting support
try:
    import matplotlib
except ImportError as e:
    print('Benchmarks cannot be run without matplotlib support.')


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name='pyMayBee',
    version='0.0.8',
    author='Jérémy Bois',
    author_email='jeremy.bois@u-bordeaux.fr',
    description='Optimization toolkit for constrained or unconstrained multi-objective and multi-criteria problem using ABC as meta-heuristique.',
    long_description=read('README.md'),
    keywords='optimization ABC multi-objective multi-criteria constrained unconstrained archive_history',
    license='GPL',
    packages=find_packages(),
    install_requires=['path.py',
                      'numpy>=1.11',
                      'pandas>=0.18'],
    classifiers=['Development Status :: Alpha',
                 'Environment :: Console',
                 'Operating System :: OS Independent'
                 'Programming Language :: Python :: 3',
                 'Programming Language :: Python :: 3.3',
                 'Programming Language :: Python :: 3.4',
                 'Programming Language :: Python :: 3.5',
                 'Programming Language :: Python :: 3.6',
                 'Intended Audience :: Science/Research',
                 'Topic :: Scientific/Engineering',
                 'Topic :: Optimization utilities']
)
