# -*- coding:Utf8 -*-

"""
    Entity.sources.py

    Definition of all kind of individuals which can be used as solution container.
        - FoodSource is a representation of a solution with a position and fitness.
"""

import pyMayBee.Misc.random_helper as rh

import copy
from collections import Sequence
from ..Primitive import ImmutableSource, Fitness

__all__ = ['FoodSource']


class FoodSource(object):

    """Each food solution store informations about its state (fitness and position).

    :param position: A sequence of Criterion Class.
    :param fitness: A fitness class or fitness class instance.
    :param random_position: :data:`False` to keep criterion value.
                            :data:`True` to randomly generate a FoodSource position.
                            (default :data:`True`)

    .. note::
        Trial value is not take into account to test *FoodSource* equality.
        Only *fitness* and *position* are used to test equality.
    """

    __slots__ = ('_fitness', '_trial', '_position', '_evaluated')

    def __init__(self, position, fitness):
        # Flag used to track source not evaluated
        self._evaluated = False
        try:
            if isinstance(fitness, Fitness):
                # Set fitness is an instance
                self._fitness = fitness
                # Start source with a fitness
                self._evaluated = True
            elif issubclass(fitness, Fitness):
                # Empty fitness if fitness is a Fitness class or subclass
                self._fitness = fitness()
            else:
                raise TypeError
        except TypeError:
            raise TypeError('fitness must be a sub class or and instance of Fitness')
        self._trial = 0
        # Map criterion to criterion.name
        if isinstance(position, Sequence):
            self._position = {criterion.name: criterion for criterion in position}
        elif isinstance(position, dict):
            self._position = position
        else:
            raise TypeError('position argument must be a sequence or a dict of criteria.')

    @property
    def fitness(self):
        """**fitness** behave like an read-only attribute."""
        return self._fitness

    @property
    def position(self):
        """**position** behave like an read-only attribute."""
        return self._position

    @property
    def trial(self):
        """**trial** behave like an read-only attribute."""
        return self._trial

    def _get_evaluated(self):
        """**evaluated** read access"""
        return self._evaluated

    def _set_evaluated(self, value):
        """**evaluated** read access"""
        self._evaluated = value

    evaluated = property(_get_evaluated, _set_evaluated, None,
                         ('**evaluated** is a flag used to check if source must is already evaluated.'))

    def increment_trial(self):
        self._trial += 1

    def lost(self, max_trial):
        """:data:`True` if max_trial exceeded else :data:`False`."""
        if self._trial > max_trial:
            return True
        else:
            return False

    def reset_trial(self):
        self._trial = 0

    def reset(self, new_position):
        """Provide a convinient way to reset a source. Mark it as not evaluated
        and reset trial counter to 0. Also replace current position by *new_position*.
        """
        self.reset_trial()
        self.evaluated = False
        self._position = new_position

    def random_position(self):
        """Update position randomly using uniform distribution.
        Also reset the trial counter and mark source as not evaluated.
        """
        # Call on criterion without value assign a default random value
        new_position = {name: criterion() for (name, criterion) in self.position.items()}
        # Reset trial and mark source as not evaluated
        self.reset(new_position)

    def random_levy_flight(self, scale_factor=None, beta=None):
        """Update position randomly using Lévy flight.
        Also reset the trial counter and mark source as not evaluated.

        :param scale_factor: Coefficient used to scale the Lévy random walk when not enough
                             archive sources (default to 0.01).
        :param beta: Beta value used for Lévy flight distribution (default to 1.5).
        """
        beta = 1.5 if beta is None else beta
        scale_factor = 0.01 if scale_factor is None else scale_factor
        new_position = {}
        # Emulate do while loop to make sure at least a change occurs
        while True:
            for (name, criterion) in self.position.items():
                if criterion.is_quantitative():
                    # Corner case where random walk will fail
                    if criterion.value == 0:
                        new_position[name] = criterion()
                    else:
                        new_pos = rh.levy_flight(criterion.value,
                                                 0,
                                                 beta, scale_factor)
                        # Add together step and current position
                        new_pos += criterion.value
                        new_position[name] = criterion(new_pos)
                else:
                    # Qualitative values are take uniformly without any step
                    new_position[name] = criterion()
            if new_position != self.position:
                break  # Break infinite while loop when job done
            else:
                new_position = {}     # Reinit position and loop again
        # Reset trial and mark source as not evaluated
        self.reset(new_position)

    def update(self, fitness, position):
        """Update the source position and corresponding fitness.
        External object must use it to update the FoodSource instance.
        """
        self.fitness.values = fitness.values
        # Try to update constraints too if exist
        try:
            self.fitness.constraints = fitness.constraints
        except AttributeError:
            pass
        self._position = position
        # Reset trial
        self.reset_trial()

    def add_to(self, archive):
        """Add source immutable representation to *archive* as a namedtuple
        with **fitness** and **position** attribute only.
        """
        if self.fitness.is_feasible():
            source = self.get_namedtuple()
            archive.add(source)

    def get_namedtuple(self):
        """Get source namedtuple copy. Only keep fitness, position and trial."""
        return ImmutableSource(self.fitness.clone(), self.position, self.trial)

    def copy(self):
        return copy.copy(self)

    def clone(self):
        """Internal alias for ``copy.deepcopy`` which will call :meth:`__deepcopy__`"""
        return copy.deepcopy(self)

    def __repr__(self):
        return "{}({}, {})".format(type(self).__name__, repr(self.position), repr(self.fitness))

    def __str__(self):
        return "{}({}, trial={})".format(type(self).__name__,
                                         self.fitness,
                                         self.trial)

    def __eq__(self, other):
        return self.fitness == other.fitness and self.position == other.position

    def __ne__(self, other):
        """Inequality operator ``self != other``."""
        return not self.__eq__(other)

    # See http://indexerror.net/4275/peut-on-utiliser-une-instance-de-classe-comme-clé-dun-dico?show=4276#a4276
    # __hash__ = None  # Source is mutable

    def __copy__(self):
        new_source = type(self)(self.position, type(self.fitness))
        new_source._fitness = self.fitness.copy()
        new_source._position = self.position.copy()
        new_source._trial = self.trial
        return new_source

    def __deepcopy__(self, memo):
        new_source = type(self)(copy.deepcopy(self.position, memo), type(self.fitness))
        new_source._fitness = copy.deepcopy(self.fitness)
        new_source._position = copy.deepcopy(self.position)
        new_source._trial = self.trial
        return new_source
