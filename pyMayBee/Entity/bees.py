# -*- coding:Utf8 -*-

"""
    Entity.bees.py

    Definition of all kind of bee.
        - BaseBee is an individual used to find a new source (see double buffer pattern).
        - OBLBe is a BaseBee which compute a bee position using OBL strategy
        - ScoutBee is an OBLBee which also assign new position to source using OBL and Lévy Flight
        - EmployedBee is a BaseBee which compute a bee position using Uniform motion
        - OnlookerBee is an EmployedBee which select a source based on its fitness
"""


import pyMayBee.Misc.random_helper as rh

from ..Primitive import ImmutableSource, Fitness
from ..exceptions import AbcAttributeError


__all__ = ['OBLBee',
           'ScoutBee', 'ScoutLevyOBLBee', 'ScoutLevyBee', 'ScoutOBLBee',
           'EmployedBee', 'EmployedSpyBee', 'EmployedLevyBee', 'EmployedLevySpyBee',
           'OnlookerBee', 'OnlookerLevyBee',
           ]


class BaseBee(object):

    """Each bee represent a solution to evaluate. Each Bee keep a reference of
    the food source to optimize.

    :param hive: A reference to an instance of an existing hive.
    :param source: A reference to an existing source.
    """

    def __init__(self, hive, source):
        # Reference of the Bee hive
        self.hive = hive
        # Reference of the original food source
        self.source = source
        # Bee starts from source position
        self._position = source.position
        # Init bee fitness with empty values
        self._fitness = type(source.fitness)()

    def _get_fitness(self):
        """**fitness** behave like an attribute"""
        return self._fitness

    def _set_fitness(self, value):
        """**fitness** behave like an attribute."""
        if isinstance(value, Fitness):
            self._fitness = value
        else:
            raise AbcAttributeError('Only a subclass of {} can be used'.format(Fitness.__name__))

    def _del_fitness(self):
        self._fitness = type(self._fitness)()

    # Create a descriptor to control the way self.fitness is handle.
    fitness = property(_get_fitness, _set_fitness, _del_fitness,
                       ("**values** behave like an attribute."))

    @property
    def position(self):
        """**position** behave like an read-only attribute."""
        return self._position

    @property
    def source_position(self):
        """Get original food source position vector."""
        return self.source.position

    def _set_source_fitness(self, values):
        self.source_fitness.values = values
        self.source.evaluated = True

    def _get_source_fitness(self):
        return self.source.fitness

    source_fitness = property(_get_source_fitness, _set_source_fitness, None,
                              ('**source_fitness** wrap access to bee source fitness.'))

    def add_to_archive(self):
        """Add **bee** immutable representation to *archive* as a namedtuple
        with :attr:`fitness` and :attr:`position` attribute only.
        """
        if self.fitness.is_feasible():
            bee = self.get_namedtuple()
            self.hive.archive.add(bee)

    def get_namedtuple(self):
        """Get bee namedtuple copy. Only keep fitness and position. Trial sets to 0"""
        return ImmutableSource(self.fitness.clone(), self.position, 0)

    def update_source(self, constraints=False):
        """Choose between Bee and associated source the best position according
        to fitness. Calls ``self.source.update`` method to update the source
        position.

        :param constraints: :data:`True` to compute domination using constraints
                            (default to :data:`False`)
        """
        # Bound func call to correct function according to constraints flag
        domination_function = self.fitness.norm_dominate if constraints else self.fitness.dominate
        # Check if bee fitness dominates source one
        if domination_function(self.source_fitness):
            self.source.update(self.fitness, self.position)
        else:
            self.source.increment_trial()


class EmployedBee(BaseBee):

    """
    A Bee which select randomly a source from the Hive.archive and to update its
    position using uniform motion to increase its quality.
    Quality is defined by the Fitness class used.

    Use a uniform distribution for the random walk.

    :param hive: A reference to an instance of an existing hive.
    :param source: A reference to an existing source.
    :param scale_uniform: Coefficient used to scale the uniform step length
                          (default to 1)
    """

    # Define the chance for a criterion to be updated (Uniform() < MODIF_RATE --> update)
    # 0 means only one criterion value change
    # 1 means all criteria values change
    MODIF_RATE = 0.3

    def __init__(self, hive, source, scale_uniform=None):
        super().__init__(hive=hive, source=source)
        self.scale_uniform = 1 if scale_uniform is None else scale_uniform
        # Find bee position using random walk
        self.start_search()
        # Increment nb evaluation for hive
        hive.nb_evaluation += 1

    def start_search(self):
        """First select an archive source. Then use it to update `EmployedBee` position.
        Position updated based on MR and levy flight step.

        :param hive: A reference of the Bee hive"""
        # Flag used to select if archive source will be used
        archive_too_small = False
        # Select source K from archive (must be different from attr:`source`)
        archive_source = self.hive.archive.pick_random()
        # Only one element in archive and same as current source
        if len(self.hive.archive) == 1 and self.source.position == archive_source.position:
            archive_too_small = True
        else:
            # Make sure archive source position different from current source position
            while self.source.position == archive_source.position:
                archive_source = self.hive.archive.pick_random()

        # Update bee position using random walk
        self._position = self._update_pos(archive_source, not archive_too_small)

    def _update_pos(self, archive_source, archive_used, **kwargs):
        """Set *BaseBee* position with a random walk from food source current
        position.

        :param archive_source: A source selected from hive archive
        """
        new_position = {}
        MF = type(self).MODIF_RATE
        # Multiple criterion update (at least one True update)
        if MF != 0:
            # Emulate do while loop to make sure at least a change occurs
            while True:
                for (name, criterion) in self.source.position.items():
                    # Update only if random number below MF
                    if rh.RandomGenerator.uniform(0, 1) < MF:
                        new_position[name] = self._do_step(criterion,
                                                           archive_source.position[name],
                                                           archive_used)
                    else:
                        new_position[name] = criterion
                if new_position != self.position:
                    break  # Break infinite while loop
                else:
                    new_position = {}     # Reinit position
        # Make sure at least one change occurs when MF == 0
        else:
            while True:
                new_position = self._position.copy()  # Start from old position
                name = rh.RandomGenerator.choice(list(self.source.position.keys()))
                criterion = self.source.position[name]
                new_position[name] = self._do_step(criterion,
                                                   archive_source.position[name],
                                                   archive_used)
                # If new position different break the do loop
                if new_position != self.position:
                    break
        return new_position

    def _do_step(self, criterion, archive_criterion, archive_used):
        """Perform a random uniform motion when possible otherwise pick a random value.

        :param criterion: A class:`Criterion` instance where levy flight occurs.
        :param archive_criterion: Value of class:`archive` source for this criterion.
        """
        # Can only make random walk on quantitative values
        if criterion.is_quantitative():
            # Archive source used
            if archive_used:
                # Step based on difference from archive and current position (current - archive)
                new_pos = rh.uniform_motion(criterion.value,
                                            archive_criterion.value,
                                            self.scale_uniform)
                # Add together step and current position
                new_pos += criterion.value
            else:
                # Corner case where solution cannot be updated using random walk
                if criterion.value == 0:
                    return criterion()
                # Uniform step based only on current position (current - 0)
                new_pos = rh.uniform_motion(criterion.value,
                                            0,
                                            self.scale_uniform)
                # Add together step and current position
                new_pos += criterion.value
            return criterion(new_pos)
        else:
            # Qualitative values are take uniformly without any step
            return criterion()


class EmployedLevyBee(EmployedBee):

    """A Bee which select randomly a source from the Hive.archive and to update its
    position using Lévy flight to increase its quality.
    Quality is defined by the Fitness class used.

    Use a Lévy distribution for the random walk when not enough
    source inside the archive and an Uniform distribution otherwise.

    :param hive: A reference to an instance of an existing hive.
    :param source: A reference to an existing source.
    :param scale_levy: Coefficient used to scale the Lévy random walk
                           (default to 0.01).
    :param beta: Beta value used for Lévy flight distribution (default to 1.5).
    """

    def __init__(self, hive, source, scale_levy=None, beta=None):
        self.beta = 1.5 if beta is None else beta
        self.scale_levy = 0.01 if scale_levy is None else scale_levy
        super().__init__(hive=hive, source=source)

    def _do_step(self, criterion, archive_criterion, archive_used):
        """If *archive_used* perform a Uniform motion in its direction else
        make a random Lévy flight based on its current position.
        In any case perform a uniform random selection for qualitative parameters.

        :param criterion: A class:`Criterion` instance where levy flight occurs.
        :param archive_criterion: Value of class:`archive` source for this criterion.
        """
        # Can only make lévy flights on quantitative values
        if criterion.is_quantitative():
            # Archive source used
            if archive_used:
                # Step based on difference from archive and current position (current - archive)
                new_pos = rh.levy_flight(criterion.value,
                                         archive_criterion.value,
                                         self.beta, self.scale_levy)
                # Add together step and current position
                new_pos += criterion.value
            else:
                # Corner case where solution cannot be updated using random walk
                if criterion.value == 0:
                    return criterion()
                # Lévy step based only on current position (current - 0)
                new_pos = rh.levy_flight(criterion.value,
                                         0,
                                         self.beta, self.scale_levy)
                # Add together step and current position
                new_pos += criterion.value
            return criterion(new_pos)
        else:
            # Qualitative values are take uniformly
            return criterion()


class OnlookerBee(EmployedBee):

    """A Bee which select a source from the hive based on its :attr:`weighted_prob`
    where more weight is assigned to better sources (ones that dominate others).
    As with :class:`EmployedBee` a random source from the archive is selected
    and used to improve the source fitness.

    Use a Uniform distribution for the random walk.

    :param hive: A reference to an instance of an existing hive.
    :param scale_uniform: Coefficient used to scale the uniform random walk length
                          (default to 1)

    """

    MODIF_RATE = 0.2

    def __init__(self, hive, scale_uniform=1):
        assigned_source = hive.sources[rh.launch_roulette(hive.weighted_prob)]
        # Delegate to parent which will start search as for an EmployedBee
        super().__init__(hive=hive, source=assigned_source,
                         scale_uniform=scale_uniform)


class OnlookerLevyBee(EmployedLevyBee):

    """A Bee which select a source from the hive based on its :attr:`weighted_prob`
    where more weight is assigned to better sources (ones that dominate others).
    As with :class:`UniformEmployedBee` a random source from the archive is selected
    and used to improve the source fitness.

    Use a Lévy distribution for the random walk when not enough
    source inside the archive and an Uniform distribution otherwise.

    :param hive: A reference to an instance of an existing hive.
    :param scale_levy: Coefficient used to scale the Lévy random walk
                           (default to 0.01).
    :param beta: Beta value used for Lévy flight distribution (default to 1.5).
    """

    def __init__(self, hive, scale_levy=None, beta=None):
        assigned_source = hive.sources[rh.launch_roulette(hive.weighted_prob)]
        # Delegate to parent which will start search as for an EmployedBee
        super().__init__(hive=hive, source=assigned_source,
                         beta=beta, scale_levy=scale_levy)


class OBLBee(BaseBee):

    """A bee that compute opposite position of reference source.

    :param hive: A reference to an instance of an existing hive.
    :param source: A reference to an existing source.
    """

    def __init__(self, hive, source):
        super().__init__(hive=hive, source=source)
        # Find new position using OBL
        self.start_search()
        # Increment nb evaluation for hive
        hive.nb_evaluation += 1

    def start_search(self):
        """Update :attr:`position` using OBL method."""
        # Update bee position using OBL
        self._position = self._update_pos()

    def _update_pos(self):
        """Set new position based on OBL method."""
        return {name: self._do_step(criterion) for (name, criterion)
                in self.source.position.items()}

    def _do_step(self, criterion):
        """Perform an opposite based learning (OBL) when criterion is quantitative.
        In case of a qualitative criterion do nothing.

        :param criterion: A :class:`AbstractCriterion` child class.
        """
        # Can only make opposite based learning on quantitative values
        if criterion.is_quantitative():
            return self._do_opposite_position(criterion)
        else:
            # Qualitative values remains unchanged (no opposite exists)
            return criterion

    def _do_opposite_position(self, criterion):
        """Do an opposite based learning on *criterion* which can be defined as
        ``new_value = low_bound + up_bound - value``

        :param criterion: A :class:`AbstractCriterion` child class.
        """
        return criterion(criterion.low_bound + criterion.up_bound - criterion.value)

    def add_to_archive(self):
        """Add source and bee to archive because OBL bee used in initialization.
        So source not yet in archive as bee."""
        self.source.add_to(self.hive.archive)
        super().add_to_archive()


class ScoutBee(OBLBee):

    """A Bee which selects a new random position for the reference source
    and using OBL compute a new position for the Bee.

    :param hive: A reference to an instance of an existing hive.
    :param source: A reference to an existing source.
    """

    def __init__(self, hive, source):
        # New random source position and reset trial
        old_position = source.position.copy()
        source.random_position()
        # Make sure random position different from current one
        # Infinite loop if only one position ...
        while source.position == old_position:
            source.random_position()
        # Increment nb evaluation for hive
        hive.nb_evaluation += 1
        # Find new position using OBL
        super().__init__(hive=hive, source=source)


class ScoutLevyOBLBee(OBLBee):

    """A Bee which selects a new random position for source using a Lévy flight
    and using OBL compute a new position for the Bee.

    :param hive: A reference to an instance of an existing hive.
    :param source: A reference to an existing source.
    """

    def __init__(self, hive, source, scale_levy=0.01, beta=1.5):
        # New random source position, reset trial, and mark as not evaluated
        # New position always different to previous one
        source.random_levy_flight(scale_factor=scale_levy, beta=beta)
        # Increment nb evaluation for hive
        hive.nb_evaluation += 1
        # Find new position using OBL
        super().__init__(hive=hive, source=source)


class ScoutOBLBee(OBLBee):

    """A Bee which selects a new random position for source using a uniform motion
    and using OBL compute a new position for the Bee.

    :param hive: A reference to an instance of an existing hive.
    :param source: A reference to an existing source.
    """

    def __init__(self, hive, source):
        # New random source position, reset trial, and mark as not evaluated
        # New position always different to previous one
        source.random_position()
        # Increment nb evaluation for hive
        hive.nb_evaluation += 1
        # Find new position using OBL
        super().__init__(hive=hive, source=source)


class ScoutLevyBee(EmployedLevyBee):

    """A Bee which replace source using Lévy flight between source and archive
    and then compute another Lévy flight from new source to another archive source.
    Source is then also mark as must be evaluated as the bee position.

    :param hive: A reference to an instance of an existing hive.
    :param source: A reference to an existing source.
    """

    # Define the chance for a criterion to be updated (Uniform() < MODIF_RATE --> update)
    # 0 means only one criterion value change
    # 1 means all criteria values change
    MODIF_RATE = 1

    def __init__(self, hive, source, scale_levy=0.01, beta=1.5):
        # Update source and bee position
        # EmployedLevyBee --> EmployedBee --> BaseBee --> start_search
        super().__init__(hive=hive, source=source,
                         beta=beta, scale_levy=scale_levy)
        # Increment nb evaluation corresponding to source update
        hive.nb_evaluation += 1

    def start_search(self):
        """Update source position using Lévy flight from current position and
        an archive position. Then update new source position using another Lévy
        flight with another archive source.
        So bee position result on two consecutive Lévy flight operation.
        This approach differs from :class:`ScoutLevyOBLBee` in two way:

          - First random walk used archive here where walk is only based on source
            for :class:`ScoutLevyOBLBee`.
          - Bee modification used another Lévy flight where :class:`ScoutLevyOBLBee`
            use OBL strategie.

        :param hive: A reference to an instance of an existing hive.
        :param source: A reference to an existing source.
        :param scale_levy: Coefficient used to scale the Lévy random walk
                           (default to 0.01).
        :param beta: Beta value used for Lévy flight distribution (default to 1.5).
        """
        # Source update
        # Flag used to select if archive source will be used
        archive_too_small = False
        # Select source K from archive (must be different from attr:`source`)
        archive_source = self.hive.archive.pick_random()
        # Only one element in archive and same as current source
        if len(self.hive.archive) == 1 and self.source.position == archive_source.position:
            archive_too_small = True
        else:
            # Make sure archive source position different from current source position
            while self.source.position == archive_source.position:
                archive_source = self.hive.archive.pick_random()
        # Update source position using random walk
        new_position = self._update_pos(archive_source, not archive_too_small)
        self.source.reset(new_position)

        # Bee update
        # Flag used to select if archive source will be used
        archive_too_small = False
        # Pick another archive source
        archive_source = self.hive.archive.pick_random()
        # Only one element in archive and same as current source
        if len(self.hive.archive) == 1 and self.source.position == archive_source.position:
            archive_too_small = True
        else:
            # Make sure archive source position different from current source position
            while self.source.position == archive_source.position:
                archive_source = self.hive.archive.pick_random()
        # Update bee position using random walk from new source position
        self._position = self._update_pos(archive_source, not archive_too_small)

    def add_to_archive(self):
        """Add source and bee to archive as both are changed."""
        self.source.add_to(self.hive.archive)
        super().add_to_archive()


class EmployedSpyBee(EmployedBee):

    """
    A Bee which select randomly a source from the Hive.archive and from
    the Hive.sources and combine information to find a new position.
    Uniform motion used as symetrical version [-1, 1] with
    source reference and directed version [0, 1] for archive reference.
    Quality is defined by the Fitness class used.

    Use a uniform distribution for the random walk.

    :param hive: A reference to an instance of an existing hive.
    :param source: A reference to an existing source.
    :param scale_uniform: Coefficient used to scale the uniform step length
                          (default to 1)
    """

    # Define the chance for a criterion to be updated (Uniform() < MODIF_RATE --> update)
    # 0 means only one criterion value change
    # 1 means all criteria values change
    MODIF_RATE = 0.3

    def __init__(self, hive, source, scale_uniform=None):
        # Select a random source position inside sources (must be different)
        while True:
            self.other_source = rh.RandomGenerator.choice(hive.sources)
            if self.other_source.position != source.position:
                break
        # Run Employed bee search with altered _do_step method
        super().__init__(hive=hive, source=source, scale_uniform=scale_uniform)

    def _do_step(self, criterion, archive_criterion, archive_used):
        """Perform a random uniform motion when possible otherwise pick a random value.

        :param criterion: A class:`Criterion` instance where levy flight occurs.
        :param archive_criterion: Value of class:`archive` source for this criterion.
        """
        # Get other_source value for this criterion
        other_position = self.other_source.position[criterion.name]
        # Can only make random walk on quantitative values
        if criterion.is_quantitative():
            # Archive source used
            if archive_used:
                # Step based on difference from others source and current position
                # rand (-1, 1) x (current - other)
                src_impact = rh.uniform_motion(criterion.value,
                                               other_position.value,
                                               self.scale_uniform)
                # Step based on difference from archive and current position
                # rand (0, 1) x (archive - current)
                arch_impact = rh.directed_uniform_motion(criterion.value,
                                                         archive_criterion.value,
                                                         self.scale_uniform)
                # Add together step and current position
                new_pos = criterion.value + src_impact + arch_impact
            else:
                # Step based on difference from others source and current position
                # rand (-1, 1) x (current - other)
                src_impact = rh.uniform_motion(criterion.value,
                                               other_position.value,
                                               self.scale_uniform)
                new_pos = criterion.value + src_impact
            return criterion(new_pos)
        else:
            # Qualitative values are take uniformly without any step
            return criterion()


class EmployedLevySpyBee(EmployedLevyBee):

    """
    A Bee which select randomly a source from the Hive.archive and from
    the Hive.sources and combine information to find a new position.
    Lévy flight used in place of uniform motion as symetrical version with
    source reference and directed version for archive reference.
    Quality is defined by the Fitness class used.

    Use a uniform distribution for the random walk.

    :param hive: A reference to an instance of an existing hive.
    :param source: A reference to an existing source.
    :param scale_levy: Coefficient used to scale the uniform step length
                          (default to 1)
    """

    # Define the chance for a criterion to be updated (Uniform() < MODIF_RATE --> update)
    # 0 means only one criterion value change
    # 1 means all criteria values change
    MODIF_RATE = 0.3

    def __init__(self, hive, source, scale_levy=None):
        # Select a random source position inside sources (must be different)
        while True:
            self.other_source = rh.RandomGenerator.choice(hive.sources)
            if self.other_source.position != source.position:
                break
        # Run Employed bee search with altered _do_step method
        super().__init__(hive=hive, source=source, scale_levy=scale_levy)

    def _do_step(self, criterion, archive_criterion, archive_used):
        """Perform a random uniform motion when possible otherwise pick a random value.

        :param criterion: A class:`Criterion` instance where levy flight occurs.
        :param archive_criterion: Value of class:`archive` source for this criterion.
        """
        # Get other_source value for this criterion
        other_position = self.other_source.position[criterion.name]
        # Can only make random walk on quantitative values
        if criterion.is_quantitative():
            # Archive source used
            if archive_used:
                # Step based on difference from others source and current position
                # rand (-1, 1) x (current - other)
                src_impact = rh.levy_flight(criterion.value,
                                            other_position.value,
                                            self.scale_uniform)
                # Step based on difference from archive and current positionarchive - current rand (0, 1) x (current - archive)
                arch_impact = rh.directed_levy_flight(criterion.value,
                                                      archive_criterion.value,
                                                      self.scale_uniform)
                # Add together step and current position
                new_pos = criterion.value + src_impact + arch_impact
            else:
                # Step based on difference from others source and current position
                # rand (-1, 1) x (current - other)
                src_impact = rh.levy_flight(criterion.value,
                                            other_position.value,
                                            self.scale_uniform)
                new_pos = criterion.value + src_impact
            return criterion(new_pos)
        else:
            # Qualitative values are take uniformly without any step
            return criterion()
