# -*- coding:Utf8 -*-

from .DTLZ import *
from .ZDT import *
from .hanne import *
from .schaffer import *
from .other import *
