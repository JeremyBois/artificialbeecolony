# -*- coding:Utf8 -*-

"""
    Problem.ZDT.py

    ZDT problems definition.
    [Functions](https://en.wikipedia.org/wiki/Test_functions_for_optimization)
"""

import math

from ..Algorithm import ABCHive


__all__ = ['ZDT1Hive', 'ZDT2Hive', 'ZDT3Hive', 'ZDT4Hive', 'ZDT6Hive',
           'ZDT1LinearHive',
           'ZDT1_3Hive', 'ZDT3_3Hive', 'ZDT6_3Hive']


class ZDT1Hive(ABCHive):

    def evaluate(self, positions):
        """
            Optimal Pareto front for G == 1.
            Convex Pareto-optimal front.
        """
        nb_simulations = len(positions)
        fitness = []
        # Constant
        m, n = 2, 30
        for row_id in range(nb_simulations):
            f1 = positions['x1'][row_id]
            G = (1 + (9 / 29) *
                 sum(positions['x{}'.format(i)][row_id] for i in range(m, n + 1)))
            H = 1 - math.sqrt(f1 / G)
            f2 = G * H
            fitness.append((f1, f2))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class ZDT1LinearHive(ABCHive):

    def evaluate(self, positions):
        """
            Optimal Pareto front for G == 1.
            Linear Pareto-optimal front.
        """
        nb_simulations = len(positions)
        fitness = []
        # Constant
        m, n = 2, 30
        for row_id in range(nb_simulations):
            f1 = positions['x1'][row_id]
            G = (1 + (9 / 29) *
                 sum(positions['x{}'.format(i)][row_id] for i in range(m, n + 1)))
            H = 1 - f1 / G
            f2 = G * H
            fitness.append((f1, f2))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class ZDT1_3Hive(ABCHive):

    def evaluate(self, positions):
        """
            Optimal Pareto front for G == 1.
            2D Convex Pareto-optimal front.
        """
        nb_simulations = len(positions)
        fitness = []
        # Constant
        m, n = 2, 30
        for row_id in range(nb_simulations):
            f1 = positions['x1'][row_id]
            f2 = positions['x2'][row_id]
            G = (1 + (9 / 29) *
                 sum(positions['x{}'.format(i)][row_id] for i in range(m, n + 1)))
            H1 = 1 - math.sqrt(f1 / G)
            H2 = 1 - math.sqrt(f2 / G)
            f3 = G * H1 * H2
            fitness.append((f1, f2, f3))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class ZDT2Hive(ABCHive):

    def evaluate(self, positions):
        """
            Optimal Pareto front for G == 1.
            Non convex counterpart of ZDT1 Pareto-optimal front.
        """
        nb_simulations = len(positions)
        fitness = []
        # Constant
        m, n = 2, 30
        for row_id in range(nb_simulations):
            f1 = positions['x1'][row_id]
            G = (1 + (9 / 29) *
                 sum(positions['x{}'.format(i)][row_id] for i in range(m, n + 1)))
            H = 1 - (f1 / G) ** 2
            f2 = G * H
            fitness.append((f1, f2))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class ZDT3Hive(ABCHive):

    def evaluate(self, positions):
        """
            Optimal Pareto front for G == 1.
            Discrete Pareto-optimal front which consist of several noncontiguous
            convex parts.
        """
        nb_simulations = len(positions)
        fitness = []
        # Constant
        m, n = 2, 30
        for row_id in range(nb_simulations):
            f1 = positions['x1'][row_id]
            G = (1 + (9 / 29) *
                 sum(positions['x{}'.format(i)][row_id] for i in range(m, n + 1)))
            H = 1 - math.sqrt(f1 / G) - (f1 / G) * math.sin(10 * math.pi * f1)
            f2 = G * H
            fitness.append((f1, f2))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class ZDT3_3Hive(ABCHive):

    def evaluate(self, positions):
        """
            Optimal Pareto front for G == 1.
            2D Discrete Pareto-optimal front which consist of several noncontiguous
            convex parts.
        """
        nb_simulations = len(positions)
        fitness = []
        # Constant
        m, n = 2, 30
        for row_id in range(nb_simulations):
            f1 = positions['x1'][row_id]
            f2 = positions['x2'][row_id]
            G = (1 + (9 / 29) *
                 sum(positions['x{}'.format(i)][row_id] for i in range(m, n + 1)))
            H1 = 1 - math.sqrt(f1 / G) - (f1 / G) * math.sin(10 * math.pi * f1)
            H2 = 1 - math.sqrt(f2 / G) - (f2 / G) * math.sin(10 * math.pi * f2)
            f3 = G * H1 * H2
            fitness.append((f1, f2, f3))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class ZDT4Hive(ABCHive):

    def evaluate(self, positions):
        """
            Optimal Pareto front for G == 1.
            Multi modal non convex Pareto front (21) which make this test hard.
        """
        nb_simulations = len(positions)
        fitness = []
        # Constant
        m, n = 2, 10
        for row_id in range(nb_simulations):
            f1 = positions['x1'][row_id]
            G = (91 +
                 sum(positions['x{}'.format(i)][row_id] ** 2 -
                     10 * math.cos(4 * math.pi * positions['x{}'.format(i)][row_id])
                     for i in range(m, n + 1)))
            H = 1 - math.sqrt(f1 / G)
            f2 = G * H
            fitness.append((f1, f2))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class ZDT6Hive(ABCHive):

    def evaluate(self, positions):
        """
            Optimal Pareto front for G == 1.
            Non Convex Pareto-optimal front.
            Includes two difficulties cause by the nonuniformity of the search space.
            First pareto is not uniformly distributed along the global Pareto front (biased
            for solutions which f1 is near one),
            Second the density of the solutions is lowest near the Pareto-optimal front
            and highest away the front
        """
        nb_simulations = len(positions)
        fitness = []
        # Constant
        m, n = 2, 10
        for row_id in range(nb_simulations):
            x1 = positions['x1'][row_id]
            f1 = 1 - math.exp(-4 * x1) * math.sin(6 * math.pi * x1) ** 6
            G = (1 + 9 *
                 (sum(positions['x{}'.format(i)][row_id] ** 2 for i in range(m, n + 1)) / 9) ** 0.25)
            H = 1 - (f1 / G) ** 2
            f2 = G * H
            fitness.append((f1, f2))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class ZDT6_3Hive(ABCHive):

    def evaluate(self, positions):
        """
            Optimal Pareto front for G == 1.
            2D Non Convex Pareto-optimal front.
            Includes two difficulties cause by the nonuniformity of the search space.
            First pareto is not uniformly distributed along the global Pareto front (biased
            for solutions which f1 is near one),
            Second the density of the solutions is lowest near the Pareto-optimal front
            and highest away the front
        """
        nb_simulations = len(positions)
        fitness = []
        # Constant
        m, n = 2, 10
        for row_id in range(nb_simulations):
            x1 = positions['x1'][row_id]
            x2 = positions['x2'][row_id]
            f1 = 1 - math.exp(-4 * x1) * math.sin(6 * math.pi * x1) ** 6
            f2 = 1 - math.exp(-4 * x2) * math.sin(6 * math.pi * x2) ** 6
            G = (1 + 9 *
                 (sum(positions['x{}'.format(i)][row_id] ** 2 for i in range(m, n + 1)) / 9) ** 0.25)
            H1 = 1 - (f1 / G) ** 2
            H2 = 1 - (f2 / G) ** 2
            f3 = G * H1 * H2
            fitness.append((f1, f2, f3))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints
