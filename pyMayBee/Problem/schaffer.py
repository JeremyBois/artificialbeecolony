# -*- coding:Utf8 -*-

"""
    Problem.schaffer.py

    Schaffer problems definition.
    [Functions](https://en.wikipedia.org/wiki/Test_functions_for_optimization)
"""


from ..Algorithm import ABCHive


__all__ = ['Schaffer1Hive', 'Schaffer2Hive']


class Schaffer1Hive(ABCHive):

    def evaluate(self, positions):
        nb_simulations = len(positions)
        fitness = []
        for row_id in range(nb_simulations):
            f1 = positions['x'][row_id] * positions['x'][row_id]
            f2 = (positions['x'][row_id] - 2) ** 2
            fitness.append((f1, f2))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class Schaffer2Hive(ABCHive):

    def evaluate(self, positions):
        nb_simulations = len(positions)
        fitness = []
        for row_id in range(nb_simulations):
            x = positions['x'][row_id]
            if x <= 1:
                f1 = -x
            elif x > 4:
                f1 = x - 4
            elif x > 1 and x <= 3:
                f1 = x - 2
            elif x > 3 and x <= 4:
                f1 = 4 - x
            else:
                raise ValueError
            f2 = (x - 5) * (x - 5)
            fitness.append((f1, f2))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints
