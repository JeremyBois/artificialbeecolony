# -*- coding:Utf8 -*-

"""
    Problem.hanne.py

    Hanne problems definition.
"""

import math

from ..Algorithm import ABCHive

__all__ = ['Hanne1Hive', 'Hanne2Hive', 'Hanne3Hive', 'Hanne4Hive', 'Hanne5Hive']


class Hanne1Hive(ABCHive):

    def evaluate(self, positions):
        """
            Linear Pareto-optimal front.
        """
        nb_simulations = len(positions)
        fitness = []
        constraints = []
        for row_id in range(nb_simulations):
            # Fitness
            f1 = positions['x1'][row_id]
            f2 = positions['x2'][row_id]
            fitness.append((f1, f2))
            # Constraint
            g1 = 5 - f1 - f2
            # Constraint only if negative
            g1 = g1 if g1 > 0 else 0
            constraints.append((g1, ))
        return fitness, constraints


class Hanne2Hive(ABCHive):

    def evaluate(self, positions):
        """
            Convex Pareto-optimal front.
        """
        nb_simulations = len(positions)
        fitness = []
        constraints = []
        for row_id in range(nb_simulations):
            # Fitness
            x1 = positions['x1'][row_id]
            x2 = positions['x2'][row_id]
            f1 = x1 * x1
            f2 = x2 * x2
            fitness.append((f1, f2))
            # Constraint
            g1 = 5 - x1 - x2
            # Constraint only if negative
            g1 = g1 if g1 > 0 else 0
            constraints.append((g1, ))
        return fitness, constraints


class Hanne3Hive(ABCHive):

    def evaluate(self, positions):
        """
            Non convex Pareto-optimal front.
        """
        nb_simulations = len(positions)
        fitness = []
        constraints = []
        for row_id in range(nb_simulations):
            # Fitness
            x1 = positions['x1'][row_id]
            x2 = positions['x2'][row_id]
            f1 = math.sqrt(x1)
            f2 = math.sqrt(x2)
            fitness.append((f1, f2))
            # Constraint
            g1 = 5 - x1 - x2
            # Constraint only if negative
            g1 = g1 if g1 > 0 else 0
            constraints.append((g1, ))
        return fitness, constraints


class Hanne4Hive(ABCHive):

    def evaluate(self, positions):
        """
            Discrete Pareto-optimal front which consist of several noncontiguous
            linear parts.
        """
        nb_simulations = len(positions)
        fitness = []
        constraints = []
        for row_id in range(nb_simulations):
            # Fitness
            x1 = positions['x1'][row_id]
            x2 = positions['x2'][row_id]
            f1 = x1
            f2 = x2
            fitness.append((f1, f2))
            # Constraint
            g1 = x2 - 5 + 0.5 * x1 * math.sin(4 * x1)
            # Constraint only if negative
            g1 = abs(g1) if g1 < 0 else 0
            constraints.append((g1, ))
        return fitness, constraints


class Hanne5Hive(ABCHive):

    def evaluate(self, positions):
        """
            Non Convex Pareto-optimal front.
            Includes three difficulties cause by the nonuniformity of the search space.
            First pareto is not uniformly distributed along the global Pareto front.
            Second the density of the solutions is lowest near the Pareto-optimal front
            and highest away the front
            Three, front is in some place convex and non convex in another with
            local optimums.
        """
        nb_simulations = len(positions)
        fitness = []
        constraints = []
        for row_id in range(nb_simulations):
            # Fitness
            x1 = positions['x1'][row_id]
            x2 = positions['x2'][row_id]
            # int truncates where math.floor rounds down
            f1 = int(x1) + 0.5 + (x1 - int(x1)) * math.sin(2 * math.pi * (x2 - int(x2)))
            f2 = int(x2) + 0.5 + (x1 - int(x1)) * math.cos(2 * math.pi * (x2 - int(x2)))
            fitness.append((f1, f2))
            # Constraint
            g1 = 5 - x1 - x2
            # Constraint only if negative
            g1 = g1 if g1 > 0 else 0
            constraints.append((g1, ))
        return fitness, constraints
