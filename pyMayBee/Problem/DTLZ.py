# -*- coding:Utf8 -*-

"""
    Problem.DTLZ.py

    DTLZ problems definition.
"""

import math

from ..Algorithm import ABCHive


__all__ = ['DTLZ1Hive', 'DTLZ2Hive', 'DTLZ3Hive', 'DTLZ4Hive', 'DTLZ5Hive', 'DTLZ6Hive', 'DTLZ7Hive']


class DTLZ1Hive(ABCHive):

    def evaluate(self, positions):
        """
            Optimal Pareto front for xi == 0.5 for i in range(M, n) with M == 3 and n == 7.
            Represent a linear pareto front in 3D.
        """
        nb_simulations = len(positions)
        fitness = []
        M = 3  # Number of objectives
        m, n = 1, 7
        twenty_pi = 20.0 * math.pi
        k = n - M + 1
        for row_id in range(nb_simulations):
            x = {i: positions['x{}'.format(i)][row_id] for i in range(m, n + 1)}
            G = 100 * (k + sum((x[i] - 0.5) ** 2 - math.cos(twenty_pi * (x[i] - 0.5)) for i in range(M, n + 1)))

            # Objectives
            f = {}
            for i in range(1, M + 1):
                obj = 0.5 * (1 + G)
                for j in range(1, M - i + 1):
                    obj *= x[j]
                if i > 1:
                    obj *= 1.0 - x[M - i + 1]
                f[i] = obj
            fitness.append((f[1], f[2], f[3]))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class DTLZ2Hive(ABCHive):

    def evaluate(self, positions):
        """
            Optimal Pareto front for xi == 0.5 for i in range(M, n) with M == 3 and n == 12.
            Represent a convex pareto front in 3D.
        """
        nb_simulations = len(positions)
        fitness = []
        M = 3  # Number of objectives
        m, n = 1, 12
        half_pi = math.pi / 2.0
        for row_id in range(nb_simulations):
            x = {i: positions['x{}'.format(i)][row_id] for i in range(m, n + 1)}
            G = sum((x[i] - 0.5) ** 2 for i in range(M, n + 1))
            # Objectives
            f = {}
            for i in range(1, M + 1):
                obj = 1 + G
                for j in range(1, M - i + 1):
                    obj *= math.cos(x[j] * half_pi)
                if i > 1:
                    obj *= math.sin(x[M - i + 1] * half_pi)
                f[i] = obj
            fitness.append((f[1], f[2], f[3]))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class DTLZ3Hive(ABCHive):

    def evaluate(self, positions):
        """
            Not sure about implementation ...
            Optimal Pareto front for xi == 0.5 for i in range(M, n) with M == 3 and n == 12.
            Represent a convex pareto front in 3D.
        """
        nb_simulations = len(positions)
        fitness = []
        M = 3  # Number of objectives
        m, n = 1, 12
        k = n - M + 1
        twenty_pi = 20.0 * math.pi
        half_pi = math.pi / 2.0
        for row_id in range(nb_simulations):
            x = {i: positions['x{}'.format(i)][row_id] for i in range(m, n + 1)}
            G = 100.0 * (k + sum((x[i] - 0.5) ** 2 - math.cos(twenty_pi * (x[i] - 0.5)) for i in range(M, n + 1)))
            # Objectives
            f = {}
            for i in range(1, M + 1):
                obj = 1.0 + G
                for j in range(1, M - i + 1):
                    obj *= math.cos(x[j] * half_pi)
                if i > 1:
                    obj *= math.sin(x[M - i + 1] * half_pi)
                f[i] = obj
            fitness.append((f[1], f[2], f[3]))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class DTLZ4Hive(ABCHive):

    def evaluate(self, positions):
        """
            Not sure about implementation ...
            Optimal Pareto front for xi == 0.5 for i in range(M, n) with M == 3 and n == 12.
            Represent a convex pareto front in 3D.
        """
        nb_simulations = len(positions)
        fitness = []
        M = 3  # Number of objectives
        m, n = 1, 12
        alpha = 100
        half_pi = math.pi / 2.0
        for row_id in range(nb_simulations):
            x = {i: positions['x{}'.format(i)][row_id] for i in range(m, n + 1)}
            G = sum((x[i] - 0.5) ** 2 for i in range(M, n + 1))

            # Objectives
            f = {}
            for i in range(1, M + 1):
                obj = 1 + G
                for j in range(1, M - i + 1):
                    obj *= math.cos(x[j] ** alpha * half_pi)
                if i > 1:
                    obj *= math.sin(x[M - i + 1] ** alpha * half_pi)
                f[i] = obj
            fitness.append((f[1], f[2], f[3]))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class DTLZ5Hive(ABCHive):

    def evaluate(self, positions):
        """
            Optimal Pareto front for xi == 0.5 for i in range(M, n) with M == 3 and n == 12.
            From https://pypi.python.org/pypi/optproblems
            Front is a 3D line not a surface.
        """
        nb_simulations = len(positions)
        fitness = []
        M = 3  # Number of objectives
        m, n = 1, 12
        for row_id in range(nb_simulations):
            x = {i: positions['x{}'.format(i)][row_id] for i in range(m, n + 1)}
            G = sum((x[i] - 0.5) ** 2 for i in range(M, n + 1))
            # Bias to get only a curve and not a surface
            temp_t = math.pi / (4.0 * (1.0 + G))
            theta = {i: temp_t * (1.0 + 2.0 * G * x[i]) for i in range(2, M)}
            theta[1] = x[1] * math.pi / 2.0
            # Objectives
            f = {}
            for i in range(1, M + 1):
                obj = 1 + G
                for j in range(1, M - i + 1):
                    obj *= math.cos(theta[j])
                if i > 1:
                    obj *= math.sin(theta[M - i + 1])
                f[i] = obj
            fitness.append((f[1], f[2], f[3]))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class DTLZ6Hive(ABCHive):

    def evaluate(self, positions):
        """
            Optimal Pareto front for xi == 0.5 for i in range(M, n) with M == 3 and n == 12.
            From https://pypi.python.org/pypi/optproblems
            Front is a 3D line not a surface.
        """
        nb_simulations = len(positions)
        fitness = []
        M = 3  # Number of objectives
        m, n = 1, 12
        for row_id in range(nb_simulations):
            x = {i: positions['x{}'.format(i)][row_id] for i in range(m, n + 1)}
            G = sum(x[i] ** 0.1 for i in range(M, n + 1))
            # Bias to get only a curve and not a surface
            temp_t = math.pi / (4.0 * (1.0 + G))
            theta = {i: temp_t * (1.0 + 2.0 * G * x[i]) for i in range(2, M)}
            theta[1] = x[1] * math.pi / 2.0
            # Objectives
            f = {}
            for i in range(1, M + 1):
                obj = 1 + G
                for j in range(1, M - i + 1):
                    obj *= math.cos(theta[j])
                if i > 1:
                    obj *= math.sin(theta[M - i + 1])
                f[i] = obj
            fitness.append((f[1], f[2], f[3]))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class DTLZ7Hive(ABCHive):

    def evaluate(self, positions):
        """
            Optimal Pareto front for xi == 0.5 for i in range(M, n) with M == 3 and n == 22.
            Front is discrete.
        """
        nb_simulations = len(positions)
        fitness = []
        M = 3  # Number of objectives
        m, n = 1, 22
        k = n - M + 1
        for row_id in range(nb_simulations):
            x = {i: positions['x{}'.format(i)][row_id] for i in range(m, n + 1)}
            G = 1.0 + 9.0 / k * sum(x[i] for i in range(M, n + 1))
            # Objectives
            f = {i: x[i] for i in range(1, M)}
            # Last depends on G and other
            H = M
            for i in range(1, M):
                H -= f[i] / (1.0 + G) * (1.0 + math.sin(3.0 * math.pi * f[i]))
            f[M] = (1.0 + G) * H
            fitness.append((f[1], f[2], f[3]))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


# Must be checked
# class DTLZ8Hive(ABCHive):

#     def evaluate(self, positions):
#         """
#             Optimal Pareto front for xi == 0.5 for i in range(M, n) with M == 3.
#             M constraints and n == 10 * M
#         """
#         nb_simulations = len(positions)
#         fitness = []
#         constraints = []
#         M = 3  # Number of objectives
#         m, n = 1, 10 * M
#         for row_id in range(nb_simulations):
#             x = {i: positions['x{}'.format(i)][row_id] for i in range(m, n + 1)}
#             # Objectives
#             f = {}
#             for j in range(1, M + 1):
#                 # ERROR
#                 # index 0 does not exist ...
#                 # ERROR
#                 # mini_i, maxi_i = floor((j - 1) * n / M), floor(j * n / M)
#                 mini_i, maxi_i = floor(j * n / M), floor(j * n / M)
#                 f[j] = (1 / floor(n / M)) * sum(x[i] for i in range(mini_i, maxi_i + 1))
#             fitness.append((f[1], f[2], f[3]))

#             # Constraints
#             g1 = f[M] + 4 * f[1] - 1
#             g2 = f[M] + 4 * f[2] - 1
#             mini = min(f[i] + f[1] for i in range(1, M) if i != 1)  # f[2] + f[1] (M==3)
#             g3 = 2 * f[M] + mini - 1
#             # Constraint only if negative
#             g1 = abs(g1) if g1 < 0 else 0
#             g2 = abs(g2) if g2 < 0 else 0
#             g3 = abs(g3) if g3 < 0 else 0
#             constraints.append((g1, g2, g3))
#         return fitness, constraints
