# -*- coding:Utf8 -*-

"""
    Problem.other.py

    Other problems definition.
    [Functions](https://en.wikipedia.org/wiki/Test_functions_for_optimization)

    Without constraints :
        - Kursawe
        - Viennet
        - FonsecaFleming
        - Poloni
        - BiModalConvex
        - Rastrigin
        - Matyas
        - ConvexGlobalNonConvexLocal

    With constraints
        - ConstrEx
        - TNK
        - Binh
        - OsyczkaKundu
        - ChakongHaime
        - BinhKorn

"""

import math

from ..Algorithm import ABCHive


__all__ = ['KursaweHive', 'ViennetHive', 'FonsecaFlemingHive', 'PoloniHive',
           'BiModalConvexHive', 'RastriginHive', 'MatyasHive', 'ConvexGlobalNonConvexLocalHive',
           'ConstrExHive', 'TNKHive', 'BinhHive', 'OsyczkaKunduHive',
           'ChakongHaimesHive', 'BinhKornHive']


class KursaweHive(ABCHive):

    def evaluate(self, positions):
        nb_simulations = len(positions)
        fitness = []
        m, n = 1, 3
        for row_id in range(nb_simulations):
            f1 = sum(-10 * math.exp(-0.2 *
                                    math.sqrt(positions['x{}'.format(i)][row_id] ** 2 +
                                              positions['x{}'.format(i + 1)][row_id] ** 2))
                     for i in range(m, n))

            f2 = sum(abs(positions['x{}'.format(i)][row_id]) ** 0.8 + 5 *
                     math.sin(positions['x{}'.format(i)][row_id] ** 3)
                     for i in range(m, n + 1))
            fitness.append((f1, f2))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class ViennetHive(ABCHive):

    def evaluate(self, positions):
        nb_simulations = len(positions)
        fitness = []
        for row_id in range(nb_simulations):
            x, y = positions['x'][row_id], positions['y'][row_id]
            f1 = 0.5 * (x * x + y * y) + math.sin(x * x + y * y)
            f2 = ((3 * x - 2 * y + 4) ** 2) / 8 + ((x - y + 1) ** 2) / 27 + 15
            f3 = 1 / (x * x + y * y + 1) - 1.1 * math.exp(-(x * x + y * y))
            fitness.append((f1, f2, f3))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class FonsecaFlemingHive(ABCHive):

    def evaluate(self, positions):
        nb_simulations = len(positions)
        fitness = []
        n = 20
        for row_id in range(nb_simulations):
            x = positions['x'][row_id]
            x1, x2 = 0, 0
            for i in range(1, n + 1):
                x1 += (x - 1 / math.sqrt(n)) ** 2
                x2 += (x + 1 / math.sqrt(n)) ** 2
            f1 = 1 - math.exp(-x1)
            f2 = 1 - math.exp(-x2)
            fitness.append((f1, f2))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class PoloniHive(ABCHive):

    def evaluate(self, positions):
        nb_simulations = len(positions)
        fitness = []
        # Const
        A1 = 0.5 * math.sin(1) - 2 * math.cos(1) + math.sin(2) - 1.5 * math.cos(2)
        A2 = 1.5 * math.sin(1) - math.cos(1) + 2 * math.sin(2) - 0.5 * math.cos(2)
        for row_id in range(nb_simulations):
            x, y = positions['x'][row_id], positions['y'][row_id]
            B1 = 0.5 * math.sin(x) - 2 * math.cos(x) + math.sin(y) - 1.5 * math.cos(y)
            B2 = 1.5 * math.sin(x) - math.cos(x) + 2 * math.sin(y) - 0.5 * math.cos(y)
            f1 = 1 + (A1 - B1) ** 2 + (A2 - B2) ** 2
            f2 = (x + 3) ** 2 + (y + 1) ** 2
            fitness.append((f1, f2))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class ConstrExHive(ABCHive):

    def evaluate(self, positions):
        nb_simulations = len(positions)
        fitness = []
        constraints = []
        for row_id in range(nb_simulations):
            x, y = positions['x'][row_id], positions['y'][row_id]
            # Fitness
            f1 = x
            f2 = (1 + y) / x
            fitness.append((f1, f2))
            # Constraint
            g1 = y + 9 * x - 6
            g2 = -y + 9 * x - 1
            # Constraint only if negative
            g1 = abs(g1) if g1 < 0 else 0
            g2 = abs(g2) if g2 < 0 else 0
            constraints.append((g1, g2))
        return fitness, constraints


class BinhKornHive(ABCHive):

    def evaluate(self, positions):
        nb_simulations = len(positions)
        fitness = []
        constraints = []
        for row_id in range(nb_simulations):
            x, y = positions['x'][row_id], positions['y'][row_id]
            # Fitness
            f1 = 4 * x ** 2 + 4 * y ** 2
            f2 = (x - 5) ** 2 + (y - 5) ** 2
            fitness.append((f1, f2))
            # Constraint
            g1 = (x - 5) ** 2 + y ** 2 - 25
            g2 = (x - 8) ** 2 + (y + 3) ** 2 - 7.7
            # Constraint only if positive
            g1 = abs(g1) if g1 > 0 else 0
            # Constraint only if negative
            g2 = abs(g2) if g2 < 0 else 0
            constraints.append((g1, g2))
        return fitness, constraints


class ChakongHaimesHive(ABCHive):

    def evaluate(self, positions):
        nb_simulations = len(positions)
        fitness = []
        constraints = []
        for row_id in range(nb_simulations):
            x, y = positions['x'][row_id], positions['y'][row_id]
            # Fitness
            f1 = 2 + (x - 2) ** 2 + (y - 1) ** 2
            f2 = 9 * x - (y - 1) ** 2
            fitness.append((f1, f2))
            # Constraint
            g1 = x ** 2 + y ** 2 - 225
            g2 = x - 3 * y + 10
            # Constraint only if positive
            g1 = abs(g1) if g1 > 0 else 0
            g2 = abs(g2) if g2 > 0 else 0
            constraints.append((g1, g2))
        return fitness, constraints


class OsyczkaKunduHive(ABCHive):

    def evaluate(self, positions):
        nb_simulations = len(positions)
        fitness = []
        constraints = []
        m, n = 1, 6
        for row_id in range(nb_simulations):
            x = {i: positions['x{}'.format(i)][row_id] for i in range(m, n + 1)}
            # Fitness
            f1 = (-25 * (x[1] - 2) ** 2 - (x[2] - 2) ** 2 - (x[3] - 1) ** 2 -
                  (x[4] - 4) ** 2 - (x[5] - 1) ** 2)
            f2 = sum(x[i] ** 2 for i in range(m, n + 1))
            fitness.append((f1, f2))
            # Constraint
            g1 = x[1] + x[2] - 2
            g2 = 6 - x[1] - x[2]
            g3 = 2 - x[2] + x[1]
            g4 = 2 - x[1] + 3 * x[2]
            g5 = 4 - (x[3] - 3) ** 2 - x[4]
            g6 = (x[5] - 3) ** 2 + x[6] - 4
            # Constraint only if negative
            g1 = abs(g1) ** 2 if g1 < 0 else 0
            g2 = abs(g2) ** 2 if g2 < 0 else 0
            g3 = abs(g3) ** 2 if g3 < 0 else 0
            g4 = abs(g4) ** 2 if g4 < 0 else 0
            g5 = abs(g5) ** 2 if g5 < 0 else 0
            g6 = abs(g6) ** 2 if g6 < 0 else 0
            constraints.append((g1, g2, g3, g4, g5, g6))
        return fitness, constraints


class BinhHive(ABCHive):

    def evaluate(self, positions):
        nb_simulations = len(positions)
        fitness = []
        constraints = []
        for row_id in range(nb_simulations):
            x, y = positions['x'][row_id], positions['y'][row_id]
            # Fitness
            f1 = x ** 2 - y
            f2 = -0.5 * x - y - 1
            fitness.append((f1, f2))
            # Constraint
            g1 = 6.5 - x / 6 - y
            g2 = 7.5 - 0.5 * x - y
            g3 = 30 - 5 * x - y
            # Constraint only if negative
            g1 = abs(g1) if g1 < 0 else 0
            g2 = abs(g2) if g2 < 0 else 0
            g3 = abs(g3) if g3 < 0 else 0
            constraints.append((g1, g2, g3))
        return fitness, constraints


class TNKHive(ABCHive):

    def evaluate(self, positions):
        nb_simulations = len(positions)
        fitness = []
        constraints = []
        for row_id in range(nb_simulations):
            x1, x2 = positions['x1'][row_id], positions['x2'][row_id]
            # Fitness
            f1, f2 = x1, x2
            fitness.append((f1, f2))
            # Constraint
            g1 = -(x1 ** 2) - x2 ** 2 + 1 + 0.1 * math.cos(16 * math.atan2(x1, x2))
            g2 = (x1 - 0.5) ** 2 + (x2 - 0.5) ** 2
            # Constraint only if negative
            g1 = abs(g1) if g1 > 0 else 0
            g2 = abs(g2) if g2 > 0.5 else 0
            constraints.append((g1, g2))
        return fitness, constraints


class BiModalConvexHive(ABCHive):

    def evaluate(self, positions):
        """
            Optimal Pareto front for x2 ~ 0.2.
            Convex Pareto-optimal front.
            Includes two difficulties cause by the nonuniformity of the search space.
            First pareto is not uniformly distributed along the global Pareto front
            Second the density of the solutions is lowest near the Pareto-optimal front
            and highest near a local pareto front.
            This lead to a complex optimization problem.
        """
        nb_simulations = len(positions)
        fitness = []
        for row_id in range(nb_simulations):
            x1 = positions['x1'][row_id]
            x2 = positions['x2'][row_id]
            f1 = x1
            G = 2 - math.exp(-((x2 - 0.2) / 0.004) ** 2) - 0.8 * math.exp(-((x2 - 0.6) / 0.4) ** 2)
            f2 = G / x1
            fitness.append((f1, f2))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class RastriginHive(ABCHive):

    def __init__(self, archive, configuration,
                 population_size, max_trial, fitness_type,
                 history=True, n=10):
        self.n = n
        super().__init__(archive, configuration,
                         population_size, max_trial, fitness_type,
                         history)

    def evaluate(self, positions):
        """
            Optimal Pareto front for G = 0 with (x[i] == 0 for i in range(1, 20)).
            Multi modal Rastrigin function with n == 20.
            One of the more tedious optimization problem.
        """
        nb_simulations = len(positions)
        fitness = []
        m, n = 1, self.n
        A = 10
        for row_id in range(nb_simulations):
            x = {i: positions['x{}'.format(i)][row_id] for i in range(m, n + 1)}
            f1 = positions['x0'][row_id]
            G = A * n + sum(x[i] ** 2 - A * math.cos(2 * math.pi * x[i]) for i in range(m, n + 1))
            # Add 1 to have multiple solution because optimal when G == 0
            f2 = (G + 1) / f1
            fitness.append((f1, f2))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class MatyasHive(ABCHive):

    def evaluate(self, positions):
        """
            Optimal Pareto front for G = 0 with x == y == 0.
            Convex Pareto-optimal front.
            Includes two difficulties cause by the nonuniformity of the search space.
            First pareto is not uniformly distributed along the global Pareto front
            Second the density of the solutions is lowest near the Pareto-optimal front
            and highest near a local pareto front.
            This lead to a complex optimization problem.
        """
        nb_simulations = len(positions)
        fitness = []
        for row_id in range(nb_simulations):
            x = positions['x'][row_id]
            y = positions['y'][row_id]
            z = positions['z'][row_id]
            f1 = z
            G = 0.26 * (x * x + y * y) - 0.48 * x * y
            f2 = (G + 1) / f1
            fitness.append((f1, f2))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints


class ConvexGlobalNonConvexLocalHive(ABCHive):

    def evaluate(self, positions):
        """

            EXPERIMENTAL !!!!!

            Optimal Pareto front for G = 0 with x == y == 0.
            Compose of multiple non convex pareto front (alpha == 4.0)
            and a convex global pareto front (alpha == 0.25)
        """
        nb_simulations = len(positions)
        fitness = []
        beta = 1
        g_star = 2
        g_star_star = 1
        for row_id in range(nb_simulations):
            x1 = positions['x1'][row_id]
            x2 = positions['x2'][row_id]
            f1 = x1 * 4

            # Compute G
            if x2 <= 0.4:
                # If 0 <= x2 <= 0.4
                G = 4 - 3 * math.exp(-((x2 - 0.2) / 0.02) ** 2)
                # G = 4 - 3 * math.exp(((x2 - 0.2) / 0.02) ** 2)
                # G = 4 - 3 * math.exp(-((x2 - 0.2) / 0.02)) ** 2
            else:
                # If 0.4 < x2 <= 1
                G = 4 - 2 * math.exp(-((x2 - 0.7) / 0.2) ** 2)
                # G = 4 - 2 * math.exp(((x2 - 0.7) / 0.2) ** 2)
                # G = 4 - 2 * math.exp(-((x2 - 0.7) / 0.2)) ** 2

            # Compute alpha
            alpha = 0.25 + 3.75 * (G - g_star_star) / (g_star - g_star_star)

            # Compute H
            H = 0
            if f1 <= (beta * G):
                H = 1 - (f1 / (beta * G)) ** alpha
            f2 = G * H
            fitness.append((f1, f2))
        constraints = (None for _ in range(nb_simulations))
        return fitness, constraints
