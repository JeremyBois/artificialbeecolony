# -*- coding:Utf8 -*-

"""
    An optimization utility package. Provide an implementation of ABC meta-heuristique
    as an abstrcat class which can be used to implement any type of problem.
    This package can also be used to create a new algorithm implementation using
    elements from *Entity*, and *Primitive*.
"""

__title__ = 'pyMayBee'
__author__ = 'Jérémy Bois'
__version__ = '0.0.8'


# Internal random generator
from random import Random
RandomGenerator = Random()

# Make all element accessible from a single import statement
from . import Algorithm
from . import Entity
from . import Misc
from . import Primitive


import logging
try:  # Python 2.7+
    from logging import NullHandler
except ImportError:
    class NullHandler(logging.Handler):
        """Create a NullHandler."""

        def emit(self, record):
            pass

# Assign NullHandler as default root handler
logging.getLogger(__name__).addHandler(logging.NullHandler())
