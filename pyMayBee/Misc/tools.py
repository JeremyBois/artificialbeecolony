# -*- coding:Utf8 -*-

"""
    Module tools.py:
    Utilities functions.

        - :func:`pyMayBee.Misc.tools.binary_search`: Find element index in a probability table
        - :func:`pyMayBee.Misc.tools.closest`: Find closest element value inside an array
        - :func:`pyMayBee.Misc.tools.from_json`: Wrapper function to load a JSON file
        - :func:`pyMayBee.Misc.tools.inherit_docstring`: Decorator that transfer parent class doc
          to decorated child
"""

import json
import inspect

from path import Path
from math import floor


__all__ = ['merge_dicts', 'binary_search', 'closest', 'from_json', 'inherit_docstring']


def merge_dicts(*dict_args):
    """
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    """
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result


def binary_search(sorted_array, element):
    """Return the index of *element* inside the *sorted_array* which can be created
    from a probability table using :func:`pyMayBee.Misc.random_helper.init_roulette`.

    :param element: A number between *sorted_array[0]* and *sorted_array[-1]*
    :param sorted_array: A table of cumulated weights
    """
    # Fast look up
    if element >= sorted_array[-1]:
        return len(sorted_array) - 1
    left, right = 0, len(sorted_array) - 1
    while not(left > right):
        m = floor((left + right) / 2)
        if sorted_array[m] < element:
            left = m + 1
        elif sorted_array[m] > element:
            right = m - 1
        else:
            # We found a perfect match
            break
    # Weight probabilities define a range between each sorted_array element.
    # When a random number is between sorted_array[0] and sorted_array[1] then we
    # want to return 1 not 0 in case of a roulette wheel.
    if not(sorted_array[m] > element):
        return m + 1
    else:
        return m


def closest(sorted_array, element):
    """Find the closest element in *sorted_array* in O(log(n)) time.

    .. seealso::
        :func:`binary_search` to find the nearest greater index.
    """
    # Get index of closest greater element
    idx = binary_search(sorted_array, element)
    # Decremental Boolean (True == 1, False == 0)
    idx -= sorted_array[idx] - element > element - sorted_array[max(0, idx - 1)]
    return sorted_array[idx]


def from_json(json_file, encoding='utf-8', debug=False, object_pairs_hook=None):
    """Read *json_file* and return formatted informations as a :class:`dict` object.
    Can return a OrderedDict object by setting *object_pairs_hook* to :class:`OrderedDict`

    :param json_file: A json file **Path** storing simulation process informations.
    """
    # Read json
    with open(Path(json_file), 'r', encoding=encoding) as j_file:
        data = json.load(j_file, encoding=encoding, object_pairs_hook=object_pairs_hook)
    if debug:
        pp(data)
    # Extract config
    return data


def pp(json_dict, sort_keys=True, indent=4):
    """Json dumps wrapper to easy access to pretty print of json files.

    :param sort_keys: Sort output keys (default to True)
    :param indent: Indentation level used (default to 4)

    ..see also::`json.dump`
    """
    print(json.dumps(json_dict, sort_keys=sort_keys,
                     indent=indent, separators=(',', ': ')))


def inherit_docstring(cls):
    """Inherite parent docstring to child.
    Can be used as a decorator for class creation.
    """
    for base in inspect.getmro(cls):
        if base.__doc__ is not None:
            cls.__doc__ = base.__doc__
            break
    return cls
