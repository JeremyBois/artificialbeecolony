# -*- coding:Utf8 -*-

"""
    Module random_helper.py:
    Provide some helper functions to compute random numbers.

        - :func:`pyMayBee.Misc.random_helper.launch_roulette`: Pick a random number
          using a cumulated density probability constructed from a probability
          table
        - :func:`pyMayBee.Misc.random_helper.init_roulette`: Transform a probability
          table to a cumulated density probability array.
        - :func:`pyMayBee.Misc.random_helper.levy_flight`: Perform a random walk from
          from a Lévy distribution.

          .. seealso::
            :func:`pyMayBee.Misc.random_helper.levy_step` and
            :func:`pyMayBee.Misc.random_helper.directed_levy_flight`
        - :func:`pyMayBee.Misc.random_helper.uniform_motion`: Perform a random walk from
          from a Uniform distribution.

          .. seealso::
            :func:`pyMayBee.Misc.random_helper.directed_uniform_motion`
        - :func:`pyMayBee.Misc.random_helper.brownian_motion`: Perform a random walk from
          from a Normale (Gaussian) distribution.
"""

import math

# Get internal random generator
from pyMayBee import RandomGenerator

from .tools import binary_search


# Limit star expression side effect
__all__ = []


def set_random_seed(value, version=2):
    """Set seed *value* used package wide for random number generation."""
    RandomGenerator.seed(value, version)


def levy_step(beta=1.5):
    """Compute a levy step according to **Mantagna** algorithm.

    :param beta: ``0 < beta < 2``
    :Return float: A number picked from a symetrical and stable Lévy distribution.

    """
    sigma_v = 1
    sigma_u = ((math.gamma(1 + beta) * math.sin(math.pi * beta / 2)) /
               (math.gamma((1 + beta) / 2) * beta * 2**((beta - 1) / 2))) ** (1 / beta)
    # Compute random number using gaussian distribution
    u = RandomGenerator.gauss(0, sigma_u * sigma_u)
    v = RandomGenerator.gauss(0, sigma_v)
    return u / (abs(v) ** (1 / beta))


def init_roulette(probs):
    """Initialize a roulette wheel selection algorithm using weights from *probs*
    Return an array of cumulated probabilities. If probs are normalized then
    the array start from probs[0] and the last element is 1.

    :param probs: A probability or weight table.
    """
    cum_weight_array = [probs[0], ]
    for (ind, prob) in enumerate(probs[1:]):
        cum_weight_array.append(cum_weight_array[ind] + prob)
    return cum_weight_array


def launch_roulette(cum_weight_array):
    """Pickup randomly (Uniform distribution) a number using the cumulated density
    probability define inside the *cum_weight_array* using the search algorithm
    :func:`pyMayBee.Misc.tools.binary_search`.

    :param cum_weight_array: A cumulated weight array constructed based on
                             a probability table.
    :Return:  the *cum_weight_array* **INDEX** corresponding to the picked number.

    .. seealso::
        :func:`init_roulette`
    """
    picked_element = RandomGenerator.uniform(0, cum_weight_array[-1])
    return binary_search(cum_weight_array, picked_element)


def roll(prob_tab):
    """Return a random value according to a probability table *prob_tab*
    (weight, value) couples.
    Prob_tab probabilities does not have to be normalized.

    :param prob_tab: A Sequence with (weight, val) as elements where weight is a
                     probability law if sum((i[0] for i in prob_tab)) == 1.

    .. seealso:: `lauch_roulette` to speed up selection if multiple lookup.
    """

    def lookup_value(x):
        """Look after the value *x* inside the prob_tab in O(n) and return it.
        Loop over each element of the table until cumulative x
        is bigger than *x*.
        :param x: A value in [0, sum_of_weights)"""
        cumulative_weight = 0
        for (weight, value) in prob_tab:
            cumulative_weight += weight
            if x < cumulative_weight:
                return value

    sum_of_weights = sum((weight for (weight, _) in prob_tab))
    return lookup_value(RandomGenerator.randint(0, sum_of_weights - 1))


def levy_flight(current_value, ref_value, beta=1.5, scale_factor=0.01):
    """Generation of a **random walk** using Lévy stable and symetrical distribution."""
    return scale_factor * levy_step(beta=beta) * (current_value - ref_value)


def directed_levy_flight(current_value, ref_value, beta=1.5, scale_factor=0.01):
    """Generation of a directed **random walk** using Lévy stable and symetrical distribution."""
    return scale_factor * abs(levy_step(beta=beta)) * (ref_value - current_value)


def brownian_motion(current_value, ref_value, mu=0, sigma=1):
    """Generation of a **random walk** using normal distribution."""
    return RandomGenerator.gauss(mu, sigma) * (current_value - ref_value)


def directed_uniform_motion(current_value, ref_value, scale_factor=0.7):
    """Generation of a directed **random walk** using uniform distribution."""
    return scale_factor * RandomGenerator.uniform(0, 1) * (ref_value - current_value)


def uniform_motion(current_value, ref_value, scale_factor=0.7):
    """Generation of a **random walk** using uniform distribution."""
    return scale_factor * RandomGenerator.uniform(-1, 1) * (current_value - ref_value)
