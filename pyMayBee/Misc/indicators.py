# -*- coding:Utf8 -*-

"""
    Misc.indicator.py
"""


import numpy as np
import pandas as pd
import pygmo as pg

import collections


__all__ = ['compute_hypervolumes', 'metric_D', 'schott', 'generational_dist',
           'inv_generational_dist', 'find_nearest', 'normalize']


def find_nearest(array, value):
    """Return index of closest element from *value* in the *array*."""
    idx = (np.abs(array - value)).argmin()
    return array[idx]


def normalize(archives, minimization=False):
    """Normalize each archive in *archives* using min and max from
    **all** set of archive sources.

    :param archives: A dict where keys are iteration number and values
                     are :class:`collection.Container` and each element
                     have a fitness attribute (:class:`Primitive.fitness.Fitness`).
                     If not assume its a container.

    :param minimization: Set to :data:`True` to transform each sources fitness
                         to assume minimization because some indicators assume
                         minimization on their own.
    :Return: Concatenated *archives* as a :class:`pd.DataFrame` and columns
             names for each objective in the Fitness order.
    """
    # Make sure we have a dict like interface
    if not isinstance(archives, collections.Mapping):
        archives = {1: archives}

    # pyMayBee assume maximization in Fitness class
    # Negative multiplier to assume minimization
    multiplier = -1.0 if minimization else 1.0

    # Concatenate together all data to be able to normalize them
    fronts = list()
    for iteration, archive in archives.items():
        sources = pd.DataFrame([src.fitness.w_values for src in archive]) * multiplier
        # Keep archive number
        sources['iteration'] = iteration
        fronts.append(sources)
    archives_pop = pd.concat(fronts)

    # Normalize each objective using (val - min) / (max - min)
    objectives = [col for col in archives_pop.columns if col != 'iteration']
    array = np.array(archives_pop[objectives])
    archives_pop[objectives] = (array - array.min(0)) / array.ptp(0)

    return archives_pop, objectives


def compute_hypervolumes(archives, step=1, ref='Nadir'):
    """Extract each archive from *archives* and compute hypervolumes
    of multiple iteration based on *step*.

    :param archives: A dict where keys are iteration number and values
                     are :class:`collection.Container` and each element
                     have a fitness attribute (:class:`Primitive.fitness.Fitness`).
                     If not assume its a container.
    :param step: Can be used to only compute hypervolume each *step* iteration.

    :param ref: Select reference point. Default to Nadir point.

    :Return: hypervolume values as a list

    .. warning::
        *archives* are first transformed to assume minimization (as assume for hypervolume computation)
        then normalized to avoid scale difference between objectives.

    .. note::
        Hypervolume compute using **pygmo** library
        Copyright 2017, pagmo development team., see
        https://github.com/esa/pagmo/blob/master/LICENSE.txt
    """
    # Update values to assume minimization and normalize them
    fronts, objectives = normalize(archives, minimization=True)

    # Nadir always [1] * number of objectives due to normalization.
    # Add 0.1 since any point sharing the "worst" value for a given objective
    # with the reference point will contribute zero to the overall hypervolume
    if ref == 'Nadir':
        ref_point = [1.1] * len(objectives)
    else:
        # pygmo.hypervolume as a method 'refpoint' but I prefer to get
        # worst on all archive population than only the one used to compute
        # hypervolume
        raise NotImplementedError('Only shifted Nadir point supported')

    # Compute hypervolumes for each iteration
    hypervolumes = []
    last_front = fronts['iteration'].max()
    first_front = fronts['iteration'].min()
    for iteration in range(first_front, last_front + 1, step):
        front = fronts[fronts['iteration'] == iteration][objectives].values
        hv = pg.hypervolume(front)
        hypervolumes.append(hv.compute(ref_point))

    return hypervolumes


def metric_D(archive_a, archive_b, **kwargs):
    """Metric which compute the area only covered by *archive_a* and
    is defined as **D(archive_a, archive_b)**.

    :param archive_a: A :class:`Primitive.archive.ArchiveInterface`.
    :param archive_b: A :class:`Primitive.archive.ArchiveInterface`.

    :Return: ``hypervolume(archive_a + archive_b) - hypervolume(archive_b)``
    """
    # Merge _a and _b together before computing hypervolumes
    # Archive is mutable so first make a true copy
    merged_archive = archive_a.clone()
    merged_archive.merge(archive_b)

    # Then assign fake iteration number to compute normalization on full population
    archives = {0: archive_a, 1: archive_b, 2: merged_archive}

    # Compute hypervolume for all fronts to get correct min and max for normalization
    hypervolumes = compute_hypervolumes(archives)

    return hypervolumes[2] - hypervolumes[1]


def schott(archive, **kwargs):
    """Compute Schott Spacing metric which can be used to measure uniformity of
    *archive* sources without any knowledge of true Pareto front.
    The closer to zero, the better.

    :param archive: A :class:`collection.Container` where each element
                    have a fitness attribute (:class:`Primitive.fitness.Fitness`).
    """
    # Update values to assume minimization and normalize them
    front, objectives = normalize(archive, minimization=False)

    # Get numpy 2D array
    w_src = front[objectives].values

    nb_src = w_src.shape[0]
    spacing_dists = np.zeros(nb_src)
    # Iterate over sources fitness
    for idx in range(nb_src):
        # Removed current from population before computing distance
        current = w_src[idx, :]
        other = w_src[np.where(np.any(current != w_src, axis=1))]

        # Get spacing distance from current to closest other source fitness
        spacing_dists[idx] = np.min(np.sum(np.abs(other - current), axis=1))

    # Now compute mean spacing distance
    average_spacing = np.average(spacing_dists)

    # Compute variance
    space_dist = np.sqrt((1 / (nb_src - 1)) *
                         np.sum(np.power(average_spacing - spacing_dists, 2)))

    return space_dist


def generational_dist(front, p_front, coef=1, improved=True, **kwargs):
    """Compute the generational distance of the *front* to true Pareto front, *p_front*
    defined as GD(front, p_front).
    Generational distance is not Pareto compliant.

    :param front: A :class:`Set` of non-dominated sources having
                  a :attr:`fitness` attribute.
    :param p_front: A :class:`Set` of true Pareto non-dominated sources having
                    a :attr:`fitness` attribute.
    :param coef: Coefficient used in generational distance (Default to 1).
    :param improved: Prefer **Schutze et al.** modification (see 10.1109/TEVC.2011.2161872)

    .. note::
        Result strongly depends on the distribution of point used as Pareto
        front *p_front* and a high resolution (many reference points) is needed
        to avoid counter intuitive results.

    .. note::
        For a quasi Pareto compliant implementation see works in:

          - 10.1109/MCDM.2014.7007204
          - 10.1007/978-3-319-15892-1_8
    """
    raise NotImplementedError('Math is correct but must update argument')
    w_src = np.array([src.fitness.w_values for src in front])
    w_src_p = np.array([src.fitness.w_values for src in p_front])
    nb_src = w_src.shape[0]
    dists = np.zeros(nb_src)
    # Iterate over sources fitness
    for idx in range(nb_src):
        # Removed current from population before computing distance
        current = w_src[idx, :]

        # Get squared euclidean distance from current to closest Pareto source fitness
        dists[idx] = np.min(np.sum(np.power(w_src_p - current, 2), axis=1))

    # Compute distances from squared distances
    dists = np.sum(np.power(np.sqrt(dists), coef))

    # Compute generational distance
    if improved:
        return (dists / nb_src) ** (1 / coef)
    else:
        return (dists ** (1 / coef)) / nb_src


def inv_generational_dist(front, p_front, coef=1, improved=True, **kwargs):
    """Compute the inverted generational distance of the *front* to true Pareto
    front, *p_front* defined as IGD(p_front, front).
    The IGD measure usually shows the overall quality of an obtained solution set
    Then, it can then be used to evaluated convergence and quality of *front*
    compare to the Pareto front, *p_front*.
    Inverted generational distance is not Pareto compliant.

    :param front: A :class:`Set` of non-dominated sources having
                  a :attr:`fitness` attribute.
    :param p_front: A :class:`Set` of true Pareto non-dominated sources having
                    a :attr:`fitness` attribute.
    :param coef: Coefficient used in generational distance (Default to 1).
    :param improved: Prefer **Schutze et al.** modification (see 10.1109/TEVC.2011.2161872)

    .. note::
        Result strongly depends on the distribution of point used as Pareto
        front *p_front* and a high resolution (many reference points) is needed
        to avoid counter intuitive results.

    .. note::
        For a quasi Pareto compliant implementation see works in:

          - 10.1109/MCDM.2014.7007204
          - 10.1007/978-3-319-15892-1_8
    """
    raise NotImplementedError('Math is correct but must update argument')
    w_src = np.array([src.fitness.w_values for src in front])
    w_src_p = np.array([src.fitness.w_values for src in p_front])
    nb_src_p = w_src_p.shape[0]
    dists_p = np.zeros(nb_src_p)
    # Iterate over sources fitness
    for idx in range(nb_src_p):
        # Removed current from population before computing distance
        current_p = w_src_p[idx, :]

        # Get squared euclidean distance from current_p to closest front source fitness
        dists_p[idx] = np.min(np.sum(np.power(current_p - w_src, 2), axis=1))

    # Compute distances from squared distances
    dists_p = np.sum(np.power(np.sqrt(dists_p), coef))

    # Compute inverted generational distance
    if improved:
        return (dists_p / nb_src_p) ** (1 / coef)
    else:
        return (dists_p ** (1 / coef)) / nb_src_p
