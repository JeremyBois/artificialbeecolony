# -*- coding:Utf8 -*-

"""
    Module history_helper.py:
    Utilities functions to track algorithm evolution.

        - :func:`pyMayBee.Misc.history_helper.backup_sources`: Create a pickle and a CSV to
          save sources evolution for each iteration and each phase. Also save to a JSON
          iteration informations such as max, min, ...
        - :func:`pyMayBee.Misc.history_helper.backup_archive`: Create a pickle and a CSV to
          save archive evolution at each iteration.
        - :func:`pyMayBee.Misc.history_helper.backup_population`: Create a pickle and a CSV to
          save hive (population) evolution.
        - :func:`pyMayBee.Misc.history_helper.save_history`: Complete saving of the algorithm
          states.

          .. seealso::
              :func:`pyMayBee.Misc.history_helper.cycle_backup`
        - :func:`pyMayBee.Misc.history_helper.cycle_backup`: Complete saving of the algorithm
          states in a cycle process to keep previous state as backup.

          .. seealso::
              :func:`pyMayBee.Misc.history_helper.save_history`
        - :func:`pyMayBee.Misc.history_helper.get_state_from`: Retrieve algorithm state
          from backup folder using sources and archive backup.
"""

import json
import pickle
import csv

from path import Path
from collections import Sequence

from .tools import merge_dicts


__all__ = ['save_history', 'cycle_backup', 'get_state_from']


# This function must be updated each time AbstractCriterion.to_dict() is updated.
def _criteria_to_dict(criteria):
        """Create a dict representation from a list of criteria.
        Value for each criterion is mapped to its name.

        Temporary solution due to cycling import if trying to use classmethod.

        :param criteria: A dict or Sequence which contains multiple criteria definition.
        """
        if isinstance(criteria, dict):
            return {criterion.name: criterion.value for criterion in criteria.values()}
        elif isinstance(criteria, Sequence):
            return {criterion.name: criterion.value for criterion in criteria}
        else:
            return {criteria.name: criteria.value}


def backup_archive(file_path, archive_hist, criteria_names, objectives_names,
                   constraints_names=None):
    """
        Store data inside *archive_hist* to *file_path* as a CSV with columns
        names composed of *criteria_names* + *objectives_names* + *constraints_names*.

        :param file_path: File name without extension used to store historic
        :param archive_hist: Hive historic of the archive where keys are iteration number
                             and values sources in the archive for that iteration.
        :param criteria_names: Names used for criteria
        :param objectives_names: Assign name to objectives
        :param constraints_names: Assign name to constraints
                                  (default to :data:`None` meaning no constraints)
    """
    # Constraint check
    HAVE_CONSTRAINTS = constraints_names is not None
    # Order is criteria, objectives, constraints. Keep same order for all CSV
    if HAVE_CONSTRAINTS:
        FIELDNAMES = list(criteria_names) + list(objectives_names) + list(constraints_names)
    else:
        FIELDNAMES = list(criteria_names) + list(objectives_names)
    # Write as a csv
    with open(file_path + '.csv', 'w', newline='') as csvfile:
        # Order is iteration, criteria, objectives, constraints
        archive_fieldnames = ['iteration', ] + FIELDNAMES
        writer = csv.DictWriter(csvfile, fieldnames=archive_fieldnames, delimiter=',')
        writer.writeheader()
        # Create a row for each source where first column is iteration
        for iteration in archive_hist:
            iteration_dict = {'iteration': iteration}
            archive = archive_hist[iteration]
            for source in archive:
                # Map value to criterion name
                criteria = _criteria_to_dict(source.position)
                # Map value to fitness name
                objectives = {name: value for (name, value) in zip(objectives_names, source.fitness.values)}
                # Map value to constraint name (Must always be (0,))
                if HAVE_CONSTRAINTS:
                    constraints = {name: value for (name, value) in zip(constraints_names, source.fitness.constraints)}
                else:
                    constraints = {}
                writer.writerow(merge_dicts(iteration_dict, criteria, objectives, constraints))


def backup_sources(file_path, sources_hist, criteria_names, objectives_names,
                   constraints_names=None):
    """
        Store data inside *sources_hist* to *file_path* as a CSV with columns
        names composed of *criteria_names* + *objectives_names* + *constraints_names*.
        Also store data inside *sources_hist* (min, max, nb_evaluation, max_constraints)
        to *file_path* as a JSON.

        :param file_path: File name without extension used to store historic
        :param sources_hist: Hive historic of the population

                             .. code-block:: python

                                 {'nb_iteration':
                                     'current_phase':
                                         {'max': val, 'min': val, 'constraints_max': val,
                                          'feasibility_rate': val, 'nb_evaluation': val,
                                          'sources': sources_snapshot}
                                     }
                                 }

        :param criteria_names: Names used for criteria
        :param objectives_names: Assign name to objectives
        :param constraints_names: Assign name to constraints
                                  (default to :data:`None` meaning no constraints)
    """
    # Constraint check
    HAVE_CONSTRAINTS = constraints_names is not None
    # Order is criteria, objectives, constraints. Keep same order for all CSV
    if HAVE_CONSTRAINTS:
        FIELDNAMES = list(criteria_names) + list(objectives_names) + list(constraints_names)
    else:
        FIELDNAMES = list(criteria_names) + list(objectives_names)
    # Create JSON with sources main parameters (min, max, feasibility)
    # Create CSV with sources for each phase (iteration, phase, position, objectives, constraints)
    json_dict = {}
    csv_dict = {}
    # Write as a csv
    with open(file_path + '.csv', 'w', newline='') as csvfile:
        # Order is iteration, criteria, objectives, constraints
        sources_fieldnames = ['iteration', 'phase', 'source_id'] + FIELDNAMES
        writer = csv.DictWriter(csvfile, fieldnames=sources_fieldnames, delimiter=',')
        writer.writeheader()
        # Create a row for each sources where first columns are (iteration, phase)
        for iteration in sources_hist:
            csv_dict['iteration'] = iteration
            phases = sources_hist[iteration]
            for phase in phases:
                parameters = sources_hist[iteration][phase]
                # Get (min, max, feasibility, nb_evaluation) but not (sources, ) for JSON
                stats_dict = {parameter: value for (parameter, value) in parameters.items() if parameter != 'sources'}
                json_dict.setdefault(iteration, {})[phase] = stats_dict
                # Get only sources for CSV
                csv_dict['phase'] = phase
                for (source_id, source) in enumerate(parameters['sources']):
                    # Use index as id to be able to follow a specific source evolution
                    csv_dict['source_id'] = source_id
                    # Map value to criterion name
                    criteria = _criteria_to_dict(source.position)
                    # Map value to fitness name
                    objectives = {name: value for (name, value) in zip(objectives_names, source.fitness.values)}
                    # Map value to constraint name
                    if HAVE_CONSTRAINTS:
                        constraints = {name: value for (name, value) in zip(constraints_names, source.fitness.constraints)}
                    else:
                        constraints = {}
                    writer.writerow(merge_dicts(csv_dict, criteria, objectives, constraints))
    # Write as a JSON
    with open(file_path + '.json', 'w', encoding='utf-8') as j_file:
        output = json.dumps(json_dict, ensure_ascii=False, indent=4)
        j_file.write(output)


def backup_population(file_path, population_hist, criteria_names, objectives_names,
                      constraints_names=None):
    """
        Store data inside *population_hist* to *file_path* as a CSV with columns
        names composed of *criteria_names* + *objectives_names* + *constraints_names*.

        :param file_path: File name without extension used to store historic
        :param population_hist: Hive historic of the population as a :class:`list`
                                of sources.
        :param criteria_names: Names used for criteria
        :param objectives_names: Assign name to objectives
        :param constraints_names: Assign name to constraints
                                  (default to :data:`None` meaning no constraints)
    """
    # Constraint check
    HAVE_CONSTRAINTS = constraints_names is not None
    # Order is criteria, objectives, constraints. Keep same order for all CSV
    if HAVE_CONSTRAINTS:
        FIELDNAMES = list(criteria_names) + list(objectives_names) + list(constraints_names)
    else:
        FIELDNAMES = list(criteria_names) + list(objectives_names)
    with open(file_path + '.csv', 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=FIELDNAMES, delimiter=',')
            writer.writeheader()
            # Add a row for each source inside population
            for source in population_hist:
                # Map value to criterion name
                criteria = _criteria_to_dict(source.position)
                # Map value to fitness name
                objectives = {name: value for (name, value) in zip(objectives_names, source.fitness.values)}
                # Map value to constraint name
                if HAVE_CONSTRAINTS:
                    constraints = {name: value for (name, value) in zip(constraints_names, source.fitness.constraints)}
                else:
                    constraints = {}
                writer.writerow(merge_dicts(criteria, objectives, constraints))


def save_history(hive, folder_path, objectives_names, constraints_names=None):
    """Save history as JSON or CSV and Pickle for:
        - population evolution : population_history.csv
        - sources evolution    : sources_history.json, sources_history.csv
        - archive evolution    : archive_history.csv

    **sources_history.json** contains statistics informations for each phase whereas
    **sources_history.csv** contains sources position, fitness, and constraints.

    :param hive: A :class:`ABCHive` sub-instance.
    :param folder_path: Folder used to store history.
    :param objectives_names: Assign name to objectives
    :param constraints_names: Assign name to constraints
                              (default to :data:`None` meaning no constraints)

    .. note::
        Can be a bit slow with a lot of data due to deeply nested structure.
    """
    # Get reference to avoid misspelling
    population_h = hive._population_history  # List
    sources_h = hive._sources_history        # Dict
    archive_h = hive._archive_history       # Dict

    folder_path = Path(folder_path).expand().abspath()

    # Pickle is easy
    with open(folder_path / 'population_history.p', 'wb') as my_file:
        pickle.dump(population_h, my_file)
    with open(folder_path / 'sources_history.p', 'wb') as my_file:
        pickle.dump(sources_h, my_file)
    with open(folder_path / 'archive_history.p', 'wb') as my_file:
        pickle.dump(archive_h, my_file)

    # Store for humans (format is function specific)
    criteria_names = list(population_h[0].position.keys())
    backup_population(folder_path / 'population_history', population_h,
                      criteria_names, objectives_names, constraints_names)
    backup_sources(folder_path / 'sources_history', sources_h,
                   criteria_names, objectives_names, constraints_names)
    backup_archive(folder_path / 'archive_history', archive_h,
                   criteria_names, objectives_names, constraints_names)


def cycle_backup(hive, folder_path, objectives_names, constraints_names=None,
                 cycling_size=3):
    """Save history as JSON or CSV and Pickle for:

        - population evolution : population_history.csv
        - sources evolution    : sources_history.json, sources_history.csv
        - archive evolution    : archive_history.csv

    **sources_history.json** contains statistics informations for each phase whereas
    **sources_history.csv** contains sources position, fitness, and constraints.

    Store files inside a sub-directory ('Backup') where *cycling_size* folders
    are created and each time this function is called a folder is selected
    as main folder and files are saved (older files are replaced).
    *cycling_size* defines the rate at which each sub-folders files are replaced.

    For example using *cycling_size* of 3 (default), 3 folders will be created
    (0, 1, 2) where:

        - Folder 0 used for iteration 0, 3, 6, ...
        - Folder 1 used for iteration 1, 4, 7, ...
        - Folder 2 used for iteration 2, 5, 8, ...

    This structure allows safe backup without having to create kept the whole
    history (each iteration).

    :param hive: A :class:`ABCHive` sub-instance.
    :param folder_path: Folder used to store backup.
    :param objectives_names: Assign name to objectives
    :param constraints_names: Assign name to constraints
                              (default to :data:`None` meaning no constraints)
    :param cycling_size: Change cycling saving measure. The bigger the more previous
                         backups are kept, the more memory is needed.

    .. warning::
        Cycle is based on hive.nb_iteration
    """
    # Get reference to avoid misspelling
    population_h = hive._population_history  # List
    sources_h = hive._sources_history        # Dict
    archive_h = hive._archive_history        # Dict

    # Assign subdirecectory for history storage
    folder_path = Path(folder_path).expand().abspath()
    folder_path /= 'Backup'
    # Create it if not yet exists
    folder_path.mkdir_p()

    # Create folders based on cycling size if not already created
    if not folder_path.dirs():
        for cycle in range(cycling_size):
            cycle_folder = folder_path / str(cycle)
            cycle_folder.mkdir()

    # Get correct folder path based on cycling size and iteration number
    dir_path = folder_path.dirs()[hive.nb_iteration % cycling_size]

    # Pickle history
    with open(dir_path / 'population_history.p', 'wb') as my_file:
        pickle.dump(population_h, my_file)
    with open(dir_path / 'sources_history.p', 'wb') as my_file:
        pickle.dump(sources_h, my_file)
    with open(dir_path / 'archive_history.p', 'wb') as my_file:
        pickle.dump(archive_h, my_file)

    # Humans format history
    criteria_names = list(population_h[0].position.keys())
    backup_population(dir_path / 'population_history', population_h,
                      criteria_names, objectives_names, constraints_names)
    backup_sources(dir_path / 'sources_history', sources_h,
                   criteria_names, objectives_names, constraints_names)
    backup_archive(dir_path / 'archive_history', archive_h,
                   criteria_names, objectives_names, constraints_names)
    # Return path used to save history if user need it
    return dir_path


def get_state_from(backup_folder, iteration=None, phase='Onlookers'):
    """Load sources and archive state from backup files and return
    optimization state, sources list and archive instance.

    :param backup_folder: Folder where pickle backup are stored
    :param iteration: The iteration to load (default to last iteration).

    :Return: Tuple as (state, sources, archive,
             (population_history, sources_hist, archive_hist)

    .. note::
        The :class:`Primitive.fitness.Fitness` subinstance used must be
        defined before to pickle to find it.
    """
    # Avoid cycling import
    from pyMayBee.Entity.sources import FoodSource

    backup_folder = Path(backup_folder).expand().abspath()
    with open(backup_folder / 'population_history.p', 'rb') as population_file:
        population_hist = pickle.load(population_file)
    with open(backup_folder / 'archive_history.p', 'rb') as archive_file:
        archive_hist = pickle.load(archive_file)
    with open(backup_folder / 'sources_history.p', 'rb') as sources_file:
        sources_hist = pickle.load(sources_file)
    if iteration is None:
        iteration = max(archive_hist.keys())

    # Extract optimization progress from sources history
    config = {}
    iteration_info = sources_hist[iteration][phase]
    config['nb_sources'] = len(iteration_info['sources'])
    config['nb_iteration'] = iteration
    for (key, value) in iteration_info.items():
        if key != 'sources':
            config[key] = value

    # Extract *iteration* state for sources ...
    sources = []
    for source in iteration_info['sources']:
        sources.append(FoodSource(source.position, source.fitness))
        sources[-1]._trial = source.trial  # -1 always return last element

    # ... and archive
    archive = archive_hist[iteration]

    # History as a tuple
    history = (population_hist, sources_hist, archive_hist)

    return config, sources, archive, history
