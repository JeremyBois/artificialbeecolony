# -*- coding:Utf8 -*-

"""
    Module archive.py:
    Helper classes to create archives. Archives are all container and mutable.

        - :class:`pyMayBee.Primitive.archive.ArchiveInterface` : Interface which can
          be used to construct a Archive structure
        - :class:`pyMayBee.Primitive.archive.EpsilonArchive` : Implementation of an
          :math:`\epsilon`-Archive which cut in hyper-cube the objective space
          Only a solution is allowed per hyper-cube to avoid redundancy.
"""

import pyMayBee.Misc.random_helper as rh

from collections import Container

from enum import Enum
from math import ceil

import copy


__all__ = ["EpsilonArchive"]


class ArchiveInterface(Container):

    """Archive Interface which can be used to implement more complex container.

    .. seealso::
        :class:`EpsilonArchive` for a :math:`\epsilon-box` dominated implementation
        which cut in hyper-cube the objective domain.
    """

    def __init__(self):
        # Keep solutions inside the archive as a list
        self.archive = list()

    @property
    def is_define(self):
        return self.__bool__()

    def add(self, new_source):
        self.archive.append(new_source)

    def merge(self, other_archive):
        """Merge solutions of *other_archive* into the archive.

        :param other_archive: Every kind of iterable (list, tuple, Archive, ...)
        """
        for source in other_archive:
            self.add(source)

    def pick_random(self):
        """Pick a random source inside the archive."""
        return rh.RandomGenerator.choice(self.archive)

    def copy(self):
        """Internal alias for ``copy.copy`` which will call :meth:`__copy__`"""
        return copy.copy(self)

    def clone(self):
        """Internal alias for ``copy.deepcopy`` which will call :meth:`__deepcopy__`"""
        return copy.deepcopy(self)

    def __repr__(self):
        return "{}()".format(self.__class__.__name__)

    def __str__(self):
        return str(self.archive)

    # Container will change so make sure it cannot be used in hash table
    __hash__ = None

    def __iter__(self):
        """Iterate over archive members."""
        return iter(self.archive)

    def __len__(self):
        """Get the number of source inside the archive."""
        return len(self.archive)

    def __bool__(self):
        return len(self) != 0

    def __eq__(self, other):
        if isinstance(other, ArchiveInterface):
            return self.archive == other.archive
        else:
            return False

    def __contains__(self, source):
        return source in self.archive


class EpsilonArchive(ArchiveInterface):

    """Implementation of an :math:`\epsilon-Archive` which cut in hyper-cube
    the objective space. Only a solution is allowed per hyper-cube to avoid redundancy.
    Contains only :math:`\epsilon-nondominated` individuals based on
    :math:`\epsilon-dominance`. When a solution is added to the archive their hyper-cube
    is define using *epsilons* parameters (see :meth:`add`).


    :param epsilons: Sizes of epsilon boxes used to restrict archive size.
                     Each objective must have an epsilon values
                     (``len(epsilons) == len(objectives)``)
    :param sources: A sequence of source to be added into the archive (Optional)
    :param distance_based: Choose how to deals with source inside the same box.

                                - :data:`True` selection on same hyper-cube is done
                                  using euclidean distance to hyper-cube upper corner
                                  (default)
                                - :data:`False`: selecton on same hyper-cube is done
                                  using pareto-dominance rules.

    :param box_normalized: Choose how to compute the distance between solution inside
                           the same box. Have no effect if ``distance_based is False``

                                - :data:`True`  Distance relative to epsilon tolerance
                                  (default)
                                - :data:`False` Distance without normalization

    .. note::
        epsilons values should be chosen to consider similar solution to be the same
        :math:`objective(i) == objectives(i) +- \epsilon)`

    .. note::
        Maximization is assumed in the dominance sort to be coherent with the Fitness
        implementation which already assumes a maximization.

    .. warning::
        For two archive to be equals, they only need to have the same epsilons values
        and the same list of source.
        For example an archive using distance based approach for selection inside
        a same box (``distance_based is True``) can be equal to another using
        strict dominance(``distance_based is False``).
    """

    # Boxes states can only be one of this fourth state.
    STATE = Enum("State", ("dominate", "dominated", "equal", "nondominated"))

    def __init__(self, epsilons, sources=(), distance_based=True, box_normalized=True):
        super(EpsilonArchive, self).__init__()
        # Make sure _epsilons is immutable
        self._epsilons = tuple(epsilons)
        # Keep track of source to delete
        self._to_delete = list()
        # Which method used to select the box solution
        self._distance_based = distance_based
        # Map distance function according to box_normalized value
        self._box_normalized = box_normalized
        self._dist_func = self._distances_normalized if box_normalized is True else self._distances
        if sources:
            self.merge(sources)

    @staticmethod
    def remove_clusters_from(archive_original, epsilons):
        """Remove sources from *archive_original* with respect to new epsilons.
        Let *archive_original* untouched and return an archive with same
        epsilons values but less clustered.

        :param archive_original: Archive where they are too many cluster.
        :param epsilons: Values for temporary epsilons used to remove closest sources.

        :Return: Return a new :class:`EpsilonArchive` instance with less clusters.
        """
        # Create a temp archive with sources from archive_original
        temp_arch = EpsilonArchive(epsilons)
        temp_arch.merge(archive_original)
        # Create a new archive with same epsilons as archive_original argument
        # but with only remaining solutions of temp_arch
        new_archive = EpsilonArchive(archive_original.epsilons)
        new_archive.merge(temp_arch)
        return new_archive

    def remove_clusters(self, epsilons):
        """Remove clusters from archive using new values of *epsilons*. In order
        to actually removed clusters, *epsilons* must be higher than current one.

        :param epsilons: Values for temporary epsilons used to remove closest sources.
        """
        # Create a temp archive with sources from archive_original
        temp_arch = EpsilonArchive(epsilons)
        temp_arch.merge(self)
        # Keep only remaining solutions of temp_arch
        self.archive = temp_arch.archive

    def _get_epsilons(self):
        return self._epsilons

    epsilons = property(_get_epsilons, None, None,
                        ("**epsilons** behave like an read-only attribute."))

    def add(self, new_source):
        """ Add *new_source* to archive
        Assume *new_source* has at least a :class:`pyMayBee.Primitive.fitness.Fitness`
        attribute namely **fitness**.

        :param new_source: New source you want to add to the archive.

        .. warning::
            if *new_source* is mutable a copy should be passed to avoid change
            afterwards.
        """
        # First get new_source box value
        new_box = self._compute_box_from_objectives(new_source)
        # Iterate over all archive sources to check dominance
        for archive_source in self.archive:
            # Box dominance
            archive_box = self._compute_box_from_objectives(archive_source)
            state = self._compute_box_dominance(new_box, archive_box)
            if state == self.__class__.STATE.dominate:
                # If new source dominate, mark archive source for deletion
                self._to_delete.append(archive_source)
            elif state == self.__class__.STATE.dominated:
                # If new box is dominated stop iteration
                # Must had some logger here to say: "New source not added bro !"
                return
            elif state == self.__class__.STATE.equal:
                # If boxes are similar.
                if self._distance_based is True:
                    # Check which source has the lowest distance to the box upper corner
                    # `_dist_func` call `_distance` or `_distances_normalized`
                    # according to `_box_normalized` flag (see __init__)
                    new_distance, archive_distance = self._dist_func(new_source,
                                                                     archive_source,
                                                                     new_box)
                    new_source_better = new_distance < archive_distance
                else:
                    # Check strict dominance between sources
                    # Comparison between source is made using their fitness attribute
                    new_source_better = new_source.fitness > archive_source.fitness
                if new_source_better:
                        # New source closer, mark archive source for deletion
                        self._to_delete.append(archive_source)
                else:
                    # Archive source better, stop iteration
                    # Must had some logger here to say: "New source not added bro !"
                    return
        # No archive sources has dominated the new one, we can add it to the archive
        self.archive.append(new_source)
        # Delete marked sources
        self._delete_marked()

    def _distances(self, new_source, archive_source, box):
        """Compute distance squared (to avoid a :math:`sqrt`) for *new_source* and
        *archive_source* to *box* upper-right corner as maximization is assumed.
        Called in case of *archive_source* and *new_source* are in the same box.

        :param new_source: New source we want to add to the archive.
        :param archive_source: A source already inside the archive which shares
                               the same box value as the *new_source*.
        :param box: Box value shared by *new_source* and *archive_source*.

        :Return: Distance squared for *new_source* and for *archive_source* to
                 upper-right corner.

        .. note::
            *new_source* does not have to dominate *archive_source* to replace it,
            just to be closer to upper-right corner of the box than *archive_source*.

        .. seealso::
            :meth:`_distances_normalized`

        """
        # Compute absolute position of upper corner
        upper_box = tuple(obj * eps for (obj, eps) in zip(box, self.epsilons))
        # Compute euclidean distances using generator
        new_iterator = ((b_val - val) ** 2 for (b_val, val) in zip(upper_box, new_source.fitness.w_values))
        archive_iterator = ((b_val - val) ** 2 for (b_val, val) in zip(upper_box, archive_source.fitness.w_values))
        # Return squared distances
        return sum(new_iterator), sum(archive_iterator)

    def _distances_normalized(self, new_source, archive_source, box):
        """Compute distance squared (to avoid a :math:`sqrt`) using normalized
        objectives for *new_source* and *archive_source* to *box* upper-right corner
        as maximization is assumed.
        Called in case of *archive_source* and *new_source* are in the same box.

        :param new_source: New source we want to add to the archive.
        :param archive_source: A source already inside the archive which shares
                               the same box value as the *new_source*.
        :param box: Box value shared by *new_source* and *archive_source*.

        :Return: Distance squared for *new_source* and for *archive_source* to
                 upper-right corner.

        .. note::
            *new_source* does not have to dominate *archive_source* to replace it,
            just to be closer to upper-right corner of the box than *archive_source*
            after normalization.

        .. seealso::
            :meth:`_distances`
        """
        # Get minimum and maximum absolute value for the box (twice)
        boxes_it1 = zip(*(((obj - 1) * eps, obj * eps) for (obj, eps) in zip(box, self.epsilons)))
        boxes_it2 = zip(*(((obj - 1) * eps, obj * eps) for (obj, eps) in zip(box, self.epsilons)))
        # Compute euclidean distance on normalized objectives
        new_iterator = ((1 - (val - mini) / (maxi - mini)) ** 2 for (val, mini, maxi)
                        in zip(new_source.fitness.w_values, *boxes_it1))
        archive_iterator = ((1 - (val - mini) / (maxi - mini)) ** 2 for (val, mini, maxi)
                            in zip(archive_source.fitness.w_values, *boxes_it2))
        # Return squared distances
        return sum(new_iterator), sum(archive_iterator)

    def _delete_marked(self):
        """Marked sources for deletion will be deleted."""
        for source in self._to_delete:
            # Remove from archive
            self.archive.remove(source)
        # Remove markers
        self._to_delete = list()

    def _compute_box_from_objectives(self, source):
        """Compute *source* hyper-cube assuming maximization.

        :param source: Source of which we want the box value.
        :Return: Hyper-cube values
        """
        return tuple(ceil(w_value / epsilon)
                     for (w_value, epsilon) in zip(source.fitness.w_values, self.epsilons))

    def _compute_box_dominance(self, new_box, archive_box):
        """Compute a strict dominance on hyper-cube and return an enum value
        according to the dominance state.

        .. code-block:: python

        If archive_box > new_box:
            return STATE.dominated
        elif new_box > archive_box:
            return STATE.dominate
        elif archive_box == new_box:
            return STATE.equal
        else:
            return STATE.nondominated

        :param new_box: The new source hyper-cube value.
        :param archive_box: An archive source hyper-cube value.
        """
        # Assume no domination at first
        dominate = False
        dominated = False
        boxes_values_iter = zip(new_box, archive_box)
        for (new_box_val, archive_box_val) in boxes_values_iter:
            # Archive source has a better box objective value
            if archive_box_val > new_box_val:
                dominated = True
            # New source has a better box objective value
            elif archive_box_val < new_box_val:
                dominate = True
            # If both have at least a better box objective they are not-dominated
            if dominated and dominate:
                return self.__class__.STATE.nondominated
        # If archive source box dominates new one
        if dominated:
            return self.__class__.STATE.dominated
        # If new source box dominates the archive one
        elif dominate:
            return self.__class__.STATE.dominate
        # At this point they must be equals
        else:
            return self.__class__.STATE.equal

    def __repr__(self):
        return "{}({}, {})".format(self.__class__.__name__, self.epsilons, self.archive)

    def __str__(self):
        return str(self.archive)

    def __eq__(self, other):
        """Equality operator ``self == other``."""
        if isinstance(other, EpsilonArchive):
            return self._epsilons == other._epsilons and self.archive == other.archive
        else:
            return False

    def __ne__(self, other):
        """Inequality operator ``self != other``."""
        return not self.__eq__(other)

    def __add__(self, other):
        """Merge two archive together with respect to first one flags.
        *other* can be any iterable or a unique source.

        .. warning::
            :attr:`distance_based` are get from *self*
            New archive is a copy of *self* with *other* merge into it.
        """
        # A source object alone
        if hasattr(other, "fitness"):
            new_archive = self.copy()
            new_archive.add(other)
            return new_archive
        # An iterable
        elif hasattr(other, "__iter__"):
            # Clone first archive and merge other inside copy
            new_archive = self.copy()
            new_archive.merge(other)
            return new_archive
        raise TypeError("{} is not supported.".format(type(other)))

    def __radd__(self, other):
        """Merge two archive together with respect to first one flags.
        *other* can be any iterable.

        .. warning::
            :attr:`distance_based` and :attr:`normalized` are get from *self*
            New archive is a copy of *self* with *other* merge into.
        """
        return self.__add__(other)

    def __copy__(self):
        """Create swallow copy of the class."""
        cls_copy = type(self)(self._epsilons, (), self._distance_based, self._box_normalized)
        # Merge original sources into new one
        cls_copy.merge(self.archive)
        return cls_copy

    def __deepcopy__(self, memo):
        """Create deep copy of the class assuming ``self._epsilons`` is immutable."""
        cls_copy = type(self)(copy.deepcopy(self._epsilons, memo), (),
                              self._distance_based,
                              self._box_normalized)
        # Merge original sources into new one
        cls_copy.merge(self.archive)
        return cls_copy
