# -*- coding:Utf8 -*-

"""
    Module fitness.py
    Helper classes to handle multi-objective dominance evaluation for any combination
    of minimization of maximization. A fitness assume maximization and is mutable.

        - :class:`pyMayBee.Primitive.fitness.Fitness`: Compute dominance based
          on Pareto approach without any constraint handling method.
        - :class:`pyMayBee.Primitive.fitness.FitnessConstrained`: Shared
          Pareto dominance approach but also expose a constraint handling method
          based on **Woldesenbet et al.** to deal with multi-objective constrained problems.
        - :func:`pyMayBee.Primitive.fitness.fitness_factory`: A factory function which
          can be used to create any type of Fitness without any knowledge of
          OOP programming. Only a sugar helper that create under the hoods the class
          for you with correct setup.
"""

import sys
import math

from operator import truediv, mul
import copy

from ..exceptions import ClassMethodProtected


__all__ = ['Fitness', 'FitnessConstrained', 'fitness_factory']


# class DeltaPenalty(object):

#     """docstring for DeltaPenalty.
#     This class is must be used as a decorator for the Fitness class constructor.
#     Alter values with penalty and return modified values to class constructor."""

#     def __init__(self):
#         raise NotImplemented

#     def __call__(self):
#         pass


# class ClosestValidPenalty(object):

#     """docstring for ClosestValidPenalty.
#     This class is must be used as a decorator for the Fitness class constructor.
#     Alter values with penalty and return modified values to class constructor."""

#     def __init__(self):
#         raise NotImplemented

#     def __call__(self):
#         pass


class Fitness(object):

    """An abstract multi-objective fitness to handle multi-objective problems.
    There are multiple way to do create your Fitness class:

      .. code-block:: python

        # Child will **NOT** use __slots__
        class Child(Fitness):
            weights = (value1, value2, ...)

      .. code-block:: python

        # Child will inherite __slots__ and new attribute **CANNOT** be added
        class Child(Fitness):
            __slots__ = ()
            weights = (weigths values)

      .. code-block:: python

        # Child will inherite __slots__ and new attribute **CAN** be added
        class Child(Fitness):
            __slots__ = ("__dict__", )
            weights = (weigths values)

    Maximization and minimization are taken care off by a multiplication
    between the :attr:`weights` and the Fitness :attr:`values`.
    Maximization is assumed, so you have to use **negative** weights to be able to
    minimize an objective. If you want to compute a single objective optimization
    you still need to use a :class:`tuple` for :attr:`weights` but with one element only.

    .. note::
        Iteration over :class:`Fitness` iterate over :attr:`values` which are
        not aware of minimization or maximization.

    .. note::

        - When comparing fitness values ``a > b`` will return :data:`True`
          if *a* **dominate** *b* (even in the case of minimization).
        - Fitness may be compared using the ``>``, ``<``, ``>=``, ``<=``, ``==``,
          ``!=``. **Dominance** can then be test using ``>`` operator.
          The comparison of those operators is **NOT** made lexicographically.

    .. warning::
        :attr:`weights` must be set before any creation of an instance.
        .. py:classmethod::`set_weights` can be used as an helper.
        Setting :attr:`weights` from an instance is **not allowed** to avoid side effect.
    """

    __slots__ = ("_w_values", )

    # Weights used to define if we want to minimize or maximize the objectives
    weights = ()

    def __init__(self, values=()):
        if not self.__class__.weights:
            raise TypeError("Can't instantiate {} without a shared "
                            "attribute weights. "
                            "Please first inherite from {} and "
                            "set some weights.".format(self.__class__,
                                                       self.__class__.__name__))

        elif len(values) == len(self.__class__.weights):
            self._w_values = tuple(map(mul, values, self.__class__.weights))
        # Raise an error only if default value overwritten
        elif values:
            raise ValueError("Can’t instantiate {0}. Please use `values` of "
                             "the same size as {1}.weights. "
                             "Currently `values` has a length of {2} and "
                             "{1}.weights of {3}".format(self.__class__,
                                                         self.__class__.__name__,
                                                         len(values),
                                                         len(self.__class__.weights)))
        # Default to empty tuple
        else:
            self._w_values = ()

    @classmethod
    def get_weights(cls):
        return cls.weights

    @ClassMethodProtected
    def set_weights(cls, weights):
        """Assign new *weights* as .. py:classmethod::`Weights` after some sanity checks."""
        if not hasattr(weights, "__iter__"):
            raise TypeError("Attribute weights of {} must be an "
                            "iterable.".format(cls.__name__))
        elif 0 in weights:
            raise ValueError("Can only set weight to non zero values")
        else:
            cls.weights = tuple(weights)

    @property
    def is_define(self):
        return self.__bool__()

    def _get_values(self):
        """Getter for values without weights."""
        return tuple(map(truediv, self._w_values, self.__class__.weights))

    def _set_values(self, values):
        """Setter used to set new values."""
        try:
            if not (len(values) == len(self.__class__.weights)):
                raise ValueError
            self._w_values = tuple(map(mul, values, self.__class__.weights))
        except TypeError:
            _, _, traceback = sys.exc_info()
            raise TypeError("Values must be an iterable of the same size as weights. "
                            "Currently trying to assign value(s) {0}, {1} with "
                            "weights {2}.".format(values, type(values), self.__class__.weights),
                            traceback)
        except ValueError:
            _, _, traceback = sys.exc_info()
            raise ValueError("Values must be an iterable of the same size as weights. "
                             "Currently trying to assign value(s) <{0}, length = {2}> with "
                             "weights <{1}, length = {3}>.".format(values, self.__class__.weights,
                                                                   len(values),
                                                                   len(self.__class__.weights)),
                             traceback)

    def _del_values(self):
        self._w_values = ()

    # Create a descriptor to control the way self.values is handle.
    values = property(_get_values, _set_values, _del_values,
                      ("**values** behave like an attribute."))

    def _get_w_values(self):
        """Return weighted values. Do not try to update directly :attr:`w_values`
        attribute used :attr:`values` instead."""
        return self._w_values

    def _set_w_values(self, values):
        raise AttributeError("<{}.w_values> is a read only "
                             "attribute.".format(self.__class__.__name__))

    def _del_w_values(self):
        raise AttributeError("<{}.w_values> is a read only "
                             "attribute.".format(self.__class__.__name__))

    # Create a descriptor to control the way self._w_values is handle.
    w_values = property(_get_w_values, _set_w_values, _del_w_values,
                        ("**w_values** is a read only attribute."))

    def dominate(self, other, objs=slice(None)):
        """Return True if each objective of *self* is not strictly worse than
        the corresponding objective of *other* and at least one objective of *self*
        is strictly better.
        Be aware that :func:`dominate` is not the opposite of :func:`is_dominated`
        .. seealso:: :meth:`is_dominated`

        :param objs: Slice indicating on which objectives the domination is
                    tested. The default value is `slice(None)`, representing
                    every objectives.
        """
        dominate = False
        for (self_w_values, other_w_values) in zip(self.w_values[objs], other.w_values[objs]):
            if self_w_values < other_w_values:
                return False
            elif self_w_values > other_w_values:
                dominate = True
        return dominate

    def is_dominated(self, other, objs=slice(None)):
        """Return True if each objective of *other* is not strictly worse than
        the corresponding objective of *self* and at least one objective of *other*
        is strictly better.

        :param objs: Slice indicating on which objectives the domination is
                    tested. The default value is `slice(None)`, representing
                    every objectives.

        .. note::
            :func:`is_dominated` is not the opposite of :func:`dominate`
            .. seealso:: :meth:`dominate`, :meth:`is_not_dominated`

        """
        is_dominated = False
        for (self_w_values, other_w_values) in zip(self.w_values[objs], other.w_values[objs]):
            if other_w_values < self_w_values:
                return False
            elif other_w_values > self_w_values:
                is_dominated = True
        return is_dominated

    def is_not_dominated(self, other, objs=slice(None)):
        """Return True if *other* does not dominate *self*.
        .. seealso:: :meth:`dominate`

        :param objs: Slice indicating on which objectives the domination is
                    tested. The default value is `slice(None)`, representing
                    every objectives."""
        return not (other > self)

    def is_feasible(self):
        """If no constraints, solution feasible."""
        return True

    def copy(self):
        """Internal alias for ``copy.deepcopy`` which will call :meth:`__deepcopy__`.
        That is, copy and clone are alias for the ``copy.deepcopy``.

        .. seealso:: :meth:`clone`
        """
        return copy.deepcopy(self)

    def clone(self):
        """Internal alias for ``copy.deepcopy`` which will call :meth:`__deepcopy__`
        That is, copy and clone are alias for the ``copy.deepcopy``.

        .. seealso:: :meth:`clone`
        """
        return copy.deepcopy(self)

    def __hash__(self):
        return hash(self.w_values)

    def __repr__(self):
        """String representation which can be used to recreate the same object."""
        return "{}({})".format(self.__class__.__name__,
                               self.values if self.is_define else tuple())

    def __str__(self):
        """Return the values of the Fitness object."""
        values = (self.values if self.is_define else tuple())
        w_values = (self.w_values if self.is_define else tuple())
        return str("{2}({0}, {1})".format(values, w_values, self.__class__.__name__))

    def __len__(self):
        return len(self._w_values)

    def __bool__(self):
        """Define behavior when ``bool()`` call on the instance."""
        return len(self) != 0

    def __iter__(self):
        """Iterate over :attr:`values` (not weighted values)."""
        return iter(self.values)

    def __eq__(self, other):
        """Equality operator ``self == other``."""
        if isinstance(other, Fitness):
            return self.w_values == other.w_values
        else:
            return self.w_values == other

    def __ne__(self, other):
        """Inequality operator ``self != other``."""
        if isinstance(other, Fitness):
            return self.w_values != other.w_values
        else:
            return self.w_values != other

    def __lt__(self, other):
        """Less than operator ``self < other``."""
        if isinstance(other, Fitness):
            return self.is_dominated(other)
        else:
            return self.w_values < other

    def __gt__(self, other):
        """Greater than operator ``self > other``."""
        if isinstance(other, Fitness):
            return self.dominate(other)
        else:
            return self.w_values > other

    def __le__(self, other):
        """Less than or equal operator ``self <= other``."""
        if isinstance(other, Fitness):
            return self == other or self.is_dominated(other)
        else:
            return self.w_values <= other

    def __ge__(self, other):
        """Greater than or equal operator ``self >= other``."""
        if isinstance(other, Fitness):
            return self == other or self.dominate(other)
        else:
            return self.w_values >= other

    def __deepcopy__(self, memo):
        """Create deep copy of the class assuming ``self._w_values`` is immutable."""
        cls_copy = type(self)()
        # Assume self._w_values is immutable
        cls_copy._w_values = self._w_values
        return cls_copy


class FitnessConstrained(Fitness):

    """Implementation of a Fitness with constrained handling.
    Provide the same API of :class:`Fitness` but add an interface to get
    objectives values after constraint handling. Constraints handling
    support is defined based on **Woldesenbet et al., Constraint handling in
    multi-objective evolutionary optimization** and required fitness and constraint
    to be normalized first. This approach also needed to know the population
    feasibility rate to adapt the constraint impact.
    That is, the method interactively adapt the constraint importance to avoid too
    many non-doable source in the population.
    In order to make normalization possible .. py:classmethod::`set_bounds_for_fitness`
    must be called when population change to update:

        - :attr:`fitness_max` as the max value for each objective
        - :attr:`fitness_min` as the min value for each objective
        - :attr:`constraints_max` as the max value for each constraint
        - :attr:`feasibility_rate` the feasibility rate for the population

    :attr:`constraints_max` is initialize at (0, ) to be iterable and consistent with
    :attr:`constraints`. Important to note that 0 is the minimum value for a constraint
    because constraints are always positive. Zeros means no constraints.

    :param values: Value for each objective as a :data:`tuple`
    :param constraints: Value for each constraints as a :data:`tuple`

    .. note::
        Why a different interface for using objectives with constraints ?
        In some implementation we must keep a track of Fitness without constraint
        handling. Moreover both interfaces make clear which Fitness will be get.

    .. note::
        I want to be more restrictive on the constraint support, it is possible ?
        You can rise to the squared input constraints which will lead to increase
        constraint negative weight.

    .. note::
        If you are looking for a penalty constrained fitness like **DeltaPenalty**
        or **ClosestValidPenalty** you aren't in the right place.
        This kind of penalty can be easily implemented using decorator with the base
        Fitness Class.
        decorator(func_feasible, delta_value, distance) with **func_feasible**
        a function which return :data:`True` if solution admissible. **distance**
        a function which return a distance between solution and a valid one.

    .. warning::
        Class attribute *fitness_max*, *fitness_min* and *constraints_max*,
        and *feasibility_rate* must be defined before accessing norm values
        to get consistent results.
        Theses values can be set using .. py:classmethod::`set_bounds_for_fitness`

    .. warning::
        This constraint approach used relative min and max for each objective and
        each constraint. Then the normalized fitness can change during the
        optimization process. In order to avoid side effect,
        normalized values are computed at each call to norm_values.
        Feel free to store these values if needed, but be aware that any new solution
        can lead to different normalized values.
    """

    # Static attributes shared between all FitnessConstrained instances
    fitness_max = ()         # Maximum value for each objective
    fitness_min = ()         # Minimal value for each objective
    constraints_max = (0, )  # Maximal value for each constraint
    feasibility_rate = 1     # Rate of feasible solution (nb_feasible / nb_total)

    # Update slots allowed attributes
    __slots__ = ("constraints")

    def __init__(self, values=(), constraints=(0, )):
        super(FitnessConstrained, self).__init__(values)
        # Store constraints only, normalization will be compute on the fly
        self.constraints = constraints

    @classmethod
    def set_bounds_for_fitness(cls, low_bounds, up_bounds, constraints_up_bounds):
        """Set new values for min and max for each fitness value.
        Define best and worse values to be able to normalized objectives and constraints.

        :param low_bounds: Minimal value for each objective
        :param up_bounds: Maximal value for each objective
        :param constraints_up_bounds: Maximal value for each constraint
        """
        # low and up bounds must be of same length
        assert len(low_bounds) == len(up_bounds), "low and up bounds must be of same length "
        # Must have a bound for each fitness value
        assert len(low_bounds) == len(cls.weights), "Each fitness value must have bounds"
        # Each low bounds must be lower than each up_bounds
        assert all(min_b <= max_b for (min_b, max_b) in zip(low_bounds, up_bounds))
        cls.fitness_min = low_bounds
        cls.fitness_max = up_bounds
        # Can have a bound for a constraint lower than 0
        cls.constraints_max = tuple(max(bound, 0) for bound in constraints_up_bounds)

    @classmethod
    def set_feasibility_rate(cls, new_rate):
        if new_rate <= 1 and new_rate >= 0:
            cls.feasibility_rate = new_rate

    def is_feasible(self):
        """Solution is feasible is all constraints have 0 has value"""
        return sum(el for el in self.constraints) == 0

    @property
    def norm_values(self):
        """Return new fitness values after taking account for constraints."""
        values = self._get_normalized_fitness_values()
        return tuple(values)

    def is_norm_dominated(self, other, objs=slice(None)):
        """Test if *self* is dominated by an *other* constrainedFitness using **normalized**
        weights values.
        If you only want to check non-dominance against fitness .. seealso: :meth:`is_dominated`

        :param objs: Slice indicating on which objectives the domination is
                    tested. The default value is `slice(None)`, representing
                    every objectives.
        """
        is_dominated = False
        for (self_w_values, other_w_values) in zip(self.norm_values[objs],
                                                   other.norm_values[objs]):
            # norm_values assume minimization
            if other_w_values > self_w_values:
                return False
            elif other_w_values < self_w_values:
                is_dominated = True
        return is_dominated

    def norm_dominate(self, other, objs=slice(None)):
        """Test if *self* dominate an *other* constrainedFitness using **normalized**
        weights values.
        If you only want to check dominance against fitness .. seealso: :meth:`dominate`

        :param objs: Slice indicating on which objectives the domination is
                    tested. The default value is `slice(None)`, representing
                    every objectives.
        """
        dominate = False
        for (self_w_values, other_w_values) in zip(self.norm_values[objs],
                                                   other.norm_values[objs]):
            # norm_values assume minimization
            if self_w_values > other_w_values:
                return False
            elif self_w_values < other_w_values:
                dominate = True
        return dominate

    def _do_normalized_fitness(self):
        """Return a tuple of normalized fitness using values without weights balance."""
        klass = type(self)
        zip_object = zip(self.values, klass.fitness_max, klass.fitness_min, self.weights)
        # Normalized fitness as tuple
        temp_list = []
        for (value, fit_max, fit_min, weight) in zip_object:
            # Corner case when max == min
            if fit_max == fit_min:
                temp_list.append(0)
            else:
                if weight > 0:
                    # Lower norm fitness is better even for maximization
                    temp_list.append(1 - (value - fit_min) / (fit_max - fit_min))
                else:
                    temp_list.append((value - fit_min) / (fit_max - fit_min))
        return tuple(temp_list)

    def _do_distances(self, norm_fitness, norm_constraint):
        """Compute the distance according to fitness, constraints and feasibility_rate.
        Distance calculation differs from paper implementation to works when
        maximization is assumed.
        """
        klass = type(self)
        if klass.feasibility_rate != 0:
            generator = (norm_constraint**2 + fit**2 for fit in norm_fitness)
            distances = (dist for dist in map(math.sqrt, generator))
        else:
            distances = (norm_constraint for _ in norm_fitness)
        # distances generator
        return distances

    def _do_penalities(self, norm_fitness, norm_constraint):
        """Penalties computed as in the paper except for Y where smaller fitness
        values must be more penalized because maximization is assumed.
        """
        klass = type(self)
        # Get X penalty
        if klass.feasibility_rate != 0:
            X = norm_constraint
        else:
            X = 0
        # Get Y penalty
        if not self.is_feasible():
            Y = (fit for fit in norm_fitness)
        else:
            # Solution feasible
            Y = (0 for fit in norm_fitness)
        # penalties generator
        return ((1 - klass.feasibility_rate) * X + klass.feasibility_rate * y for y in Y)

    def _do_normalized_constraint(self):
        """Compute each normalized constraint and return mean."""
        klass = type(self)
        zip_object = zip(self.constraints, klass.constraints_max)
        # Normalized mean constraints as float
        norm_sum = 0
        for (const, const_max) in zip_object:
            # If const_max is 0
            try:
                norm_sum += const / const_max
            except ZeroDivisionError:
                continue
        return norm_sum / len(self.constraints)

    def _get_normalized_fitness_values(self):
        """Return a normalized fitness values using constraints as penalties.
        Take care of minimization and maximization issues (penalties signs and
        squared values).
        """
        # Distances and penalties are generator and can only be used once.
        norm_fitness = self._do_normalized_fitness()
        norm_constraint = self._do_normalized_constraint()
        distances = self._do_distances(norm_fitness, norm_constraint)
        penalties = self._do_penalities(norm_fitness, norm_constraint)
        return (distance + penalty for (distance, penalty) in zip(distances, penalties))

    def __repr__(self):
        """String representation which can be used to recreate the same object."""
        return "{}({}, {})".format(self.__class__.__name__,
                                   self.values if self.is_define else tuple(),
                                   self.constraints)

    def __str__(self):
        """Return the values of the Fitness object."""
        values = (self.values if self.is_define else tuple())
        w_values = (self.w_values if self.is_define else tuple())
        return str("{2}({0}, {1}, {3})".format(values, w_values, self.__class__.__name__,
                                               self.norm_values))

    def __eq__(self, other):
        """Equality operator ``self == other``."""
        if isinstance(other, FitnessConstrained):
            return self.w_values == other.w_values and self.constraints == other.constraints
        elif isinstance(other, Fitness):
            # if instance do not have constraint attribute
            return self.w_values == other.w_values
        else:
            return self.w_values == other

    def __ne__(self, other):
        """Inequality operator ``self != other``."""
        return not self.__eq__(other)

    def __deepcopy__(self, memo):
        """Create deep copy of the class assuming ``self._w_values`` is immutable."""
        cls_copy = type(self)()
        # Assume self._w_values is immutable
        cls_copy._w_values = self._w_values
        # Assume self.constraints is immutable
        cls_copy.constraints = self.constraints
        return cls_copy


def fitness_factory(weights, fitness_class=Fitness, name="MyFitness"):
    """Returns a *fitness_class* class which can be used to instantiate new Fitness
    which can then be used as ``name(value)`` like any other classes.
    It’s similar to:

        .. code-block:: python

            class name(fitness_class):

                '''My new fitness with specific weights'''

                __slots__ = ()
                weights = tuple(weights for weight in weights)

    """
    return type(name, (fitness_class, ), {"weights": weights, "__slots__": ()})
