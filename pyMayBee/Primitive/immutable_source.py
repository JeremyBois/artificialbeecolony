# -*- coding:Utf8 -*-

"""
    Module immutable_source.py
    Immutable representations of a :class:`pyMayBee.Entity.sources.FoodSource` to avoid side effect of mutable object.

        - :class:`pyMayBee.Primitive.immutable_source.ImmutableSource`: Namedtuple used to create a
          limited immutable version of a :class:`pyMayBee.Entity.sources.FoodSource`.
"""

from collections import namedtuple


__all__ = ['ImmutableSource']

# Must be define outside classes to enable pickling
ImmutableSource = namedtuple('ImmutableSource', ['fitness', 'position', 'trial'])
