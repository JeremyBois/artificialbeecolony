# -*- coding:Utf8 -*-

"""
    Module criterion.py:
    Helper classes to handle different kind of criteria using abstraction. A criterion
    is an immutable object and each modification return another instance.

        - :class:`pyMayBee.Primitive.criterion.AbstractCriterion` : Abstract interface
          that each Criterion need to implement.
        - :class:`pyMayBee.Primitive.criterion.DiscreteCriterion` : A Criterion
          that assumes a finite discrete number of quantitative values.
        - :class:`pyMayBee.Primitive.criterion.ContinuousCriterion` : A Criterion
          that assumes a range of variation between two boundaries.
        - :class:`pyMayBee.Primitive.criterion.QualitativeCriterion` : A Criterion
          that assumes a finite discrete number of qualitative values.
"""

import pyMayBee.Misc.random_helper as rh

import abc
import copy
import pandas as pd

from collections import Mapping, defaultdict, Sequence

from ..Misc.tools import closest


__all__ = ["DiscreteCriterion", "QualitativeCriterion", "ContinuousCriterion"]


class ReadOnlyDict(Mapping):

    """
        A readonly dict implementation.

        :param dict_instance: A dict like instance you want to be read only.
    """

    def __init__(self, dict_instance):
        self._dict = dict_instance

    def __getitem__(self, key):
        return self._dict[key]

    def __len__(self):
        return len(self._dict)

    def __iter__(self):
        return iter(self._dict)

    def __eq__(self, other):
        return self._dict == other

    def __ne__(self, other):
        return self._dict != other

    def __repr__(self):
        return repr(self._dict)

    def __str__(self):
        return str(self._dict)

    def __hash__(self):
        return hash(tuple(self._dict.items()))


class AbstractCriterion(metaclass=abc.ABCMeta):

    """Abstract Criterion class which implement interface shared by all Criterion
    implementation.
    """

    __slots__ = ("name", "_value", "definition")

    def __init__(self, name):
        self.name = name
        self._value = None
        # Must be overwritten
        self.definition = ReadOnlyDict(dict())

    def _get_value(self):
        pass

    value = abc.abstractproperty(_get_value, None, None,
                                 ("*value* behave like a read only attribute."))

    def update(self, *args, **kwargs):
        return self.__call__(*args, **kwargs)

    def clone(self):
        """Internal alias for ``copy.deepcopy`` which will call :meth:`__deepcopy__`"""
        return copy.deepcopy(self)

    def __str__(self):
        return self.__repr__()

    def __bool__(self):
        return self.value is not None

    def __eq__(self, other):
        """Equality operator ``self == other``."""
        if isinstance(other, AbstractCriterion):
            return self.value == other.value and self.definition == other.definition
        else:
            return False

    def __ne__(self, other):
        """Inequality operator ``self != other``."""
        return not self.__eq__(other)

    def __hash__(self):
        return hash((self.definition, self._value))

    @abc.abstractmethod
    def is_quantitative(self):
        pass

    @staticmethod
    def to_df(list_of_criteria_dict):
        """Create a :class:`pd.Dataframe` from a list of multiple Criteria
        where each column is a Criterion name and each row correspond to
        a list of Criteria.

        :param list_of_criteria_dict: A list of Criteria dict
                                      Each list of Criteria represent a position
        """
        # Create an empty container with default to empty list
        dico = defaultdict(list)
        # For each position ...
        for criteria in list_of_criteria_dict:
            # ... add criterion value to corresponding criterion name
            for (name, criterion) in criteria.items():
                dico[name].append(criterion.value)
        return pd.DataFrame(dico)

    @staticmethod
    def to_dict(criteria):
        """Create a :class:`dict` representation from *criteria*.
        Value for each criterion is mapped to its name.

        :param criteria: A :class:`dict` or :class:`Sequence` which contains
                         multiple criteria definition.
        """
        if isinstance(criteria, dict):
            return {criterion.name: criterion.value for criterion in criteria.values()}
        elif isinstance(criteria, Sequence):
            return {criterion.name: criterion.value for criterion in criteria}
        else:
            return {criteria.name: criteria.value}


class DiscreteCriterion(AbstractCriterion):

    """Discrete criterion handler.

    **Criterion** is an immutable object, so updating its value return
    another instance with respect to the former constraints.
    A bisect selection is used to select the closest value inside the *steps*.
    This abstraction allows to manage Discrete and Continuous Criterion the same
    way and insure that new value is correct.

    :param name: A name used to describe the criterion.
    :param steps: Allowed values for the criterion in any order.
    :param value: Value of the criterion. If :data:`None` randomly select a value
                  with respect to allowed values (*steps*).


    .. note::
        Duplicated step in *steps* will be removed and *steps* is sorted
        from min to max under the hoods.

    .. seealso::
        :class:`ContinuousCriterion` for an implementation of continuous criterion.

        :class:`QualitativeCriterion` for an implementation of qualitative criterion.
    """

    __slots__ = ()

    def __init__(self, name, steps, value=None):
        super(DiscreteCriterion, self).__init__(name)
        # Remove duplicated values and sort steps
        steps = tuple(sorted(set(steps)))
        if not steps:
            raise ValueError("`steps` cannot be empty")
        # Find lower, upper boundaries and steps
        internal_dict = {"low_bound": steps[0], "up_bound": steps[-1], "steps": steps}
        self.definition = ReadOnlyDict(internal_dict)

        # Handle default init value
        if value is None:
            value = rh.RandomGenerator.choice(steps)

        # Make sure we are in boundaries
        value = self._restrict_to_boundaries(value)
        # Find the discrete closest value
        value = self._find_nearest_value(value)
        self._value = value

    def _get_value(self):
        return self._value

    # Make value behave like a read only attribute
    value = property(_get_value, None, None,
                     ("**value** behave like a read only attribute."))

    @property
    def steps(self):
        return self.definition["steps"]

    @property
    def up_bound(self):
        return self.definition["up_bound"]

    @property
    def low_bound(self):
        return self.definition["low_bound"]

    def is_quantitative(self):
        return True

    def _restrict_to_boundaries(self, new_value):
        """Check if *new_value* is in the correct range define by :attr:`definition.up_bound`
        and :attr:`definition["low_bound"]` attributes. Bounds are defined
        according to :attr:`steps`.

        .. code-block:: python

            if new_value > self.definition["up_bound"]:
                return self.definition["up_bound"]
            elif new_value < self.definition["low_bound"]:
                return self.definition["low_bound"]
            else:
                return new_value

        """
        if new_value > self.definition["up_bound"]:
            return self.definition["up_bound"]
        elif new_value < self.definition["low_bound"]:
            return self.definition["low_bound"]
        else:
            return new_value

    def _find_nearest_value(self, new_value):
        """Return: Closest value inside :attr:`definition["steps"`]."""
        return closest(self.definition["steps"], new_value)

    def __repr__(self):
        text = "{}(name='{}', value={}, steps={})"
        return text.format(self.__class__.__name__, self.name, self.value,
                           self.definition["steps"])

    def __str__(self):
        text = "{}(name={}, value={}, low_bound={}, up_bound={}, steps={})"
        return text.format(self.__class__.__name__, self.name, self.value,
                           self.definition["low_bound"], self.definition["up_bound"],
                           self.definition["steps"])

    def __call__(self, new_value=None, name=None, steps=None):
        """Create a new :class:`DiscreteCriterion` instance with new value.

        :param new_value: New value of Criterion
        :param name: If not :data:`None` overrides Criterion name
        :param steps: If not :data:`None` ovverides Criterion allowed values

        .. note::
            If *new_value* is :data:`None` a random value is selected in *steps*
        """
        # Create new instance with default to older one
        new_criterion = type(self)(self.name if name is None else name,
                                   self.definition["steps"] if steps is None else steps,
                                   new_value)
        return new_criterion

    def __deepcopy__(self, memo):
        """Create deep copy of the class assuming ``self.definition`` is immutable."""
        cls_copy = type(self)(self.name, self.definition["steps"], self.value)
        return cls_copy


class ContinuousCriterion(AbstractCriterion):

    """Continuous criterion handler.

    **Criterion** is an immutable object, so updating its value return
    another instance with respect to the former constraints.
    *low_bound* and *up_bound* are used to define the allowed range for the
    criterion *value*.

    :param name: A name used to describe the criterion.
    :param value: Value of the criterion. If :data:`None` randomly select a value
                  with respect to bounds
    :param low_bound: Lower bound of allowed values (``low_bound < upper_bound``)
    :param up_bound: Upper bound of allowed values (``low_bound < upper_bound``)

    .. seealso::
        :class:`DiscreteCriterion` for an implementation of discrete criterion.

        :class:`QualitativeCriterion` for an implementation of qualitative criterion.
    """

    __slots__ = ()

    def __init__(self, name, value=None, low_bound=0, up_bound=1):
        super(ContinuousCriterion, self).__init__(name)
        # Check boundaries
        if low_bound > up_bound:
            raise ValueError("Lower bound (`low_bound`) must be lower than Upper bound (`up_bound`)")
        self.definition = ReadOnlyDict({"low_bound": low_bound, "up_bound": up_bound})

        # Default value
        if value is None:
            value = rh.RandomGenerator.uniform(low_bound, up_bound)
        # Make sure we are in boundaries (discrete and continuous)
        value = self._restrict_to_boundaries(value)
        self._value = value

    def _get_value(self):
        return self._value

    # Make value behave like a read only attribute
    value = property(_get_value, None, None,
                     ("**value** behave like a read only attribute."))

    @property
    def up_bound(self):
        return self.definition["up_bound"]

    @property
    def low_bound(self):
        return self.definition["low_bound"]

    def is_quantitative(self):
        return True

    def _restrict_to_boundaries(self, new_value):
        """Check if *new_value* is in the correct range define by :attr:`definition.up_bound`
        and :attr:`definition["low_bound"]` attributes.

        .. code-block:: python

            if new_value > self.definition["up_bound"]:
                return self.definition["up_bound"]
            elif new_value < self.definition["low_bound"]:
                return self.definition["low_bound"]
            else:
                return new_value
        """
        if new_value > self.definition["up_bound"]:
            return self.definition["up_bound"]
        elif new_value < self.definition["low_bound"]:
            return self.definition["low_bound"]
        else:
            return new_value

    def __repr__(self):
        text = "{}(name='{}', value={}, low_bound={}, up_bound={})"
        return text.format(self.__class__.__name__, self.name, self.value,
                           self.definition["low_bound"], self.definition["up_bound"])

    def __str__(self):
        text = "{}(name={}, value={}, low_bound={}, up_bound={})"
        return text.format(self.__class__.__name__, self.name, self.value,
                           self.definition["low_bound"], self.definition["up_bound"])

    def __call__(self, new_value=None, name=None, low_bound=None, up_bound=None):
        """Create a new criterion instance with new value."""
        # Create new instance with default to older one
        new_criterion = type(self)(self.name if name is None else name,
                                   new_value,
                                   self.definition["low_bound"] if low_bound is None else low_bound,
                                   self.definition["up_bound"] if up_bound is None else up_bound)
        return new_criterion

    def __deepcopy__(self, memo):
        """Create deep copy of the class assuming *self* is immutable."""
        cls_copy = type(self)(self.name, self.value,
                              self.definition["low_bound"],
                              self.definition["up_bound"])
        return cls_copy


class QualitativeCriterion(AbstractCriterion):

    """Qualitative criterion handler.

    **Criterion** is an immutable object, so updating its value return
    another instance with respect to the former constraints.
    A qualitative criterion used is more restrictive than :class:`DiscreteCriterion`
    because their is no order inside steps that can be used to learn from.

    :param name: A name used to describe the criterion.
    :param value: Value of the criterion.
    :param steps: Allowed values for criterion value attribute

    .. note::
        Duplicated step in *steps* will be removed and *steps* is sorted
        lexicographically as qualitative value cannot be sorted by importance.

    .. seealso::
        :class:`ContinuousCriterion` for an implementation of continuous criterion.

        :class:`DiscreteCriterion` for an implementation of discrete criterion.
    """

    __slots__ = ()

    def __init__(self, name, steps, value=None):
        super(QualitativeCriterion, self).__init__(name)
        # Remove duplicated values and sort steps
        steps = tuple(sorted(set(steps)))
        if not steps:
            raise ValueError("`steps` cannot be empty")
        self.definition = ReadOnlyDict({"steps": steps})

        # Random steps as init if value is not defined
        if value is None:
            value = rh.RandomGenerator.choice(steps)

        # Check data consistence.
        if self._exist_in_steps(value):
            self._value = value
        else:
            raise ValueError("New value must be in {}".format(self.definition["steps"]))

    def _get_value(self):
        return self._value

    # Make value behave like a read only attribute
    value = property(_get_value, None, None,
                     ("**value** behave like a read only attribute."))

    @property
    def steps(self):
        return self.definition["steps"]

    def is_quantitative(self):
        return False

    def _exist_in_steps(self, new_value):
        """:Return: :data:`True` if *new_value* exists in :attr:`steps`."""
        return new_value in self.definition["steps"]

    def __repr__(self):
        text = "{}(name='{}', value='{}', steps={})"
        return text.format(self.__class__.__name__, self.name, self.value,
                           self.definition["steps"])

    def __str__(self):
        text = "{}(name={}, value={}, steps={})"
        return text.format(self.__class__.__name__, self.name, self.value,
                           self.definition["steps"])

    def __call__(self, new_value=None, name=None, steps=None):
        """Create a new criterion instance with new value."""
        # Create new instance with default to older one
        new_criterion = type(self)(self.name if name is None else name,
                                   self.definition["steps"] if steps is None else steps,
                                   new_value)
        return new_criterion

    def __deepcopy__(self, memo):
        """Create deep copy of the class assuming ``self.definition`` is immutable."""
        cls_copy = type(self)(self.name, self.definition["steps"], self.value)
        return cls_copy
