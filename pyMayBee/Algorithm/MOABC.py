# -*- coding:Utf8 -*-

"""
    Algorithm.abc.py
"""

import pyMayBee.Misc.random_helper as rh


import abc
import pandas as pd
import itertools
import logging

from enum import Enum
from collections import Sequence


from ..Primitive import (Fitness, ContinuousCriterion, DiscreteCriterion,
                         QualitativeCriterion)
from ..Primitive.archive import ArchiveInterface

from ..Entity import (FoodSource, EmployedSpyBee, EmployedLevySpyBee,
                      OnlookerBee, OBLBee, ScoutBee)

from ..exceptions import AbcError

__all__ = ['ABCHive']


class _BasedFitness(Fitness):
    """A fitness class which assume maximization for each objective."""
    __slots__ = ()
    weights = (1, 1, 1)


class HiveAbstractInterface(metaclass=abc.ABCMeta):
    """An abstract interface which can be used to implement a MOABC algorithm.

    .. seealso::
        :class:`ABCHive` for a ready to use implementation.
    """

    PHASE = Enum('Phase', ('IDLE', 'Initialization', 'Employed', 'Onlookers'))

    @abc.abstractmethod
    def __init__(self):
        """Must be implemented by child class which want to implement an ABC algorithm.
        Describe needed attributes and their types.
        """
        self.logging = logging.getLogger(__name__ + '.HiveAbstractInterface')
        self.configuration = {}
        self._fitness_type = Fitness
        self.sources = []
        self.nb_sources = len(self.sources)
        self._trial_limit = 0
        self.archive = ArchiveInterface()
        self._current_phase = self.__class__.PHASE.IDLE

    @property
    def max_trial(self):
        """:Return: Maximum number of variation allowed per source."""
        return self._trial_limit

    @property
    def max_iteration(self):
        """:Return: Maximum number of iteration before optimization ends."""
        return self._max_iteration

    @property
    def max_evaluation(self):
        """:Return: Maximum number of evaluation before optimization ends."""
        return self._max_evaluation

    @property
    def current_phase(self):
        """:Return: String representation of current phase."""
        return str(self._current_phase).split('.')[-1]

    @abc.abstractmethod
    def init_phase(self, **kwargs):
        """Abstract implementation of initialization phase.
        Must be overwritten by child class.
        """
        pass

    @abc.abstractmethod
    def employed_phase(self, **kwargs):
        """Abstract implementation of exploration phase.
        Must be overwritten by child class.
        """
        pass

    @abc.abstractmethod
    def onlookers_phase(self, **kwargs):
        """Abstract implementation of exploitation phase.
        Must be overwritten by child class.
        """
        pass

    @abc.abstractmethod
    def evaluate(self, positions):
        """Abstract implementation of the problem evaluation.
        Must be overwritten by child class

        :param positions: A :class:`pd.Dataframe` where columns names are criterion names
                          and each row represent a position which must be evaluated.

        :Return: A 2-tuple which follows this template for **n** objective and **m** constraints:
            ((fitness_1, ... fitness_n), (constraints_1, ..., constraint_m)) where:

            - list of Fitness values for **n** objectives (order matter).
            - list of constraint values for **m** constraints (order matter)

        .. note::
            constraint must be list(:Data:`None`, ..., :Data:`None`) in case of a problem
            without constraints.
        """
        return (list(), list())

    def iteration_operations(self):
        """Do nothing by default.
        Can be overwritten to add make specific operations at each iteration.
        """
        pass

    def _update_bounds(self):
        """Update bounds (min, max) for each objectives and max for each constraint.
        Also compute the feasibility rate as sources which respects constraints divide
        by total number of sources.
        """
        # Initialize max and min from first source
        max_fitness = {index: value for (index, value) in enumerate(self.sources[0].fitness.values)}
        min_fitness = {index: value for (index, value) in enumerate(self.sources[0].fitness.values)}
        max_constraints = {index: value for (index, value) in enumerate(self.sources[0].fitness.constraints)}
        # Initialize counter based on feasibility of first source
        feasible_nb = 1 if self.sources[0].fitness.is_feasible() else 0
        # First one already checked, counter must start at 1
        population_size = 1
        for source in itertools.chain(self.sources[1:], self.bees):
            # Get fitness
            fitness = source.fitness
            # Find max and min fitness for each objective
            for (i, value) in enumerate(fitness.values):
                if value > max_fitness[i]:
                    max_fitness[i] = value
                if value < min_fitness[i]:
                    min_fitness[i] = value
            # Find max constraints for each constraint
            for (i, value) in enumerate(fitness.constraints):
                if value > max_constraints[i]:
                    max_constraints[i] = value
            # Increment number of unconstrained solutions
            if fitness.is_feasible():
                feasible_nb += 1
            population_size += 1
        # Store in hive fitness max and min
        max_fitness = tuple((max_fitness[i] for i in range(len(max_fitness))))
        min_fitness = tuple((min_fitness[i] for i in range(len(min_fitness))))
        # If no constraint return () --> ((), (), ...)
        max_constraints = tuple((max_constraints[i] for i in range(len(max_constraints))))
        # Update Fitness class max and min
        self._fitness_type.set_bounds_for_fitness(min_fitness, max_fitness, max_constraints)
        # Update feasibility rate
        self._fitness_type.set_feasibility_rate(feasible_nb / population_size)
        message = 'Fitness : min={}, max={} --- Constraints : {} --- Feasibility rate : {} (<iteration={}>, <evaluation={}>, <phase={}>)'
        self.logging.info(message.format(min_fitness, max_fitness, max_constraints,
                                         self._fitness_type.feasibility_rate,
                                         self.nb_iteration, self.nb_evaluation,
                                         self.current_phase))

    def _prepare_bees(self):
        """Prepare positions that need to be evaluated based on current sources
        (:attr:`sources`) and bees (:attr:`bees`).

        :Return: A :class:`pd.DataFrame` with bees and sources
                 positions that need to be evaluated.
        """
        # Hash table where Bee DataFrame index mapped to source index
        # such as dict[source_id] = bee_id
        hash_table = {}

        # Positions to evaluate as a DataFrame
        nb_bees = len(self.bees)
        src_count = 0
        bees_pos = []
        sources_pos = []
        for (index, bee) in enumerate(self.bees):
            bees_pos.append(bee.position)
            if not bee.source.evaluated:
                # Link source id to bee id
                hash_table[nb_bees + src_count] = index
                src_count += 1
                # Add source to list of list
                sources_pos.append(bee.source.position)
        # Create data frames
        bee_df = DiscreteCriterion.to_df(bees_pos)
        if sources_pos:
            source_df = DiscreteCriterion.to_df(sources_pos)
            # Update source data frame index
            source_df.index = [nb_bees + index for index in source_df.index]
            # Join together both data frames
            positions = pd.concat([bee_df, source_df], axis=0, join='outer')
        else:
            positions = bee_df
        # Assign name to index
        positions.index.name = "Index"
        return hash_table, positions

    def _gather_nectar(self):
        """Compute positions using :meth:`_prepare_bees` and get fitness and constraints
        corresponding values with :meth:`evaluate`.
        Then update :attr:`bees` and :attr:`sources` with these values.
        """
        # Get hash table and simulation data frame
        hash_table, positions = self._prepare_bees()

        # Get simulation result of each position
        fitness_values, constraints_values = self.evaluate(positions)

        # Now update fitness based on index
        nb_bees = len(self.bees)
        for (index, values) in enumerate(zip(fitness_values, constraints_values)):
            # Update source using hash table
            if index >= nb_bees:
                self.bees[hash_table[index]].source_fitness = values[0]
                # Only if constraint exists
                if values[1] is not None:
                    self.bees[hash_table[index]].source_fitness.constraints = values[1]
                if self._keep_history_track:
                    # Store source in population history
                    source_copy = self.bees[hash_table[index]].source.get_namedtuple()
                    self._population_history.append(source_copy)

            # Update bee using index directly
            else:
                self.bees[index].fitness.values = values[0]
                # Only if constraint exists
                if values[1] is not None:
                    self.bees[index].fitness.constraints = values[1]
                if self._keep_history_track:
                    # Store bee in population history
                    bee_copy = self.bees[index].get_namedtuple()
                    self._population_history.append(bee_copy)

    def _bee_dance(self, scaling=False, C=2, elitist=False):
        """Define a probability law table based on quality of each source in :attr:`sources`.
        Quality is defined using dominance and the more a source **dominates** another
        the higher the probability is. Probability table is stored in :attr:`weighted_prob`.
        Can be used to perform local search on most promising sources (exploitation).
        For instance, original **ABC** algorithm uses this dance on the Onlookers phase
        (:meth:`onlookers_phase`).

        :param scaling: If set to :data:`True` *C* will be used to recalibrate
                        the max weight which can be assigned to a source. This
                        can be used to be less elitist. Max weight probability
                        will then be defined as :math:`C * average_{weight}`.
        :param C: Float used to control the recalibration if *scaling* set to :data:`True`.
        :param elitist: If :data:`True` bad source (source which dominate no other one)
                        will have 0 has probability weight and therefor cannot be
                        selected by onlookers (:meth:`onlookers_phase`) bees
                        (Default to :data:`False`).

        .. note::
            Source that dominate no other sources will have 0 in :attr:`weighted_prob`
            and then cannot be picked if *elitist* is set to :data:`True`.
        """
        prob_tab = []
        # Count number of source dominated by each source
        for source in self.sources:
            dom_count = 0
            for other_source in self.sources:
                if source.fitness > other_source.fitness:
                    dom_count += 1
            # Let a chance to every source to be picked
            if not elitist:
                dom_count += 1
            prob_tab.append(dom_count)
        if sum(prob_tab) == 0:
            # Assign same probability for each source
            prob_tab = [1 for _ in range(self.nb_sources)]
        else:
            if scaling:
                # Recalibrate weight_max to C * weight_average
                weight_average = sum(prob_tab) / self.nb_sources
                weight_max = max(prob_tab)
                a = (C - 1) * weight_average / (weight_max - weight_average)
                b = weight_average * (weight_max - C * weight_average) / (weight_max - weight_average)
                prob_tab = [weight * a + b for weight in prob_tab]
        # Create new weighted probability table
        self.weighted_prob = rh.init_roulette(prob_tab)
        # Keep track of dominance table
        message = 'Source dominance table : {} (<iteration={}>, <evaluation={}>, <phase={}>)'
        self.logging.debug(message.format(prob_tab, self.nb_iteration, self.nb_evaluation,
                                          self.current_phase))

    def _update_bees(self):
        """Call :meth:`bee.add_to_archive` method on each bee to populate the archive
        (:attr:`archive`). If fitness is constrained then first call :meth:`_update_bounds`
        to update bounds and feasibility rate.
        """
        # Flag used to select correct domination operator
        have_constraints = False
        if hasattr(self._fitness_type, 'constraints'):
            # Now update best and worst fitness if constraint exists
            self._update_bounds()
            have_constraints = True
        for bee in self.bees:
            bee.add_to_archive()
            bee.update_source(have_constraints)
        message = 'Archive length is {} (<iteration={}>, <evaluation={}>, <phase={}>)'
        self.logging.info(message.format(len(self.archive), self.nb_iteration, self.nb_evaluation,
                                         self.current_phase))

    def _init_sources(self):
        """Extract criteria from a :attr:`configuration` and initialize them based
        on their type with a random value.
        """
        # Hash table
        type_dict = {'qualitative': QualitativeCriterion,
                     'discrete': DiscreteCriterion,
                     'continuous': ContinuousCriterion}
        criteria_raw = self.configuration['criteria']
        # Reset sources
        self.sources = list()
        for source in range(self.nb_sources):
            # New position for each source
            position = {}
            for crit_name in criteria_raw:
                # Remove typo mistakes
                crit_type = criteria_raw[crit_name]['type'].lower().strip()
                constructor = type_dict[crit_type]
                if crit_type in ('qualitative', 'discrete'):
                    position[crit_name] = constructor(crit_name,
                                                      criteria_raw[crit_name]['steps'],
                                                      None)
                else:
                    position[crit_name] = constructor(crit_name, None,
                                                      criteria_raw[crit_name]['low_bound'],
                                                      criteria_raw[crit_name]['up_bound'])
            # Create new source and append it to sources
            self.sources.append(FoodSource(position, self._fitness_type))
        # Log source initialization
        self.logging.debug('Sources initialized using configuration "criteria" field')

    def _init(self, max_iteration, max_evaluation, start_iteration=None, start_evaluation=None):
        """Define when optimization must stop with *max_iteration* and *max_evaluation*.
        *start_iteration* and *start_evaluation* can be used when optimization process
        must be stop and restarted at the same state.

        :param max_iteration: The maximum number of iteration.
        :param max_evaluation: The maximum number of solution evaluation.
        :param start_iteration: First iteration to proceed (default to 0)
        :param start_evaluation: Number of evaluation at start (default to
                                 ``len(sources)`` as random position already assigned
                                 to sources when configuration load (:meth:`_init_sources`))

        .. note::
            Rules to stop optimization must be implemented by **child** class
            and :class:`pyMayBee.Entity.bees.BaseBee` must increment theses counter when a new position
            is evaluated.
        """
        # Allows to initialize using user values. Needed to restart an optimization
        self.nb_iteration = 0 if start_iteration is None else start_iteration
        self.nb_evaluation = len(self.sources) if start_evaluation is None else start_evaluation
        self._max_iteration = max_iteration
        self._max_evaluation = max_evaluation


class ABCHive(HiveAbstractInterface):

    """Implementation of MOABC algorithm to solve multi-objective and multi-criteria
    optimization problems.
    Construction based on literature improvements and defined as:

        1. Initialization of the population (:meth:`init_phase`)
        2. Exploration of the domain definition (:meth:`employed_phase`)
        3. Exploitation using knowledge of other bees and result of exploration
           (:meth:`onlookers_phase`)
        4. Give up fruitless solutions (:meth:`employed_phase`)

    Optimization can be start using :meth:`run` and can be restart using :meth:`restart`.
    Phases (2, 3, 4) will be repeated until reaching a predefined number of iterations
    or evaluations.

    :param archive: A container :class:`pyMayBee.Primitive.archive.ArchiveInterface` used to store fruitful solutions.
    :param configuration: :class:`dict` with all criterion definitions
                           under 'criteria' key and any other informations if needed.
    :param population_size: The number of Bee inside the algorithm. Use to define the source
                             number which is :math:`population_{size} \  // \  2`.
    :param max_trial: Maximum number of evaluation for a specific source before calling a Scout.
    :param fitness_type: Type of :class:`pyMayBee.Primitive.fitness.Fitness` used to compare different position.
    :param history: :data:`True` to keep track of archive, sources, and whole population.

    .. note::
        This class is an Abstract class and does not provide any problem definition.
        That is, it must be inherited and :meth:`evaluate` must be implemented.

    .. note::
        History is accessible for archive, sources, and population respectively in:

            - :attr:`archive_history`
            - :attr:`sources_history`
            - :attr:`population_history`

    """

    def __init__(self, archive, configuration,
                 population_size, max_trial, fitness_type=_BasedFitness,
                 history=True):
        # Keep track of algorithm evolution an keep a log of states
        self.logging = logging.getLogger(__name__ + '.ABCHive')

        # Force a even number of food source
        self.nb_sources = population_size // 2
        self._fitness_type = fitness_type
        self._trial_limit = max_trial
        self.archive = archive  # Keep track of best solutions

        # Load configuration as a dict
        self.configuration = configuration
        self.logging.debug('Configuration loaded from JSON file')
        # Extract criteria from configuration and create sources
        self._init_sources()  # Create sources list
        self.bees = ()
        self.bees_creator = dict(init=OBLBee,
                                 employed=(EmployedLevySpyBee,
                                           EmployedSpyBee,
                                           0.5),
                                 onlookers=OnlookerBee,
                                 scout=ScoutBee)

        # Weighted probabilities for each sources are the same at start
        # Return [1, 2, 3, 4, 5, 6, 7 ...] (cumulated array)
        self.weighted_prob = rh.init_roulette([1 for _ in range(self.nb_sources)])

        # Keep track of phase
        self._current_phase = self.__class__.PHASE.IDLE
        self.logging.info('ABC in {} phase'.format(self.current_phase))

        # Flag to set or unset history tracking
        self._keep_history_track = history
        # 3 layer deep history for sources
        self._sources_history = {}
        # One level deep history for archive
        self._archive_history = {}
        # Keep track of all evaluations as a list (bees and sources)
        self._population_history = []

    @property
    def archive_history(self):
        """:Return: Archive history state."""
        return self._archive_history

    @property
    def sources_history(self):
        """:Return: Sources history state."""
        return self._sources_history

    @property
    def population_history(self):
        """:Return: Population history state."""
        return self._population_history

    def run(self, max_iteration, max_evaluation, bees={}, **kwargs):
        """Launch the optimization process.

        :param max_iteration: The maximum number of iteration.
        :param max_evaluation: The maximum number of solution evaluation.
        :param bees: A dict used to assign bees to different phases

                     .. code-block:: python

                      bees = {}
                      bees['init'] = pyMayBee.Entity.bees.OBLBee
                      bees['employed'] = (pyMayBee.Entity.bees.EmployedLevySpyBee,
                                          pyMayBee.Entity.bees.EmployedSpyBee, 0.5)
                      bees['onlookers'] = pyMayBee.Entity.bees.OnlookerBee
                      bees['scout'] = pyMayBee.Entity.bees.ScoutBee

        :param kwargs: Key argument passed to :meth:`_init`

        .. note::
            Employed phase ask for two kind of bee to let user select different
            scheme for improving exploration and exploitation.
            Last argument defines bee ratio between both.
            Ratio define the probability to select the first bee type and
            :math:`0 < ratio < 1`.
        """
        # Replace default bees by user ones if exist
        self.bees_creator.update(bees)

        # Check employed structure
        try:
            if not isinstance(self.bees_creator['employed'], Sequence):
                raise AbcError('Employed bee must be a Sequence.')
            if len(self.bees_creator['employed']) != 3:
                raise AbcError('Sequence length must be equals to 3.')
            if self.bees_creator['employed'][2] > 1 or self.bees_creator['employed'][2] < 0:
                raise AbcError('Ratio must be in [0, 1].')
        except AbcError as err:
            # Store to log
            message = 'Incorrect Employed bee definition ({}) please check `run` method docstring.'
            self.logging.error(message.format(self.bees_creator['employed']))
            # Raise
            raise err

        # Initialize algorithm
        self._init(max_iteration, max_evaluation, **kwargs)
        # Only proceed to initialization if needed
        if self.nb_iteration == 0:
            # Initialize based population
            self.init_phase()
            self._update_archive_history()
            # Proceed some operation if needed
            self.iteration_operations()
        while self.nb_iteration < self.max_iteration and self.nb_evaluation < self.max_evaluation:
            # Initialization alone in iteration 0
            self.nb_iteration += 1
            # Exploration
            self.employed_phase()
            if self.nb_evaluation >= self.max_evaluation:
                break
            # Exploitation
            self.onlookers_phase()
            if self.nb_evaluation >= self.max_evaluation:
                break
            # Update archive history at each iteration
            self._update_archive_history()
            # Proceed some operation if needed
            self.iteration_operations()

        # Update archive history (needed to get final state even if iteration did not terminate)
        self._update_archive_history()

        message = 'Unfeasible in archive : {} (<iteration={}>, <evaluation={}>, <phase={}>)'
        self.logging.debug(message.format(sum(1 for source in self.archive if not source.fitness.is_feasible()),
                                          self.nb_iteration, self.nb_evaluation, self.current_phase))
        message = 'Optimization now ended after {} evaluations (<iteration={}>, <evaluation={}>, <phase={}>)'
        self.logging.info(message.format(self.nb_evaluation, self.nb_iteration, self.nb_evaluation,
                                         self.current_phase))

    def restart(self, sources, archive, start_iteration, start_evaluation,
                max_iteration, max_evaluation, bees={}, history=None):
        """Restart optimization process using *sources* as :attr:`sources`
        and *archive* as :attr:`archive`.

        :param sources: A list of :class:`pyMayBee.Entity.sources.FoodSource` to use as sources state
        :param archive: The :class:`pyMayBee.Primitive.archive.BasedArchive` state to use
        :param start_iteration: Last saved iteration
        :param start_evaluation: Last saved evaluation
        :param max_iteration: Max iteration for optimization (count from 0)
        :param max_evaluation: Max iteration for optimization (count from 0)
        :param bees: A :class:`dict` a :class:`pyMayBee.Entity.bees.BaseBee` subclasses used for bees.
        :param history: A :class:`tuple` of history of population, sources, and archive

        .. note::
            `len(sources)` must be equal to :attr:`nb_sources` which is defined
            based on population_size parameter at instance creation (:meth:`__init__`).

        .. note::
            Update :attr:`_fitness_type` based on sources fitness.

        .. warning::
            *archive* cannot be empty if *start_iteration* different from 0 because
            initialization (:meth:`init_phase`) will be skipped and employed phase
            (:meth:`employed_phase`) need at least an archive solution.
        """
        # Hive must be initialize with correct population size
        sources_length = len(self.sources)
        assert sources_length == len(sources), 'New sources size is not the same as current ({}).'.format(sources_length)
        # Employed and Onlookers phases need at least one archive member
        assert len(archive) >= 1, 'Archive must not be empty.'
        # Update fitness class based on sources fitness
        self._fitness_type = type(sources[0].fitness)
        # Make a copy of the list of sources
        self.sources = [source for source in sources]
        # Replace archive
        self.archive = archive

        # Assign history to Hive history to
        if history is not None:
            self._population_history = history[0]
            self._sources_history = history[1]
            self._archive_history = history[2]

        # Run as usual the optimization process but without initialization
        # using the kwargs argument to change _init behavior
        self.run(max_iteration, max_evaluation,
                 bees, start_iteration=start_iteration, start_evaluation=start_evaluation)

    def init_phase(self, **kwargs):
        """Update source position using :attr:`bees_creator['init']` bees.
        Default does an opposite based learning search and keep the best between
        random start source position and its opposite.
        New source position and fitness result in the best between original source
        and bee.

        :param kwargs: Key arguments pass to Bee constructor

        .. note::
            Each sources are updated using one bee per source.
        """
        # Update current phase
        self._current_phase = self.__class__.PHASE.Initialization
        message = 'ABC in {} phase (<iteration={}>, <evaluation={}>, <phase={}>)'
        self.logging.debug(message.format(self.current_phase.upper(), self.nb_iteration,
                                          self.nb_evaluation, self.current_phase))

        bee_type = self.bees_creator['init']
        # Create OBLBee and simulate it
        self.bees = tuple((bee_type(self, source, **kwargs) for source in self.sources))
        # Now run simulations and update fitness
        self._gather_nectar()
        # Add fitness of new position to archive
        self._update_bees()
        # Update history
        self._update_sources_history()

    def employed_phase(self, **kwargs):
        """Update source position using :attr:`bees_creator['employed']` bees.
        Default compute the employed phase an scout phase.
        Default compute a random walk:

            - 50% by Lévy flight
            - 50% by Uniform motion

        Both motion are based on an archive source and another randomly selected source.
        Both archive and the other source are different from source that is updated.
        In case of source is the same as archive one and there is only one source
        in the archive only perform random walk based on the other source.
        New source position and fitness result in the best between original source
        and bee.

        :param kwargs: Key arguments pass to Bee constructor

        .. note::
            Scout phase is implemented here to make parallel computing more
            efficient. Using this implementation at least :attr:`self.nb_sources`
            positions will be evaluate whereas using a specific phase for scout
            can lead to phase with only 2 evaluations slowing down the entire computation.

        .. note::
            Random motion always attracted by archive source but not always by the
            source one. This avoid too high elitism which can lead to poor population
            diversity.

        .. note::
            Each sources are updated using one bee per source : **exploration** phase.

        """
        # Update current phase
        self._current_phase = self.__class__.PHASE.Employed
        message = 'ABC in {} phase (<iteration={}>, <evaluation={}>, <phase={}>)'
        self.logging.debug(message.format(self.current_phase.upper(), self.nb_iteration,
                                          self.nb_evaluation, self.current_phase))

        self.bees = []
        scout_occurs = False  # Debug
        for source in self.sources:
            if source.lost(self.max_trial):
                # Create a Scout bee
                bee_type = self.bees_creator["scout"]
                scout_occurs = True
            else:
                # Create a Employed bee based on the ratio
                if rh.RandomGenerator.random() > self.bees_creator['employed'][2]:
                    # > ratio ---> second type of bee
                    bee_type = self.bees_creator['employed'][1]
                else:
                    # < ratio ---> first type of bee
                    bee_type = self.bees_creator['employed'][0]
            self.bees.append(bee_type(self, source, **kwargs))
        # Make bees container immutable (bees immutability depends on implementation)
        self.bees = tuple(self.bees)

        # Debug
        if scout_occurs:
            message = 'SCOUT Phase occurred (<iteration={}>, <evaluation={}>, <phase={}>)'
            self.logging.info(message.format(self.nb_iteration, self.nb_evaluation,
                                             self.current_phase))

        # Now run simulations and update fitness
        self._gather_nectar()
        # Add fitness of new position to archive
        self._update_bees()
        # Update history
        self._update_sources_history()

    def onlookers_phase(self, **kwargs):
        """Update source position using :attr:`bees_creator['onlookers']` bees.
        Default compute a random walk from best sources. Best sources are defined
        according to the bee dance (:meth:`_bee_dance`). Each bee compute a Uniform
        motion by learning from an archive solution.
        New source position and fitness result in the best between original source
        and bee.

        .. note::
            Random motion not always attracted by archive source. This avoid too
            high elitism which to avoid being trapped in a local optimum.

        .. note::
            Multiple bees can select the same source : **exploitation** phase.
        """
        # Update current phase
        self._current_phase = self.__class__.PHASE.Onlookers
        message = 'ABC in {} phase (<iteration={}>, <evaluation={}>, <phase={}>)'
        self.logging.debug(message.format(self.current_phase.upper(), self.nb_iteration,
                                          self.nb_evaluation, self.current_phase))

        # Evaluate source pertinence
        self._bee_dance(scaling=False, elitist=False)
        bee_type = self.bees_creator["onlookers"]
        # Init bee with hive.prob and compute new position using Lévy flight
        self.bees = tuple((bee_type(self, **kwargs) for _ in range(self.nb_sources)))
        # Now run simulations and update fitness
        self._gather_nectar()
        # Add fitness of new position to archive
        self._update_bees()
        # Update history
        self._update_sources_history()

    def _update_archive_history(self):
        """Update archive history.
        Store archive state at each iteration as a key value pair where key is
        the iteration number (:attr:`nb_iteration`) and value the :attr:`archive`
        snapshot.
        Store inside a :class:`dict`
        """
        if not self._keep_history_track:
            return
        else:
            # Update archive history with true archive copy
            self._archive_history[self.nb_iteration] = self.archive.clone()

    def _update_sources_history(self):
        """Update sources history in a deeply nested :class:`dict` where additional
        information about bounds:

        .. code-block:: python

            {'nb_iteration':
                'current_phase':
                    {'max': val, 'min': val, 'constraints_max': val,
                     'feasibility_rate': val, 'nb_evaluation': val,
                     'sources': sources_snapshot}
                }
            }

        """
        if not self._keep_history_track:
            return
        else:
            self._sources_history.setdefault(self.nb_iteration, {})[self.current_phase] = {}
            # Avoid redundancy
            dict_ref = self._sources_history[self.nb_iteration][self.current_phase]
            # Only make sense for constrained fitness to be able to recalculate normalized fitness
            if hasattr(self._fitness_type, 'constraints'):
                dict_ref['max'] = self._fitness_type.fitness_max
                dict_ref['min'] = self._fitness_type.fitness_min
                dict_ref['constraints_max'] = self._fitness_type.constraints_max
                dict_ref['feasibility_rate'] = self._fitness_type.feasibility_rate
            # Only keep a limited copy of sources evolution
            dict_ref['sources'] = [source.get_namedtuple() for source in self.sources]
            # Also store number of evaluation at each phase
            dict_ref['nb_evaluation'] = self.nb_evaluation
