# -*- coding:Utf8 -*-

"""
    Algorithm subpackage which contains Algorithm implementation.

        - :class:`pyMayBee.Algorithm.MOABC.HiveAbstractInterface` : Interface which can be used to
          construct an implemenation of ABC (Artificial Bee Colony) algorithm
          for Multi-objective and multi-criteria algorithm
        - :class:`pyMayBee.Algorithm.MOABC.ABCHive` : Implementation of a multi-criteria and
          multi-objective optimization algorithm based on Artificial Bee Colony
          optimization process using :class:`pyMayBee.Algorithm.MOABC.HiveAbstractInterface` interface.

          Support for:

            - constrained and unconstrained problems
            - Qualitative / Quantitative (Discrete and continuous) critera
"""


from .MOABC import *
