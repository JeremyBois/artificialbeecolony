# -*- coding:Utf8 -*-

"""
    exceptions.py
"""


class AbcError(Exception):
    pass


class AbcAttributeError(AbcError):
    pass


class ClassAttributeError(AbcError):
    pass


class ClassMethodProtected(object):

    """A rewrite of python ``@classmethod`` to avoid user to change weights using instances.
    Weights are shared between instances and changing it will lead to nasty stuff ...
    """

    def __init__(self, decorated_func):
        self.decorated_func = decorated_func

    def __get__(self, obj, klass=None):
        """When decorator called"""
        if klass is None:
            klass = type(obj)
        # Instance used to call method
        if obj is not None:
            text = ("You must not changed {}.weights "
                    "after an instance is created.".format(klass.__name__))
            raise ClassAttributeError(text)

        def wrap(*args, **kwargs):
            return self.decorated_func(klass, *args, **kwargs)
        # Return a new function which accept any type of argument
        return wrap
