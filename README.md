# pyMayBee  #

**Mise à jour: 20170718**


## Tests: ##
In order to produce a complete test of the package run the following command
in the package folder.

    nosetests --with-coverage --cover-erase --cover-package=pyMayBee --cover-html --exclude-dir="./pyMayBee/Problem"
