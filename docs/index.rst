.. pyMayBee documentation master file, created by
   sphinx-quickstart on Mon Feb 27 19:49:09 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyMayBee's documentation!
====================================

.. toctree::
   :maxdepth: 2

   instructions
   pyMayBee



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

