How to:
=======

Installation instructions
-------------------------

You can install the package by first downloading the package at and the use
**pip** to install it.

.. code-block:: bash

    cd {{Package_directory}}
    pip install .

or you can let **pip** find it and install it from **Pypi** directly.

.. code-block:: bash

    pip install pyMayBee


Test installation
-----------------
In order to produce a complete test of the package run the following command
in the package folder (additional package are required for testing).

.. code-block:: bash

    nosetests --with-coverage --cover-erase --cover-package=pyMayBee --cover-html --exclude-dir="./pyMayBee/Problem"



Get it started
--------------

A ready to use implementation of the MOABC can be found in .. py:mod::`pyMayBee.Entity.abc.py`
which can be used to solve every kind of problems.
Use examples can be found in .. py:mod::`benchmarks.py`

This package can also be used to construct a multi-objective optimization algorithm
using tools from .. py:subpackage::`pyMayBee.Entity` or .. py:subpackage::`pyMayBee.Primitive`.
