pyMayBee.Problem
================

.. automodule:: pyMayBee.Problem
    :members:
    :undoc-members:
    :show-inheritance:


pyMayBee.Problem.DTLZ
---------------------

.. automodule:: pyMayBee.Problem.DTLZ
    :members:
    :undoc-members:
    :show-inheritance:

pyMayBee.Problem.ZDT
--------------------

.. automodule:: pyMayBee.Problem.ZDT
    :members:
    :undoc-members:
    :show-inheritance:

pyMayBee.Problem.hanne
----------------------

.. automodule:: pyMayBee.Problem.hanne
    :members:
    :undoc-members:
    :show-inheritance:

pyMayBee.Problem.other
----------------------

.. automodule:: pyMayBee.Problem.other
    :members:
    :undoc-members:
    :show-inheritance:

pyMayBee.Problem.schaffer
-------------------------

.. automodule:: pyMayBee.Problem.schaffer
    :members:
    :undoc-members:
    :show-inheritance:
