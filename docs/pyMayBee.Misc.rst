pyMayBee.Misc
=============

.. automodule:: pyMayBee.Misc
    :members:
    :undoc-members:
    :show-inheritance:


pyMayBee.Misc.history_helper
----------------------------

.. automodule:: pyMayBee.Misc.history_helper
    :members:
    :undoc-members:
    :show-inheritance:

pyMayBee.Misc.random_helper
---------------------------

.. automodule:: pyMayBee.Misc.random_helper
    :members:
    :undoc-members:
    :show-inheritance:

.. autofunction:: pyMayBee.Misc.random_helper.levy_step

.. autofunction:: pyMayBee.Misc.random_helper.init_roulette

.. autofunction:: pyMayBee.Misc.random_helper.launch_roulette

.. autofunction:: pyMayBee.Misc.random_helper.roll

.. autofunction:: pyMayBee.Misc.random_helper.levy_flight

.. autofunction:: pyMayBee.Misc.random_helper.directed_levy_flight

.. autofunction:: pyMayBee.Misc.random_helper.brownian_motion

.. autofunction:: pyMayBee.Misc.random_helper.directed_uniform_motion

.. autofunction:: pyMayBee.Misc.random_helper.uniform_motion

pyMayBee.Misc.tools
-------------------

.. automodule:: pyMayBee.Misc.tools
    :members:
    :undoc-members:
    :show-inheritance:

.. autofunction:: pyMayBee.Misc.history_helper.backup_sources

.. autofunction:: pyMayBee.Misc.history_helper.backup_archive

.. autofunction:: pyMayBee.Misc.history_helper.backup_population


pyMayBee.Misc.indicators
------------------------

.. automodule:: pyMayBee.Misc.indicators
    :members:
    :undoc-members:
    :show-inheritance:
