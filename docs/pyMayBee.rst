API
===

.. automodule:: pyMayBee
    :members:
    :undoc-members:
    :show-inheritance:

.. toctree::

    pyMayBee.Algorithm
    pyMayBee.Entity
    pyMayBee.Misc
    pyMayBee.Primitive
    pyMayBee.Problem


pyMayBee.exceptions
-------------------

.. automodule:: pyMayBee.exceptions
    :members:
    :undoc-members:
    :show-inheritance:
